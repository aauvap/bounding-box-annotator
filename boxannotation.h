#ifndef BOXANNOTATION_H
#define BOXANNOTATION_H

#include <vector>
#include <memory>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui.hpp>
#include <QString>
#include <QStringList>

enum BoxStatus {
	ACTIVE,
	LASTFRAMEREACHED
};

struct BoxProperties 
{
    cv::Point PointA;
    cv::Point PointB;

    int objectID;
    cv::Rect theBox;
    QString theTag;
    bool aSet, bSet;
    bool aligned;
    QStringList metaDataNames;
    std::vector<bool> metaDataValues;
};


class BoxTracker
{
public:
	BoxTracker(cv::Rect currentBox, cv::Mat currentImage);

	cv::Rect trackObject(cv::Mat newImage);
	void resetTracking();

private:
	void checkBox(cv::Size imageSize);
	void camShift();

	// Properties for applying CamShift
	cv::Rect currentBox;

	cv::Mat roiHist;
	cv::Mat hsvImage;
	cv::Mat mask;
	cv::Mat hueImage;
	cv::Mat backProjectedImage;
	cv::Mat histImg;

	const int vmin = 10;
	const int vmax = 256;
	const int smin = 30;
};


class BoxAnnotation
{
public:
	BoxAnnotation();
	BoxAnnotation(int id, QStringList metaDataNames = QStringList());
	BoxAnnotation(std::vector<cv::Point> corners, int id, QStringList metaDataNames = QStringList(), QString tag = QString());
	BoxAnnotation(cv::Point a, cv::Point b, int id, QStringList metaDataNames = QStringList(), QString tag = QString());


	cv::Point a() const;
	cv::Point b() const;
	QString aAsString() const;
	QString bAsString() const;
	cv::Rect box() const;
    int id() const;
    void setId(int id);

    bool isFinished() const { return activeStatus == LASTFRAMEREACHED ? true : false; };
	void setIsFinished(bool isFinished) {
		activeStatus = isFinished ?
            LASTFRAMEREACHED : ACTIVE;
	};


	void setA(cv::Point position);
	void setB(cv::Point position);
	bool isFull();
	void moveUp(int distance);
	void moveDown(int distance);
	void moveLeft(int distance);
	void moveRight(int distance);
	void shrinkVertical(int distance);
	void shrinkHorizontal(int distance);
	void growVertical(int distance);
	void growHorizontal(int distance);
	QString tag() const;
	void setTag(QString tag);
	bool getMetaData(int metaDataId) const;
	void setMetaData(int metaDataId, bool value);
	QString getMetaDataName(int metaDataId) const;
	int getMetaDataSize();

    void setBonusInfo(const std::string& bonusInfo) { this->bonusInfo = bonusInfo; };
	std::string getBonusInfo() const { return this->bonusInfo;}


    void undo();

	void trackObject(cv::Mat previousImage, cv::Mat currentImage);
	void resetTracking();


protected:
	void initPoints(cv::Point a, cv::Point b);
	void initMetaData(QStringList metaDataNames);
	void initTag(QString tag);
	void realign();
    void addToHistory();
	void setRectangle(cv::Rect rect);
	cv::Point pointWithinImage(cv::Point point);

    BoxProperties currentBox;
    std::vector<BoxProperties> history;

	std::shared_ptr<BoxTracker> tracker;

	cv::Point imageDims;
	BoxStatus activeStatus;

	std::string bonusInfo;
};
#endif // BOXANNOTATION_H
