#include "temporalinterpolator.h"

TemporalInterpolator::TemporalInterpolator(int currentFrameNumber, const QList<BoxAnnotation>& currentAnnotations)
{
	startFrameNumber = currentFrameNumber;
	startAnnotations = currentAnnotations;

	// Do not interpolate annotations which are marked as finished
	auto it = startAnnotations.begin();

	while (it != startAnnotations.end())
		if (it->isFinished()) {
			it = startAnnotations.erase(it);
		} else {
			++it;
	}

	endFrameNumber = -1;
}

int TemporalInterpolator::interpolate(int currentFrameNumber, const QList<BoxAnnotation> &newAnnotations,
	const std::map<int, QList<BoxAnnotation>> &existingAnnotations)
{
	endFrameNumber = currentFrameNumber;

	if ((endFrameNumber == startFrameNumber) || 
		(abs(endFrameNumber - startFrameNumber) < 2)) {
		return 1;
	}

	// Find out if there are any 'fixed' annotations between the start and end frames
	// with the same id as the new annotations
	for (auto newAnnotation : newAnnotations)
	{
		std::vector<std::pair<int, BoxAnnotation> > fixedAnnotations;

		for (auto startAnnotation : startAnnotations)
		{
			if (startAnnotation.id() == newAnnotation.id())
			{
				fixedAnnotations.push_back(std::pair<int, BoxAnnotation>(startFrameNumber, startAnnotation));
			}
		}

		if (fixedAnnotations.empty())
		{
			// If we have not found a corresponding annotation when the interpolator was instantiated,
			// there is nothing for us to interpolate
			continue;
		}

		int bigger = currentFrameNumber > startFrameNumber ? currentFrameNumber : startFrameNumber;
		int smaller = currentFrameNumber > startFrameNumber ? startFrameNumber : currentFrameNumber;
		
		// Check if there are any existing annotations between the start frame and the end frame 
		// that we should take into account when computing the interpolated annotations
		for (int frameNumber = smaller + 1; frameNumber < bigger; frameNumber++)
		{
			auto it = existingAnnotations.find(frameNumber);

			if (it != existingAnnotations.end())
			{
				for (auto i = 0; i < it->second.size(); ++i)
				{
					BoxAnnotation boxAnn = it->second.at(i);

					if (boxAnn.id() == newAnnotation.id())
					{
						fixedAnnotations.push_back(std::pair<int, BoxAnnotation>(frameNumber, boxAnn));
					}
				}
			}
		}

		// At last, push the new annotation as the final interpolation target
		fixedAnnotations.push_back(std::pair<int, BoxAnnotation>(endFrameNumber, newAnnotation));

		if (fixedAnnotations.size() < 2)
		{
			// If less than two fixed annotations, something has gone wrong
			continue;
		}

		// Now that we have retrieved all the annotations with the same id between 
		// startFrame and endFrame, we may interpolate the annotations
		int previousFixedAnnotation = 0;
		int nextFixedAnnotation = 1;

		for (auto frameNumber = smaller + 1; frameNumber <= bigger; ++frameNumber)
		{
			if (fixedAnnotations[nextFixedAnnotation].first == frameNumber)
			{
				if (nextFixedAnnotation == (fixedAnnotations.size() - 1))
				{
					break; // We have finished
				}
				previousFixedAnnotation++;
				nextFixedAnnotation++;
				continue;
			}

			if (fixedAnnotations[previousFixedAnnotation].second.isFinished()) {
				break; // The annotation is marked as finished. We should not interpolate more
			}

			BoxAnnotation interpolatedAnnotation = fixedAnnotations[previousFixedAnnotation].second;

			int prevFrameNumber = fixedAnnotations[previousFixedAnnotation].first;
			cv::Point prevA = fixedAnnotations[previousFixedAnnotation].second.a();
			cv::Point prevB = fixedAnnotations[previousFixedAnnotation].second.b();
			cv::Point nextA = fixedAnnotations[nextFixedAnnotation].second.a();
			cv::Point nextB = fixedAnnotations[nextFixedAnnotation].second.b();
			int nextFrameNumber = fixedAnnotations[nextFixedAnnotation].first;

			cv::Point interpolatedA, interpolatedB;
			interpolatedA.x = linearInterpolation(frameNumber, prevFrameNumber, prevA.x, nextFrameNumber, nextA.x);
			interpolatedA.y = linearInterpolation(frameNumber, prevFrameNumber, prevA.y, nextFrameNumber, nextA.y);
			interpolatedB.x = linearInterpolation(frameNumber, prevFrameNumber, prevB.x, nextFrameNumber, nextB.x);
			interpolatedB.y = linearInterpolation(frameNumber, prevFrameNumber, prevB.y, nextFrameNumber, nextB.y);

			interpolatedAnnotation.setA(interpolatedA);
			interpolatedAnnotation.setB(interpolatedB);

			// Copy the annotation into the final list of interpolated annotations
			interpolatedAnnotations[frameNumber].append(interpolatedAnnotation);
		}

	}

	return 0;
}

std::map<int, QList<BoxAnnotation>> TemporalInterpolator::getInterpolatedAnnotations()
{
	return interpolatedAnnotations;
}

int TemporalInterpolator::linearInterpolation(double x, double x1, double y1, double x2, double y2)
{
	double interpolatedPoint = (y1 * (x2 - x) + y2 * (x - x1)) / (x2 - x1);

	return round(interpolatedPoint); // Cast to int
}

cv::Point TemporalInterpolator::interpolatePoint(double currentTime, double startTime, cv::Point startPoint, double endTime, cv::Point endPoint)
{
	double xInterpolated = linearInterpolation(currentTime, startTime, startPoint.x, endTime, endPoint.x);
	double yInterpolated = linearInterpolation(currentTime, startTime, startPoint.y, endTime, endPoint.y);

	return cv::Point(xInterpolated, yInterpolated);

}
