#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QtWidgets/QDialog>
#include <QSettings>
#include <QRegExp>
#include <QtWidgets/QColorDialog>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui.hpp>

#include <common/openCVViewer/opencvviewer.h>


namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SettingsDialog(QWidget *parent, cv::Mat thermalImg);
    ~SettingsDialog();
    
    void accept();

private slots:
    void on_btnColor_clicked();
    void on_brightnessSlider_valueChanged();
    void on_contrastSlider_valueChanged();
    void on_contrastSpinBox_valueChanged();
    void on_brightnessSpinBox_valueChanged();

private:
    Ui::SettingsDialog *ui;
    QSettings settings;
    QColor overlayColor;

    void transformThermalImg();

    cv::Mat thermalImg;
};

#endif // SETTINGSDIALOG_H
