#include "annotationExporter.h"


AnnotationExporter::AnnotationExporter(QWidget * parent, const QStringList & annotationList, 
	int imageWidth, int imageHeight, const QString& annotationDir) : QDialog(parent)
{
	setWindowTitle(tr("Export annotations"));

	this->annotationList = annotationList;
	this->annotationList.sort();
	this->imageHeight = imageHeight;
	this->imageWidth = imageWidth;
	this->annotationDir = annotationDir;

	QGroupBox* annotationFormatGroupBox = new QGroupBox(tr("Export format"));
	QFormLayout* annotationFormatLayout = new QFormLayout();

	// File formats
	fileFormatComboBox = new QComboBox();

	fileFormats.append("Darknet/YOLO");

	for (auto& fileFormat : fileFormats)
	{
		fileFormatComboBox->addItem(fileFormat);
	}
	
	annotationFormatLayout->addRow(tr("Overall file format"), fileFormatComboBox);

	// Object categories
	objectCategoriesComboBox = new QComboBox();

	// Open the available csv-files in the "categoryLists" folder
	QStringList filters;
	filters << "*.txt" << "*.csv";

	QDirIterator it("./categoryLists", filters, QDir::NoFilter, QDirIterator::NoIteratorFlags);

	while (it.hasNext())
	{
		QString filePath = it.next();
		QString baseName = it.fileInfo().baseName();

		objectCategories.insert(baseName, filePath);
		objectCategoriesComboBox->addItem(baseName);
	}

	annotationFormatLayout->addRow(tr("Object categories"), objectCategoriesComboBox);

	QHBoxLayout* startConversionLayout = new QHBoxLayout();
	startConversionLayout->addStretch();

	startConversionButton = new QPushButton(tr("Start conversion"));
	connect(startConversionButton, SIGNAL(clicked(bool)), this, SLOT(startConversionButtonClicked()));
	startConversionLayout->addWidget(startConversionButton);


	// Overall layout
	annotationFormatGroupBox->setLayout(annotationFormatLayout);

	QVBoxLayout* overallLayout = new QVBoxLayout();
	overallLayout->addWidget(annotationFormatGroupBox);
	progressBar = new QProgressBar();
	progressBar->hide();
	progressLabel = new QLabel();
	progressLabel->hide();
	overallLayout->addWidget(progressBar);
	overallLayout->addWidget(progressLabel);
	overallLayout->addLayout(startConversionLayout);

	setLayout(overallLayout);
}

void AnnotationExporter::forceCoordinatesWithinImageFrame(double & xULC, double & yULC, double & xLRC, double & yLRC)
{
	xULC = std::max(0., std::min(xULC, double(imageWidth - 1)));
	yULC = std::max(0., std::min(yULC, double(imageHeight - 1)));
	xLRC = std::max(0., std::min(xLRC, double(imageWidth - 1)));
	yLRC = std::max(0., std::min(yLRC, double(imageHeight - 1)));
}

void AnnotationExporter::startConversionButtonClicked()
{
	startConversionButton->setEnabled(false);
	
	progressBar->setMaximum(annotationList.size());
	progressBar->show();
	progressLabel->setText(tr("Reading category labels..."));
	progressLabel->show();


	// Get object category format
	QFile objectCatFile(objectCategories[objectCategoriesComboBox->currentText()]);
	objectCatFile.open(QFile::ReadOnly);
	QMap<QString, int> objectIdCategoryPairs;

	if (objectCatFile.isOpen())
	{
		QTextStream stream;
		stream.setDevice(&objectCatFile);

		while (!stream.atEnd()) {
			QString line = stream.readLine();
			QStringList split = line.split(";", QString::SkipEmptyParts);

			if (split.size() >= 2)
			{
				// Take care if the category contains multiple category names
				QStringList categoryNameSplit = split[1].split(",");

				if (categoryNameSplit.size() > 1)
				{
					for (auto& categoryName : categoryNameSplit)
					{
						QString cleanedCategoryName = categoryName.remove("'").remove("\"").simplified().toLower(); // ImageNet categories look like this: 'tench, Tinca tinca'

						if (cleanedCategoryName.size() > 1)
						{
							objectIdCategoryPairs[cleanedCategoryName] = split[0].toInt();
						}
					}
				}
				else {
					QString cleanedCategoryName = split[1].remove("'").simplified().toLower();
					objectIdCategoryPairs[cleanedCategoryName] = split[0].toInt(); // The simplified operator removes whitespace from start and end
				}
				
			}
		}
	}

	objectCatFile.close();

	QString baseDir = QDir::currentPath();

	QDir d(annotationDir);
	d.mkdir("Export");
	d.cd("Export");
	progressLabel->setText(tr("Exporting annotations to:\n%1").arg(d.path()));
	progressLabel->show();

	QSet<QString> fileNames;
	QSet<QString> nonMatchedCategories;

	// Currently, we have hardcoded this to the Darknet/YOLO format
	if (!objectIdCategoryPairs.empty())
	{
		bool firstLine = true;
		QString lastFileName;
		QFile convertedAnnotationFile;
		int lineNumber = 0;

		for (auto& line : annotationList)
		{
			lineNumber++;
			progressBar->setValue(lineNumber);
			QApplication::processEvents();

			if (firstLine)
			{
				firstLine = false;
				continue;
			}

			

			QStringList entries = line.split(";");

			if (entries.size() > 7)
			{
				QString fileName = d.absoluteFilePath(entries[0].replace(".png", ".txt"));

				if (lastFileName != fileName)
				{
					convertedAnnotationFile.close();
					convertedAnnotationFile.setFileName(fileName);

					if (!fileNames.contains(entries[0]))
					{
						convertedAnnotationFile.open(QIODevice::WriteOnly); // Overwrite existing content
					}
					else {
						convertedAnnotationFile.open(QIODevice::Append); // Append
					}

					fileNames.insert(fileName);
					lastFileName = fileName;
				}

				// Get the object category
				int categoryNumber = -1;

				if (objectIdCategoryPairs.contains(entries[2]))
				{
					categoryNumber = objectIdCategoryPairs[entries[2]];
				}
				else if (objectIdCategoryPairs.contains(entries[2].toLower()))
				{
					categoryNumber = objectIdCategoryPairs[entries[2].toLower()];
				}
				else 
				{
					nonMatchedCategories.insert(entries[2]);
				}

				double xULC = entries[3].toDouble(); // ULC == upper left corner
				double yULC = entries[4].toDouble();
				double xLRC = entries[5].toDouble(); // LRC == lower right corner
				double yLRC = entries[6].toDouble(); 

				double xLRCold = xLRC;

				// Enfore that the annotations are within the image frame
				forceCoordinatesWithinImageFrame(xULC, yULC, xLRC, yLRC);

				double centerX = (xLRC - xULC) / 2 + xULC;
				double centerY = (yLRC - yULC) / 2 + yULC;
				double bBoxWidth = (xLRC - xULC);
				double bBoxHeight = (yLRC - yULC);

				QStringList convertedLine = (QStringList() << QString::number(categoryNumber)
					<< QString::number(centerX / imageWidth, 'g', 10)
					<< QString::number(centerY / imageHeight, 'g', 10)
					<< QString::number(bBoxWidth / imageWidth, 'g', 10)
					<< QString::number(bBoxHeight / imageHeight, 'g', 10));

				QTextStream outStream(&convertedAnnotationFile);
				outStream << convertedLine.join(" ") << "\n";
				outStream.flush();
			}
		}
	}

	QDir::setCurrent(baseDir);
	startConversionButton->setEnabled(true);

	if (!nonMatchedCategories.empty())
	{
		QString message;

		if (nonMatchedCategories.size() == 1)
		{
			message.append(tr("The tag \"%1\" was not found in the %2 category list. Thus, annotations contaiting this tag have all been assigned the number -1.").arg(nonMatchedCategories.values().join("")).arg(objectCategoriesComboBox->currentText()));
		}
		else {
			message.append(tr("The tags listed below were not found in the %1 category list. Thus, annotations containing these tags have all been assigned the number -1.\n").arg(objectCategoriesComboBox->currentText()));

			for (auto& category : nonMatchedCategories)
			{
				message.append(QString("\n%1").arg(category));
			}
		}


		QMessageBox::information(this, tr("Tags not found in category list"), message);
	}

	progressBar->hide();
	progressLabel->setText(tr("Finished exporting annotations to:\n%1").arg(d.path()));
	
}
