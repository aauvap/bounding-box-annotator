#include "settingsdialog.h"
#include "ui_settingsdialog.h"

SettingsDialog::SettingsDialog(QWidget *parent, cv::Mat thermalImg) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    ui->openCVView->setImage(thermalImg);

    this->thermalImg = thermalImg;
    ui->rgbPattern->setText(settings.value("rgbPattern").toString());
    ui->thermalPattern->setText(settings.value("thermalPattern").toString());
    ui->csvPath->setText(settings.value("csvPath").toString());
    ui->useMaskCheckBox->setChecked(settings.value("useMask").toBool());

    overlayColor = settings.value("overlayColor").value<QColor>();
    ui->btnColor->setStyleSheet(QString("QPushButton {"
        "background-color: rgb(%1, %2, %3);"
        "border-style: inset;"
        "border-width: 2px;"
        "}"
        "QPushButton:pressed {"
        "border-style: outset;"
        "}").arg(overlayColor.red()).arg(overlayColor.green()).arg(overlayColor.blue()));
    
    ui->enableThermalCheckBox->setChecked(settings.value("enableThermal").toBool());

    ui->contrastSpinBox->setValue(settings.value("thermalContrast", 1).toDouble());
    ui->brightnessSpinBox->setValue(settings.value("thermalBrightness", 0).toDouble());
	ui->useRedColorIsFinishedCheckBox->setChecked(settings.value("useRedColorIsFinished", 1).toBool());

	ui->limitTagsCheckBox->setChecked(settings.value("limitTagsToSuggested", 0).toBool());
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::accept()
{
    settings.setValue("rgbPattern", ui->rgbPattern->text());
    settings.setValue("thermalPattern", ui->thermalPattern->text());
    settings.setValue("csvPath", ui->csvPath->text());
    settings.setValue("useMask", ui->useMaskCheckBox->isChecked());
    settings.setValue("overlayColor", overlayColor);
    settings.setValue("overlayOpacity", ui->overlayOpacitySpinBox->value() / 100.0);

    settings.setValue("enableThermal", ui->enableThermalCheckBox->isChecked());

    settings.setValue("thermalContrast", ui->contrastSpinBox->value());
    settings.setValue("thermalBrightness", ui->brightnessSpinBox->value()); 
	settings.setValue("useRedColorIsFinished", ui->useRedColorIsFinishedCheckBox->isChecked());
	
	settings.setValue("limitTagsToSuggested", ui->limitTagsCheckBox->isChecked());

    done(QDialog::Accepted);
}

void SettingsDialog::on_contrastSpinBox_valueChanged()
{
    double alpha = ui->contrastSpinBox->value();
    int sliderValue = alpha * 20 + 10;
    ui->contrastSlider->setValue(sliderValue);
}

void SettingsDialog::on_brightnessSpinBox_valueChanged()
{
    ui->brightnessSlider->setValue(ui->brightnessSpinBox->value());
}

void SettingsDialog::on_brightnessSlider_valueChanged()
{
    transformThermalImg();
}

void SettingsDialog::on_contrastSlider_valueChanged()
{
    transformThermalImg();
}

void SettingsDialog::transformThermalImg()
{
    cv::Mat image = thermalImg.clone();

    double alpha = (static_cast<double>(ui->contrastSlider->value())-10.) / 20.;

    ui->contrastSpinBox->blockSignals(true);
    ui->contrastSpinBox->setValue(alpha);
    ui->contrastSpinBox->blockSignals(false);

    ui->brightnessSpinBox->blockSignals(true);
    int beta = ui->brightnessSlider->value();
    ui->brightnessSpinBox->setValue(beta);
    ui->brightnessSpinBox->blockSignals(false);
    
    
    for (auto y = 0; y < image.rows; ++y) {
        for (auto x = 0; x < image.cols; ++x) {
            int newVal = cv::saturate_cast<uchar>(alpha*(thermalImg.at<cv::Vec3b>(y,x)[0]) + beta);
            image.at<cv::Vec3b>(y, x)[0] = newVal;
            image.at<cv::Vec3b>(y, x)[1] = newVal;
            image.at<cv::Vec3b>(y, x)[2] = newVal;
        }
    }

    ui->openCVView->setImage(image);
}



void SettingsDialog::on_btnColor_clicked()
{
    QColor chosenColor;
    chosenColor = QColorDialog::getColor(overlayColor, this, QString("Choose overlay starting color (only hue is used)"));

    if (!chosenColor.isValid()) {
        return;
    }

    chosenColor.setHsv(chosenColor.hue(), 255, 255);

    overlayColor = chosenColor;

    ui->btnColor->setStyleSheet(QString("QPushButton {"
        "background-color: rgb(%1, %2, %3);"
        "border-style: inset;"
        "border-width: 2px;"
        "}"
        "QPushButton:pressed {"
        "border-style: outset;"
        "}").arg(overlayColor.red()).arg(overlayColor.green()).arg(overlayColor.blue()));
}
