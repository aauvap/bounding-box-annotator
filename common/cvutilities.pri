HEADERS       *= cvUtilities/cvutilities.h
SOURCES       *= cvUtilities/cvutilities.cpp

!win32{
	LIBS *= -lopencv_core \
		-lopencv_highgui \
		-lopencv_imgproc
}

win32{
    INCLUDEPATH += "C:/OpenCV/release/install/include"
    LIBS += C:/OpenCV/release/bin/*.dll
}

INCLUDEPATH *= $${VPATH}cvUtilities/
