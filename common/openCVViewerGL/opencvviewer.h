#ifndef OPENCVVIEWER_H
#define OPENCVVIEWER_H

#include <QtGui>
#include <QGLWidget>
#include <QtOpenGL>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <math.h>

// On Windows, GL_BGR is not defined by default. Fix that:
#ifndef GL_BGR
#define GL_BGR 0x80E0
#endif

class QGLShaderProgram;

/**
 * Widget to display frames or single images from OpenCV using OpenGL.
 *
 * This class integrates OpenCV and Qt. It is a widget that can be added to
 * your GUI and handle display of video and single images.
 *
 * To show an image, simply set it with setImage() and the display will
 * automatically be updated.
 */
class OpenCVViewer : public QGLWidget
{
    Q_OBJECT

public:
    /**
     * Constructor.
     *
     * Creates a new OpenCVViewer. Be aware that the capture source must be
     * set before any capture can commence. See setSource().
     */
    OpenCVViewer(QWidget *parent = 0, QGLWidget *shareWidget = 0);
    ~OpenCVViewer();

    /**
     * Sets a default widget size of 400x300 px.
     *
     * @return the preferred size of the widget.
     */
    QSize sizeHint() const;


    /**
     * Return the current image.
     *
     * This function returns the current image. Note that this is a shallow copy
     * (as per OpenCV's memory management), so any alterations of the returned
     * Mat will reflect in the original Mat being displayed in the OpenGL object.
     *
     * @return a shallow copy of the moest recently capture image.
     */
    cv::Mat getImage();    


    /**
     * Map a point in the widget to the image coordinates.
     *
     * @param the point in the widget coordinate system.
     * @return the point in the image coordinate system.
     */
    cv::Point mapPoint(cv::Point widgetCoords);

public slots:
    /**
     * Set image
     *
     * Overwrites the previous display image and updates the display.
     *
     * @param the image to display.
     */
    void setImage(cv::Mat newImage);

protected:
    void initializeGL(); ///< Used for initializing the OpenGL session.
    void paintGL(); ///< Used everytime the display is refreshed.
    void resizeGL(int width, int height); ///< Used when the widget is resized.
    void mousePressEvent(QMouseEvent *event); ///< Called each time a mouse button is clicked.
    void mouseMoveEvent(QMouseEvent* event); ///< Called when the mouse is moved and mouse tracking is on or a mouse button is down.
    void mouseReleaseEvent(QMouseEvent* event); ///< Called when a mouse button is released.
    void wheelEvent(QWheelEvent *); ///< Called when the mouse wheel is scrolled.

    GLuint texture; ///< The current OpenGL texture. Used for holding images.
    QVector<QVector2D> vertices; ///< The vertices of the texture surface.
    QVector<QVector2D> texCoords; ///< Texture mapping coordinates.
    cv::Mat displayImage; ///< Holds the image to be displayed in the OpenGL-widget.


signals:
    void mouseClicked(QMouseEvent* event); ///< Emitted each time a mouse button is clicked.
    void mouseMoved(QMouseEvent* event); ///< Emitted when the mouse is moved and mouse tracking is on or a mouse button is down.
    void mouseReleased(QMouseEvent* event); ///< Emitted when a mouse button is released.
    void wheelScrolled(QWheelEvent* event); ///< Emitted when the mouse wheel is scrolled.

};

#endif // OPENCVVIEWER_H
