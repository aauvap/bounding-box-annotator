HEADERS       = opencvcapture.h \
				../openCVViewer/opencvviewer.h \
                window.h
SOURCES       = opencvcapture.cpp \
				../openCVViewer/opencvviewer.cpp \
                main.cpp \
                window.cpp
QT           += opengl

LIBS += -lopencv_core \
        -lopencv_highgui

VPATH = ../

include($${VPATH}cvutilities.pri)
