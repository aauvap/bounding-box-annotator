var searchData=
[
  ['setcaptureproperty',['setCaptureProperty',['../classOpenCVCapture.html#a06e443d36ae2b91ef3851589bf13deef',1,'OpenCVCapture']]],
  ['setframerate',['setFrameRate',['../classOpenCVCapture.html#addd8ece1a2a8b92a9054303929ce21ce',1,'OpenCVCapture']]],
  ['setsource',['setSource',['../classOpenCVCapture.html#a477b683118f64beb2a8db43e7a8df114',1,'OpenCVCapture']]],
  ['startcapture',['startCapture',['../classOpenCVCapture.html#af2313b5c78fe18e30835246e020f603f',1,'OpenCVCapture::startCapture()'],['../classOpenCVCapture.html#a02809ecd23876e75b9f73e1f9e418ad8',1,'OpenCVCapture::startCapture(QString source, int frameRate=-1)']]],
  ['stopcapture',['stopCapture',['../classOpenCVCapture.html#ac378e6237ebfe811160c960f2b164042',1,'OpenCVCapture']]]
];
