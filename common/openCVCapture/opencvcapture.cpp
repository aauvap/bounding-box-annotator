#include "opencvcapture.h"

using namespace cv;

OpenCVCapture::OpenCVCapture()
{
	timer = new QTimer();
    frameRate = -1;
    frameNumber = 0;
}

OpenCVCapture::~OpenCVCapture()
{
}


void OpenCVCapture::startCapture()
{
	if(captureSource.isNull() || captureSource.isEmpty()) {
        qDebug() << "No capture source specified.";
		return;
	}

	bool intConvert;
	captureSource.toInt(&intConvert);
	if(intConvert) {
		int source = captureSource.toInt();
		capture.open(source);
        qDebug() << "Number?";
	} else {
		QByteArray ba = captureSource.toLocal8Bit();
		const char *source = ba.data();
        capture.open(source);
        qDebug() << ba;
	}

	if(!capture.isOpened()) {
        qDebug() << "Error initializing capture.";
		return;
	}

    frameNumber = 0;

    //Get an initial frame
    capture >> image;

	//Connect the timer signal with the capture action
	connect(timer, SIGNAL(timeout()), this, SLOT(captureFrame()));

	// Start the timer scheduled for firing according to the frame rate
	// If the frame rate is 0, the timer will not be started.
	if(frameRate < 0) {
		// Attempt to set the frame rate automatically.
		
		timer->start(floor(1.0f/(float)capture.get(cv::CAP_PROP_FPS)*1000.0f));
	} else if(frameRate > 0) {
		timer->start(floor(1.0f/(float)frameRate*1000.0f));
	}

    emit frameCaptured(image);
}

void OpenCVCapture::startCapture(QString source, int frameRate)
{
	setSource(source);
    setFrameRate(frameRate);
	startCapture();
}

void OpenCVCapture::setSource(QString source)
{
    captureSource = source;
}

QString OpenCVCapture::getSource()
{
	return captureSource;
}

void OpenCVCapture::setFrameRate(int rate)
{
	frameRate = rate;
	if(rate == 0) {
		pauseCapture();
	}
}

int OpenCVCapture::getFrameRate()
{
	return frameRate;
}

void OpenCVCapture::stopCapture()
{
	timer->stop();
	capture.release();
    frameNumber = 0;
}

void OpenCVCapture::pauseCapture()
{
	timer->stop();
}

void OpenCVCapture::resumeCapture()
{
	//Start the timer scheduled for firing according to the frame rate
	if(frameRate < 0) {
		// Attempt to set the frame rate automatically.
		timer->start(floor(1.0f/(float)capture.get(cv::CAP_PROP_FPS)*1000.0f));
	} else {
		timer->start(floor(1.0f/(float)frameRate*1000.0f));
	}
}

void OpenCVCapture::advanceCapture()
{
    if((frameRate == 0 && capture.isOpened()) || isPaused()) {
		captureFrame();
	}
}

void OpenCVCapture::grab()
{
	if(frameRate == 0 && capture.isOpened()) {
		capture.grab();
        frameNumber++;
	}
}

void OpenCVCapture::loadImage(int loadFlag)
{
	stopCapture();

	QByteArray ba = captureSource.toLocal8Bit();
	const char *source = ba.data();
    image = cv::imread(source, loadFlag);

    emit frameCaptured(image);
}

void OpenCVCapture::loadImage(QString source, int loadFlag)
{
    setSource(source);
	loadImage(loadFlag);
}

Mat OpenCVCapture::getImage()
{
	return image;
}

double OpenCVCapture::getCaptureProperty(int propId)
{
	return this->capture.get(propId);
}

bool OpenCVCapture::setCaptureProperty(int propId, double value)
{
	return this->capture.set(propId, value);
}

bool OpenCVCapture::isOpened()
{
	return capture.isOpened();
}

bool OpenCVCapture::isStopped()
{
    return !timer->isActive() && frameNumber == 0;
}

bool OpenCVCapture::isPaused()
{
    return !timer->isActive() && frameNumber != 0;
}

int OpenCVCapture::getFrameNumber()
{
    return frameNumber;
}

int OpenCVCapture::jumpToFrame(int frameNum)
{
    if(frameNum > getCaptureProperty(cv::CAP_PROP_FRAME_COUNT)) {
        frameNum = getCaptureProperty(cv::CAP_PROP_FRAME_COUNT);
    }

    setCaptureProperty(cv::CAP_PROP_POS_FRAMES, frameNum);

    return getCaptureProperty(cv::CAP_PROP_POS_FRAMES);
}


void OpenCVCapture::captureFrame()
{
    capture >> image;
    if(image.empty()) {
        stopCapture();
        return;
    }

    frameNumber++;
    emit frameCaptured(image);
}
