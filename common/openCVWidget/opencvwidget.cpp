#include <QtGui>
#include <QtOpenGL>

#include "opencvwidget.h"

using namespace cv;

OpenCVWidget::OpenCVWidget(QWidget *parent, QGLWidget *shareWidget)
	: QGLWidget(parent, shareWidget)
{
	timer = new QTimer();
	frameRate = -1;
	autoDisplay = true;
}

OpenCVWidget::~OpenCVWidget()
{
}

QSize OpenCVWidget::sizeHint() const
{
	return QSize(400, 300);
}

void OpenCVWidget::startCapture()
{
	if(captureSource.isNull() || captureSource.isEmpty()) {
		QMessageBox::critical(this,"Error","No capture source specified.");
		return;
	}

	bool intConvert;
	captureSource.toInt(&intConvert);
	if(intConvert) {
		int source = captureSource.toInt();
		capture.open(source);
	} else {
		QByteArray ba = captureSource.toLocal8Bit();
		const char *source = ba.data();
		capture.open(source);
	}

	if(!capture.isOpened()) {
		QMessageBox::critical(this,"Error","Error initializing capture.");
		return;
	}

	//Get an initial frame from the webcam
	capture >> image;

	resizeGL(this->width(), this->height()); // Adjust the viewport for the new image.

	//Connect the timer signal with the capture action
	connect(timer, SIGNAL(timeout()), this, SLOT(captureFrame()));

	// Start the timer scheduled for firing according to the frame rate
	// If the frame rate is 0, the timer will not be started.
	if(frameRate < 0) {
		// Attempt to set the frame rate automatically.
		timer->start(floor(1.0f/(float)capture.get(CV_CAP_PROP_FPS)*1000.0f));
	} else if(frameRate > 0) {
		timer->start(floor(1.0f/(float)frameRate*1000.0f));
	}

	// Since the initial frame was already captured, emit the signal and auto update, if specified:
	if(autoDisplay) {
		updateDisplay();
	}
	emit frameCaptured();
}

void OpenCVWidget::startCapture(QString source, int frameRate, bool autoDisplay)
{
	setSource(source);
	setFrameRate(frameRate);
	enableAutoDisplay(autoDisplay);
	startCapture();
}

void OpenCVWidget::setSource(QString source)
{
	captureSource = source;
}

QString OpenCVWidget::getSource()
{
	return captureSource;
}

void OpenCVWidget::setFrameRate(int rate)
{
	frameRate = rate;
	if(rate == 0) {
		pauseCapture();
	}
}

int OpenCVWidget::getFrameRate()
{
	return frameRate;
}

void OpenCVWidget::stopCapture()
{
	timer->stop();
	capture.release();
}

void OpenCVWidget::pauseCapture()
{
	timer->stop();
}

void OpenCVWidget::resumeCapture()
{
	//Start the timer scheduled for firing according to the frame rate
	if(frameRate < 0) {
		// Attempt to set the frame rate automatically.
		timer->start(floor(1.0f/(float)capture.get(CV_CAP_PROP_FPS)*1000.0f));
	} else {
		timer->start(floor(1.0f/(float)frameRate*1000.0f));
	}
}

void OpenCVWidget::advanceCapture()
{
	if(frameRate == 0 && capture.isOpened()) {
		captureFrame();
	}
}

void OpenCVWidget::grab()
{
	if(frameRate == 0 && capture.isOpened()) {
		capture.grab();
	}
}

void OpenCVWidget::loadImage(int loadFlag)
{
	stopCapture();

	QByteArray ba = captureSource.toLocal8Bit();
	const char *source = ba.data();
	image = cv::imread(source, loadFlag);
	resizeGL(this->width(), this->height());
	if(autoDisplay) {
		updateDisplay();
	}
	emit frameCaptured();

	//Draw the scene
	glDraw();
}

void OpenCVWidget::loadImage(QString source, bool autoDisplay, int loadFlag)
{
	setSource(source);
	enableAutoDisplay(autoDisplay);
	loadImage(loadFlag);
}

Mat OpenCVWidget::getImage()
{
	return image;
}

void OpenCVWidget::enableAutoDisplay(bool value)
{
	autoDisplay = value;
}

bool OpenCVWidget::getAutoDisplay()
{
	return autoDisplay;
}

void OpenCVWidget::updateDisplay()
{
	displayImage = image.clone();
}

void OpenCVWidget::initializeGL()
{
	// Create the surface we will use for the texture:
	static const int coords[4][3] = { { +1, -1 }, { -1, -1 }, { -1, +1 }, { +1, +1 } };
	for (int j = 0; j < 4; ++j) {
		/* A note about texture coordinates:
		  OpenCV uses a nice, sane coordinate system with origin in the upper left corner.
		  Just like any other image processing tool (let's just forget the fact that math-wise
		  that is silly).
		  OpenGL, however, uses a math-inspired coordinate system with origin in the lower
		  left.
		  Right here, the texture is mapped, so the image is automatically flipped in the y-
		  direction. Better do it here, than actually flipping the image elsewhere.
		*/
		texCoords.append(QVector2D(j == 0 || j == 3, j == 2 || j == 3));
		vertices.append(QVector2D(coords[j][0], coords[j][1]));
	}

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);
}

void OpenCVWidget::paintGL()
{
	if(displayImage.empty()) {
		displayImage = Mat::zeros(1, 1, CV_8UC3); // Paint a black background until we have something to show.
	}

	qglClearColor(Qt::black); // Create a nice, black background for the parts of the widget with no image.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();

	glVertexPointer(2, GL_FLOAT, 0, vertices.constData());
	glTexCoordPointer(2, GL_FLOAT, 0, texCoords.constData());
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);


	// Below are three different methods of mapping the image as a texture. Use only one.
	// Basically, stick to method 3 unless you have very good reason for not doing that.

	// Method 1:
/*
	This method binds the texture using mipmaps for smooth resizing. However, this means that a new
	mipmap must be calculated for each frame. This is slow (OpenGL is built for using static textures
	where the mipmaps are simple pre-calculated at the start of the program), so while it works it is
	so slow that I cannot recommend this approach.

	// Additions from http://www.nullterminator.net/gltexture.html:
	glGenTextures(1, &texture); // Allocate a texture name.
	glBindTexture(GL_TEXTURE_2D, texture); // Select our current texture.
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); // Select modulate to mix texture with color for shading.
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST); // When texture area is small, use the closest mipmap.
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); // When texture area is large, use the first mipmap.
	gluBuild2DMipmaps( GL_TEXTURE_2D, 3, displayImage.cols, displayImage.rows, GL_BGR, GL_UNSIGNED_BYTE, displayImage.data); // Build our texture mipmaps from the raw OpenCV image data.
	// End of additions
*/

	// Method 2:
/*
	Alternative way, going via a QPixmap. Do not use, it is slow.
	Remaining here to maintain my sanity, should the other solutions break.
	texture = bindTexture(QPixmap(QString("side1.png")), GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
*/

	// Method 3:
	// Non-mipmap way of mapping the texture (fast and clean):
	glGenTextures(1, &texture); // Allocate a texture name.
	glBindTexture(GL_TEXTURE_2D, texture); // Select our current texture.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // When the texture area is larger then the image, upscale using linear interpolation.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // When the texture area is smaller than the image, downsample using linear interpolation.
	glTexImage2D(GL_TEXTURE_2D, 0, 3, displayImage.cols, displayImage.rows, 0, GL_BGR, GL_UNSIGNED_BYTE, displayImage.data);

	// End of different methods, the last few lines are common for all methods.

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4); // Draw it!

	glDeleteTextures(1, &texture);

}

void OpenCVWidget::resizeGL(int width, int height)
{
	// Make sure the image keeps its aspect ratio, regardless of widget size:
	// (also, center it in the widget)
	float imgRatio = (float)image.cols/(float)image.rows;
	float windowRatio = (float)width/(float)height;
	if(windowRatio < imgRatio) {
		glViewport(0, (height-width/imgRatio)/2, width, width/imgRatio);
	} else {
		glViewport((width-height*imgRatio)/2, 0, height*imgRatio, height);
	}

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.0, +1.0, +1.0, -1.0, 0.0, 1.0);

	glMatrixMode(GL_MODELVIEW);
}

void OpenCVWidget::captureFrame()
{
	//Get an image from the webcam
	capture >> image;
	if(image.empty()) {
		stopCapture();
		return;
	}
	if(autoDisplay) {
		updateDisplay();
	}

	emit frameCaptured();

	//Draw the scene
	glDraw();
}

double OpenCVWidget::getCaptureProperty(int propId)
{
	return this->capture.get(propId);
}

bool OpenCVWidget::setCaptureProperty(int propId, double value)
{
	return this->capture.set(propId, value);
}

bool OpenCVWidget::isOpened()
{
	return capture.isOpened();
}

void OpenCVWidget::mousePressEvent(QMouseEvent* event)
{
	emit mouseClicked(event);
}

void OpenCVWidget::mouseMoveEvent(QMouseEvent* event)
{
	emit mouseMoved(event);
}

cv::Point OpenCVWidget::mapPoint(cv::Point widgetCoords)
{
	if(image.empty()) { // Bail out if we have no image to map to.
		return cv::Point(0,0);
	}

	cv::Point mappedPoint;
	float viewportRatio = (float)this->width()/(float)this->height();
	float imageRatio = (float)image.cols/(float)image.rows;

	mappedPoint.x = (int)((float)widgetCoords.x/(float)this->width()*(float)image.cols);
	mappedPoint.y = (int)((float)widgetCoords.y/(float)this->height()*(float)image.rows);
	if(viewportRatio > imageRatio) { // The viewport has black bars on the sides.
		int imageWidth = image.cols*(float)this->height()/(float)image.rows;
		int offset = (this->width()-imageWidth)/2;
		mappedPoint.x = image.cols*((float)(widgetCoords.x-offset)/(float)imageWidth);
	} else if(viewportRatio < imageRatio) { // The viewport has black bars on top and bottom.
		int imageHeight = image.rows*(float)this->width()/(float)image.cols;
		int offset = (this->height()-imageHeight)/2;
		mappedPoint.y = image.rows*((float)(widgetCoords.y-offset)/(float)imageHeight);
	}

	return mappedPoint;
}
