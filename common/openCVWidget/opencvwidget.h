#ifndef OPENCVWIDGET_H
#define OPENCVWIDGET_H

#include <QtGui>
#include <QGLWidget>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <math.h>

// On Windows, GL_BGR is not defined by default. Fix that:
#ifndef GL_BGR
#define GL_BGR 0x80E0
#endif

class QGLShaderProgram;

/**
 * Widget to captures frames or single images using OpenCV and display them using OpenGL.
 *
 * This class integrates OpenCV and Qt. It is a widget that can be added to
 * your GUI and it can handle both the capture and display of video and single
 * images.
 *
 * The class is implemented with a double buffer system: One cv::Mat holds the most
 * recently captured image (hereafter called capture image) and another holds the image
 * that is currently being displayed in the widget (the display image). This allows you
 * to perform processing on the capture image before showing it, by first calling
 * getImage(), then altring it, and finally calling updateDisplay(). If you simply want
 * to show it right away, enable auto display (using enableAutoDisplay()).
 */
class OpenCVWidget : public QGLWidget
{
    Q_OBJECT

public:
	/**
	 * Constructor.
	 *
	 * Creates a new OpenCVWidget. Be aware that the capture source must be
	 * set before any capture can commence. See setSource().
	 */
	OpenCVWidget(QWidget *parent = 0, QGLWidget *shareWidget = 0);
	~OpenCVWidget();

	/**
	 * Sets a default widget size of 400x300 px.
	 *
	 * @return the preferred size of the widget.
	 */
	QSize sizeHint() const;

	/**
	 * Start a capture.
	 *
	 * When the capture source is set, use this method to begin capturing from
	 * a video source using OpenCV's VideoCapture. Whenever a new frame is
	 * captured, the signal frameCaptured() is emitted.
	 */
	void startCapture();

	/**
	 * Start a capture while setting the source and frame rate.
	 *
	 * An overloaded version of startCapture() that allows you to set the source
	 * and frame rate while starting the capture.
	 *
	 * @param source the source identifier (see setSource()).
	 * @param frameRate the desired frame rate. If frameRate is < 0 the program will attempt to determine the frame rate automatically from the input file. If frameRate is == 0, no timer will be started. Instead, use advanceCapture() to manually frab the frames.
	 * @param autoDisplay set true to automatically refresh the display, when a new frame is obtained. Set to false if you want to process the image before displaying it.
	 */
	void startCapture(QString source, int frameRate = -1, bool autoDisplay = true);

	/**
	 * Set the video source.
	 *
	 * Usually a file name of the video you want to process. If only an integer is
	 * provided, the source is the connected camera with that number.
	 */
	void setSource(QString source);

	/**
	 * Return the capture source.
	 *
	 * @return the string defining the current capture source.
	 */
	QString getSource();

	/**
	 * Set desired frame rate.
	 *
	 * @param frameRate the desired frame rate. If frameRate is < 0 the program will attempt to determine the frame rate automatically from the input file.
	 */
	void setFrameRate(int rate);

	/**
	 * Retrive the set frame rate.
	 *
	 * Note that this <b>does not</b> return the frame rate the camera or video source
	 * uses internally, only the frame rate this widget will try to obtain pictures
	 * with.
	 *
	 * @return the current frame rate of the capture timer. If frameRate is < 0 the program will attempt to determine the frame rate automatically from the input file.
	 */
	int getFrameRate();

	/**
	 * Stop the capture.
	 *
	 * This stops the internal capture timer and releases the VideoCapture object. The
	 * current image will stay in memory until overwritten when a capture is started again.
	 */
	void stopCapture();

	/**
	 * Pause the capture.
	 *
	 * Pause the internal capture timer.
	 */
	void pauseCapture();

	/**
	 * Restart the capture.
	 *
	 * Resume the internal capture timer. For video input, this resumes where the video
	 * was paused, for live camera input, this resumes at the current frame.
	 */
	void resumeCapture();

	/**
	 * Manually read next frame in a stream.
	 *
	 * If no timer is set, this function can be used to manually read the next frame.
	 * This functions has no effect unless the frame rate is set to 0.
	 */
	void advanceCapture();

	/**
	 * Manually grab next frame.
	 *
	 * Use to seek to a specific point in a video (when setProperty() cannot be used
	 * due to bug 1419 in OpenCV. Remember that this does not retrieve the frame,
	 * so be sure to call advanceCapture() after calling this.
	 * This functions has no effect unless the frame rate is set to 0.
	 */
	void grab();

	/**
	 * Load a single image.
	 *
	 * Instead of using OpenCV's VideoCapture to capture a stream of images, this method
	 * uses imread() to load a single image. The image stays in memory until overwritten
	 * by a new capture or a new
	 */
	void loadImage(int loadFlag = 1);

	/**
	 * Load a single image from the specified source.
	 *
	 * Overloaded version of loadImage(). Allows you to pass the file name in and load
	 * it in a single step.
	 *
	 * @param source the name of the file to load. This will be set as the capture source.
	 * @param if true, the loaded image is displayed immidiately.
	 */
	void loadImage(QString source, bool autoDisplay = true, int loadFlag = 1);

	/**
	 * Return the current image.
	 *
	 * This function returns the current image for further processing elsewhere. Note that
	 * this is a shallow copy (as per OpenCV's memory management), so any alterations of
	 * the returned Mat will reflect in the original Mat being displayed in the OpenGL
	 * object.
	 *
	 * @return a shallow copy of the moest recently capture image.
	 */
	cv::Mat getImage();

	/**
	 * Set the auto display setting.
	 *
	 * If auto display is enabled, the most recently captured frame is automatically
	 * displayed in the OpenGL-window. If not, updateDisplay() must be called.
	 *
	 * @param value set true to enable auto display, false to disable.
	 */
	void enableAutoDisplay(bool value);

	/**
	 * Get the auto display setting.
	 *
	 * See enableAutoDisplay()
	 *
	 * @return the current auto display setting.
	 */
	bool getAutoDisplay();

	/**
	 * Update display
	 *
	 * Copies the most recently captured image into the display image, in effect showing
	 * the most recently captures image at next render. This also overwrites the previous
	 * display image. See the class description for further detail.
	 */
	void updateDisplay();

	/**
	 * Get a property
	 *
	 * Get any property from the capture device/file, as specified in
	 * http://opencv.itseez.com/modules/highgui/doc/reading_and_writing_images_and_video.html#videocapture-get
	 *
	 * @param the desired property.
	 *
	 * @return the value of the requested property.
	 */
	double getCaptureProperty(int propId);

	/**
	 * Set a property
	 *
	 * Set any property from the capture device/file, as specified in
	 * http://opencv.itseez.com/modules/highgui/doc/reading_and_writing_images_and_video.html#videocapture-set
	 *
	 * @param the desired property.
	 * @param the desired value.
	 *
	 * @return the result as returned by OpenCV.
	 */
	bool setCaptureProperty(int propId, double value);

	/**
	 * Tell whether a video capture is opened.
	 *
	 * @return true if a capture is opened (it may be paused), false if not.
	 */
	bool isOpened();

	/**
	 * Map a point in the widget to the image coordinates.
	 *
	 * @param the point in the widget coordinate system.
	 * @return the point in the image coordinate system.
	 */
	cv::Point mapPoint(cv::Point widgetCoords);

protected:
	void initializeGL(); ///< Used for initializing the OpenGL session.
	void paintGL(); ///< Used everytime the display is refreshed.
	void resizeGL(int width, int height); ///< Used when the widget is resized.
	void mousePressEvent(QMouseEvent *event); ///< Called whenever a mousebutton is pressed.
	void mouseMoveEvent(QMouseEvent* event); ///< Called when the mouse is moved and mouse tracking is on.

private:
	GLuint texture; ///< The current OpenGL texture. Used for holding images.
	QVector<QVector2D> vertices; ///< The vertices of the texture surface.
	QVector<QVector2D> texCoords; ///< Texture mapping coordinates.
	QTimer *timer; ///< Timer used for setting capture frame rate.
	cv::VideoCapture capture; ///< Capture object for video.
	cv::Mat image; ///< Holds the most recently captured image.
	cv::Mat displayImage; ///< Holds the image to be displayed in the OpenGL-widget.
	int frameRate; ///< Holds the frame rate that controls the timer.
	QString captureSource; ///< Specifies the source identifier/file name.
	bool autoDisplay; ///< Determines if the most recent image is automatically displayed.

private slots:
	void captureFrame(); ///< Captures a new frame when the timer asks it to.

signals:
	void frameCaptured(); ///< Emitted each time a new frame is captured.
	void mouseClicked(QMouseEvent* event); ///< Emitted each time the mouse is clicked.
	void mouseMoved(QMouseEvent* event); ///< Emitted when the mouse is moved and mouse tracking is on.

};

#endif
