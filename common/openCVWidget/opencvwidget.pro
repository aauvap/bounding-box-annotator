HEADERS       = opencvwidget.h \
                window.h
SOURCES       = opencvwidget.cpp \
                main.cpp \
                window.cpp
QT           += opengl

LIBS += -lopencv_core \
        -lopencv_highgui
