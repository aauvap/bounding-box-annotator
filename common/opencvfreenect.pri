HEADERS       *= openCVFreenect/opencvfreenect.h
SOURCES       *= openCVFreenect/opencvfreenect.cpp

LIBS *= -lopencv_core \
		-lopencv_highgui \
		-lfreenect \
		-lpthread

INCLUDEPATH *= $${VPATH}openCVFreenect/
