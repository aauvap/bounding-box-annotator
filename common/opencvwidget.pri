HEADERS       *= openCVWidget/opencvwidget.h
SOURCES       *= openCVWidget/opencvwidget.cpp
QT           *= opengl

LIBS *= -lopencv_core \
                -lopencv_highgui


INCLUDEPATH *= openCVWidget/
