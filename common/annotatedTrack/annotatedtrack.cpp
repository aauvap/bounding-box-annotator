#include "annotatedtrack.h"

AnnotatedTrack::AnnotatedTrack()
{

}

AnnotatedTrack::AnnotatedTrack(QString csv)
{
	setFromCSV(csv);
}

AnnotatedTrack::AnnotatedTrack(QString type, QString originFile, unsigned long firstFrame, unsigned int length, int frameWidth, int frameHeight, QString path)
{
	setType(type);
	setOriginFile(originFile);
	setFirstFrame(firstFrame);
	setLength(length);
	
	filename = QString("%1/%2_%3.avi").arg(path).arg(type).arg(QDateTime::currentDateTime().toTime_t());
    writer = cv::VideoWriter(filename.toStdString(), cv::VideoWriter::fourcc('X','V','I','D'), 30, cv::Size(frameWidth, frameHeight));
}

QString AnnotatedTrack::getFilename()
{
	return filename;
}

QString AnnotatedTrack::getType()
{
	return type;
}

void AnnotatedTrack::setType(QString type)
{
	this->type = type;
}

QString AnnotatedTrack::getOriginFile()
{
	return originFile;
}

void AnnotatedTrack::setOriginFile(QString file)
{
	this->originFile = file;
}

unsigned long AnnotatedTrack::getFirstFrame()
{
	return firstFrame;
}

void AnnotatedTrack::setFirstFrame(unsigned long frame)
{
	this->firstFrame = frame;
}

unsigned int AnnotatedTrack::getLength()
{
	return length;
}

void AnnotatedTrack::setLength(unsigned int length)
{
	this->length = length;
}

void AnnotatedTrack::writeFrame(cv::Mat frame)
{
	writer.write(frame);
}

bool AnnotatedTrack::setFromCSV(QString csv)
{
	QStringList data;
	data << csv.split(";");
	bool result;

	if(data.size() != 5) {
		return false;
	}

	filename = data[0];
	setType(data[1]);
	setOriginFile(data[2]);
	setFirstFrame(data[3].toInt(&result));
	if(!result) {
		return false;
	}
	setLength(data[4].toInt(&result));
	if(!result) {
		return false;
	}
	return true;
}

QString AnnotatedTrack::getCSV(int currentFrame)
{
	QString csv;
	csv.append(QFileInfo(getFilename()).fileName());
	csv.append(";");
	csv.append(getType());
	csv.append(";");
	csv.append(getOriginFile());
	csv.append(";");
	csv.append(QString("%1").arg(getFirstFrame()));
	csv.append(";");
	csv.append(QString("%1").arg(currentFrame-getFirstFrame()));
	csv.append("\n");
	return csv;
}
