#ifndef ANNOTATEDTRACK_H
#define ANNOTATEDTRACK_H

#include <QString>
#include <QDateTime>
#include <QString>
#include <QStringList>
#include <QFileInfo>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

class AnnotatedTrack
{
public:
    AnnotatedTrack();
	AnnotatedTrack(QString csv);
	AnnotatedTrack(QString type, QString originFile, unsigned long firstFrame, unsigned int length, int frameWidth, int frameHeight, QString path = "");

	QString getFilename();
	QString getType();
	void setType(QString type);
	QString getOriginFile();
	void setOriginFile(QString file);
	unsigned long getFirstFrame();
	void setFirstFrame(unsigned long frame);
	unsigned int getLength();
	void setLength(unsigned int length);

	bool setFromCSV(QString csv);
	QString getCSV(int currentFrame);

	void writeFrame(cv::Mat frame);

private:
	QString filename;
	QString type;
	QString originFile;
	unsigned long firstFrame;
	unsigned int length;
	cv::VideoWriter writer;
};

#endif // ANNOTATEDTRACK_H
