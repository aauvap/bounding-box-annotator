#include "autocompletedialog.h"
#include "ui_autocompletedialog.h"

AutoCompleteDialog::AutoCompleteDialog(QWidget *parent, QStringList wordList, QString title) :
    QDialog(parent),
	ui(new Ui::AutoCompleteDialog)
{
    ui->setupUi(this);

	this->wordList = wordList;

	if(wordList.length() > 0) {
		ui->wordList->addItems(wordList);
	}

	this->setWindowTitle(title);
}

AutoCompleteDialog::~AutoCompleteDialog()
{
    delete ui;
}

void AutoCompleteDialog::accept()
{
	word = ui->wordEdit->text();
	done(QDialog::Accepted);
}

QString AutoCompleteDialog::getWord()
{
	return word;
}

void AutoCompleteDialog::on_wordList_itemSelectionChanged()
{
	if(ui->wordList->selectedItems().length() > 0) {
		ui->wordEdit->setText(ui->wordList->selectedItems()[0]->text());
	}
}

void AutoCompleteDialog::on_wordEdit_textEdited(QString text)
{
	int cursorPos = ui->wordEdit->cursorPosition();
	if(cursorPos == 0) {
		ui->wordEdit->setText("");
		return;
	}

	QString matchString;
	matchString = QString("^%1").arg(text);
	matchString.truncate(cursorPos+1);

	QRegExp regex(matchString);

	QStringList matches = wordList.filter(regex);

	if(matches.length() > 0) {
		ui->wordEdit->setText(matches[0]);
	} else {
		matchString.remove(0,1);
		ui->wordEdit->setText(matchString);
	}
	ui->wordEdit->setCursorPosition(cursorPos);
}
