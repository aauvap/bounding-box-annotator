#ifndef OpenNICAPTURE_H
#define OpenNICAPTURE_H

#include <libfreenect.hpp>
#include <QObject>
#include <QDebug>
#include "opencvcapture.h"


/**
 * Class for obtaining input from Kinect.
 *
 * This is built on the OpenCVCapture class and introduces
 * just one new function: getDepthImage(), which is used to
 * access the Kinect's 3D data. Other than that it works
 * exactly like OpenCVCapture, so see its documentation for
 * further information.
 *
 * This is built on the OpenNI driver. If you wish to use
 * libfreenect, see kinectCVCapture.
 */
class OpenNICapture : public OpenCVCapture
{
    Q_OBJECT

public:
    using OpenCVCapture::startCapture;

    OpenNICapture();
    ~OpenNICapture();

    void startCapture();
    cv::Mat getDepthImage();
    cv::Mat getRawDepth();
    cv::Mat getPointCloud();
    void advanceCapture();

private:
    cv::Mat depthMat;
    cv::Mat depthf;
    cv::Mat pointCloud;

private slots:
    void captureFrame();
};

#endif // OpenNICAPTURE_H
