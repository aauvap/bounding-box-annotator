#include "opennicapture.h"

using namespace cv;

OpenNICapture::OpenNICapture() : OpenCVCapture::OpenCVCapture()
{
}

OpenNICapture::~OpenNICapture()
{
}

void OpenNICapture::startCapture() {
    if(captureSource == "kinect") {
        qDebug() << "Initializing Kinect using OpenNI...";

        capture.open(CV_CAP_OPENNI);

        if(!capture.isOpened()) {
            qDebug() << "Kinect could not be opened.";
        } else {
            qDebug() << "Kinect open.";
        }

        //Get an initial frame
        capture.retrieve(image, CV_CAP_OPENNI_BGR_IMAGE);
        capture.retrieve(depthMat, CV_CAP_OPENNI_DEPTH_MAP);
        depthMat.convertTo(depthf, CV_8UC1, 255.0/2048.0);

        //Connect the timer signal with the capture action
        connect(timer, SIGNAL(timeout()), this, SLOT(captureFrame()));

        // Start the timer scheduled for firing according to the frame rate
        // If the frame rate is 0, the timer will not be started.
        if(frameRate > 0) {
            timer->start(floor(1.0f/(float)frameRate*1000.0f));
        } else {
            frameRate = 0;
        }

        firstFrame = image.clone();

        emit frameCaptured(image);
    } else {
        OpenCVCapture::startCapture();
    }
}

void OpenNICapture::captureFrame()
{
    capture.grab();
    capture.retrieve(image, CV_CAP_OPENNI_BGR_IMAGE);
    capture.retrieve(depthMat, CV_CAP_OPENNI_DEPTH_MAP);
    capture.retrieve(pointCloud, CV_CAP_OPENNI_POINT_CLOUD_MAP);
    depthMat.convertTo(depthf, CV_8UC1, 255.0/10000.0);

    if(firstFrame.empty()) {
        firstFrame = image.clone();
    }

    //qDebug() << image.type() << " " << firstFrame.type() << " - " << image.channels() << " " << firstFrame.channels() << " - ";

    std::vector<Mat> imageChannels, firstFrameChannels;
    split(image, imageChannels);
    split(firstFrame, firstFrameChannels);

    // Stop the capture if we have no image, or the image is identical to the first frame (to prevent loops).
    if(image.empty() || (countNonZero(imageChannels[0] != firstFrameChannels[0]) == 0
                         && countNonZero(imageChannels[1] != firstFrameChannels[1]) == 0
                         && countNonZero(imageChannels[2] != firstFrameChannels[2]) == 0
                         && frameNumber > 0)) {
        stopCapture();
        return;
    }

    frameNumber++;
    emit frameCaptured(image);
}

Mat OpenNICapture::getDepthImage()
{
    return depthf;
}

Mat OpenNICapture::getRawDepth()
{
    return depthMat;
}

Mat OpenNICapture::getPointCloud()
{
    return pointCloud;
}

void OpenNICapture::advanceCapture()
{
    if(frameRate == 0) {
        captureFrame();
    }
}
