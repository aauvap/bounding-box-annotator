HEADERS       *= openCVCapture/opencvcapture.h
SOURCES       *= openCVCapture/opencvcapture.cpp

!win32{
    LIBS *= -lopencv_core \
                -lopencv_highgui
}

win32{
    INCLUDEPATH += "C:/OpenCV/release/install/include"
    LIBS += C:/OpenCV/release/bin/*.dll
}

INCLUDEPATH *= $${VPATH}openCVCapture/
