#ifndef OPENCVFREENECT_H
#define OPENCVFREENECT_H

#include <libfreenect.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <pthread.h>
#include <iostream>


class Mutex {
public:
	Mutex();
	void lock();
	void unlock();
private:
	pthread_mutex_t m_mutex;
};

class OpenCVFreenect : public Freenect::FreenectDevice
{
public:
	OpenCVFreenect(freenect_context *_ctx, int _index);
	void VideoCallback(void* _rgb, uint32_t timestamp);
	void DepthCallback(void* _depth, uint32_t timestamp);
	bool getVideo(cv::Mat& output);
	bool getDepth(cv::Mat& output);

private:
	std::vector<uint8_t> m_buffer_depth;
	std::vector<uint8_t> m_buffer_rgb;
	std::vector<uint16_t> m_gamma;
    bool m_new_rgb_frame;
    bool m_new_depth_frame;
	cv::Mat depthMat;
	cv::Mat rgbMat;
	cv::Mat ownMat;
	Mutex m_rgb_mutex;
    Mutex m_depth_mutex;
};

#endif // OPENCVFREENECT_H
