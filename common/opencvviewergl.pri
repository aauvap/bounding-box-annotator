HEADERS       *= openCVViewerGL/opencvviewer.h
SOURCES       *= openCVViewerGL/opencvviewer.cpp
QT           *= opengl

!win32{
    LIBS *= -lopencv_core \
                -lopencv_highgui
}

win32{
    INCLUDEPATH += C:\\opencv2.3.1\\install\\include

    LIBS *= C:/opencv2.3.1/install/bin/libopencv_core231.dll \
        C:/opencv2.3.1/install/bin/libopencv_highgui231.dll
}

INCLUDEPATH *= $${VPATH}openCVViewerGL/
