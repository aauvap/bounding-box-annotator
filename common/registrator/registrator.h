// <copyright file="registrator.h" company="Aalborg University">
// Copyright (c) 2014 All Right Reserved, http://vap.aau.dk/
//
// This source is subject to the MIT Licence
// Copyright (c) <2014> <Aalborg University>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// </copyright>
// <author>Chris Bahnsen</author>
// <email>cb@create.aau.dk</email>
// <email2>chris.bahnsen@hotmail.com</email2>
// <date>2014-11-10</date>
// <summary>Contains the base functions for registering trimodal imagery by use of a calibVars.yml function</summary>

#ifndef MULTIMODALREGISTRATOR_H
#define MULTIMODALREGISTRATOR_H

#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>

#include <QMessageBox>
#include <QDebug>

#ifdef Q_OS_WIN32
#include <windirent.h> // Untill c++17 arrives, we will use dirent
#else
#include <dirent.h> // Untill c++17 arrives, we will use dirent
#endif

#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#define MAP_FROM_RGB 0
#define MAP_FROM_THERMAL 1
#define MAP_FROM_DEPTH 2


class Registrator
{
public:
	Registrator();

	/* Functions for the pixel-to-pixel registration of tri-modal registration. All registration functions accepts a vector of points as input
	and might therefore be called with multiple points at once

	The vector of points is created as:
	std::vector<cv::Point2f> vecRgbCoord;
	vecRgbCoord.push_back(Point2f(x,y));

	*/

	/* computeCorrespondingThermalPointFromRgb handles the registration of RGB points into the corresponding thermal point.
	In order to provide the registration, the function takes as input the corresponding depth coordinate, which might be computed
	from the function computeCorrespondingDepthPointFromRgb */
	void computeCorrespondingThermalPointFromRgb(std::vector<cv::Point2f> vecRgbCoord, std::vector<cv::Point2f>& vecTCoord, std::vector<cv::Point2f> vecDCoord);

	void computeCorrespondingThermalPointFromRgb(std::vector<cv::Point2f> vecRgbCoord, std::vector<cv::Point2f>& vecTCoord, std::vector<cv::Point2f> vecDCoord,
		std::vector<int> &bestHom);

	/* Full overload of computeCorrespondingThermalPointFromRgb containing full information of the internal functions for debugging purposes */
	void computeCorrespondingThermalPointFromRgb(std::vector<cv::Point2f> vecRgbCoord, std::vector<cv::Point2f>& vecTCoord, std::vector<cv::Point2f> vecDCoord,
		std::vector<int> vecDepthInMm, std::vector<double>& minDist, std::vector<int> &bestHom,
		std::vector<std::vector<int> > &octantIndices, std::vector<std::vector<double> > &octantDistances,
		std::vector<cv::Point3f> &worldCoordPointvector);

	/* computeCorrespondingDepthPointFromRgb handles the registration of RGB points into the corresponding point in the depth image
	As the function uses a look-up-table for the registration, only the RGB point needs to be provided */
	void computeCorrespondingDepthPointFromRgb(std::vector<cv::Point2f> vecRgbCoord, std::vector<cv::Point2f> & vecDCoord);

	/* computeCorrespondingDepthPointFromRgb handles the registration of depth points into the corresponding point in the RGB image
	As the function uses a look-up-table for the registration, only the depth point needs to be provided */
	void computeCorrespondingRgbPointFromDepth(std::vector<cv::Point2f> vecDCoord, std::vector<cv::Point2f> & vecRgbCoord);

	/* computeCorrespondingRgbPointFromThermal handles the registration of thermal points (in the thermal modality). The registration is handled without providing any
	depth coordinate, as the thermal camera does not contain any direct connection to the depth information of the Kinect camera*/
	void computeCorrespondingRgbPointFromThermal(std::vector<cv::Point2f> vecTCoord, std::vector<cv::Point2f>& vecRgbCoord);

	void computeCorrespondingRgbPointFromThermal(std::vector<cv::Point2f> vecTCoord, std::vector<cv::Point2f>& vecRgbCoord, std::vector<double>& minDist,
		std::vector<int> &bestHom, std::vector<std::vector<int> > &octantIndices, std::vector<std::vector<double> > &octantDistances);

	/* Full overload of computeCorrespondingRgbPointFromThermal containing full information of the internal functions for debugging purposes */
	void computeCorrespondingRgbPointFromThermal(std::vector<cv::Point2f> vecTCoord, std::vector<cv::Point2f>& vecRgbCoord, std::vector<double>& minDist,
		std::vector<int> &bestHom, std::vector<std::vector<int> > &octantIndices, std::vector<std::vector<double> > &octantDistances,
		std::vector<cv::Point3f> &worldCoordPointstdvector);

	void loadMinCalibrationVars(std::string calFile);

	void loadHomography(std::string calFile);

	// If the provided images or coordinates are undistorted, this variable should be true. Otherwise, false
	void toggleUndistortion(bool undistort);

	// Use for debugging and optimization purposes
	void setDiscardedHomographies(std::vector<int> discardedHomographies);
	void setUsePrevDepthPoint(bool value = true);

	int drawRegisteredContours(cv::Mat &rgbContourImage, cv::Mat& depthContourImage, cv::Mat& thermalContourImage, cv::Mat depthImg,
		int mapType, bool preserveColors = false);

	void setImageWidth(int width);
	void setImageHeight(int height);

	bool getIsInitialized();

private:

	/* computeHomographyMapping handles the mapping of RGB <-> Thermal.
	It is called inside computeCorrespondingRgbPointFromThermal and computeCorrespondingThermalPointFromRgb */
	void computeHomographyMapping(std::vector<cv::Point2f>& vecUndistRgbCoord, std::vector<cv::Point2f>& vecUndistTCoord, std::vector<cv::Point2f> vecDCoord,
		std::vector<int> vecDepthInMm, std::vector<double>& minDist, std::vector<int> &bestHom, std::vector<std::vector<int> > &octantIndices,
		std::vector<std::vector<double> > &octantDistances, std::vector<cv::Point3f> &worldCoordPointvector);

	void computeHomographyMapping(std::vector<cv::Point2f>& cam1Points, std::vector<cv::Point2f>& cam2Points);


	/*	TrilinearHomographyInterpolator finds the nearest point for each quadrant in 3D space and calculates weights
	based on trilinear interpolation for the input 3D point. The function returns a list of weights of the points
	used for the interpolation */
	void trilinearInterpolator(cv::Point3f inputPoint, std::vector<cv::Point3f> &sourcePoints, std::vector<double> &precomputedDistance,
		std::vector<double> &weights, std::vector<int> &nearestSrcPointInd, std::vector<double> &nearestSrcPointDist);
	void bilinearInterpolator(cv::Point2f inputPoint, std::vector<cv::Point2f> &sourcePoints, std::vector<double> &precomputedDistance, std::vector<double> &weights,
		std::vector<int> &nearestSrcPointInd, std::vector<double> &nearestSrcPointDist);

	/* weightedHomographyMapper maps the undistPoint based by a weighted sum of the provided homographies weighted by homWeights */
	void weightedHomographyMapper(std::vector<cv::Point2f> undistPoint, std::vector<cv::Point2f> &estimatedPoint, std::vector<cv::Mat> &homographies,
		std::vector<double> &homWeights);


	void computeHomographies(std::vector<cv::Point2f> cam1Points, std::vector<cv::Point2f> cam2Points, int nbrHom, std::vector<cv::Mat>& homCam1Cam2,
		std::vector<cv::Mat>& homCam2Cam1, std::vector<cv::Point2f>& homCentersCam1, std::vector<cv::Point2f>& homCentersCam2);

	/* MyDistortPoints distort points, used if the source image is undistorted*/
	void MyDistortPoints(const std::vector<cv::Point2f> src, std::vector<cv::Point2f> & dst,
		const cv::Mat & cameraMatrix, const cv::Mat &distortionMatrix);

	float backProjectPoint(float point, float focalLength, float principalPoint, float zCoord);

	float forwardProjectPoint(float point, float focalLength, float principalPoint, float zCoord);

	// lookUpDepth gets the current depth of the point in the depth image
	float lookUpDepth(cv::Mat depthImg, cv::Point2f dCoord, bool SCALE_TO_THEORETICAL);

	// Helper functions for the registration of contours:
	void getRegisteredContours(std::vector<cv::Point> contour, std::vector<cv::Point> erodedContour, std::vector<cv::Point>& rgbContour,
		std::vector<cv::Point>& dContour, std::vector<cv::Point>& tContour, cv::Mat depthImg, int mapTyoe);

	void depthOutlierRemovalLookup(std::vector<cv::Point2f> vecDCoord, std::vector<int> &vecDepthInMm);

	// Concatenate a vector of vectors to a single vector
	void vecOfVecToCatVec(std::vector<std::vector<cv::Point2f> > vecOfVec, std::vector<cv::Point2f>& catVec);
	void vecOfVecToCatVec(std::vector<std::vector<double> > vecOfVec, std::vector<double>& catVec);
	void vecOfVecToCatVec(std::vector<std::vector<int> > vecOfVec, std::vector<int>& catVec);
	void vecOfVecToCatVec3D(std::vector<std::vector<cv::Point3f> > vecOfVec, std::vector<cv::Point3f>& catVec);


	struct calibrationParams {
		// Calibration parameters which are loaded from the calibVars.yml-file

		// Depth calibration parameters
		float depthCoeffA; // y = a*x+b
		float depthCoeffB;

		int WIDTH;
		int HEIGHT;

		int nbrHom;


		// Camera calibration parameters
		std::vector<int> activeImages;
		cv::Mat rgbCamMat, tCamMat, rgbDistCoeff, tDistCoeff;

		// Rectification matrices and mappings
		std::vector<cv::Mat> planarHom, planarHomInv, homCam1Cam2, homCam2Cam1;
		std::vector<cv::Point3f> homDepthCentersRgb, homDepthCentersT;
		std::vector<cv::Point2f> homCentersCam1, homCentersCam2;
		int defaultDepth;

		// Single, planar homographies simple lookup
		cv::Mat homRgbToT;
		cv::Mat homTToRgb;

		// Depth calibration parameters
		cv::Mat rgbToDCalX, rgbToDCalY, dToRgbCalX, dToRgbCalY;

	} stereoCalibParam;

	enum RegMethod { TrimodalMultipleHom, BimodalSingleHom, BimodalMultipleHom };

	struct registrationSettings {
		bool UNDISTORT_IMAGES;
		bool USE_PREV_DEPTH_POINT;

		int nbrClustersForDepthOutlierRemoval;
		int depthProximityThreshold;
		std::vector<int> discardedHomographies;
		int registrationMethod;

		bool isInitialized;
	} settings;

	cv::Mat rgbImg, tImg, dImg;
};

#endif // MULTIMODALREGISTRATOR_H
