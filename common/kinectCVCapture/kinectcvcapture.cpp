#include "kinectcvcapture.h"

using namespace cv;

KinectCVCapture::KinectCVCapture() : OpenCVCapture::OpenCVCapture()
{
}

void KinectCVCapture::startCapture() {
    if(captureSource == "kinect") {
        qDebug() << "Initializing Kinect.";

        //OpenCVFreenect& deviceID = freenect.createDevice(0); // Use for old libfreenect version.
        OpenCVFreenect& deviceID = freenect.createDevice<OpenCVFreenect>(0); // Use for new libfreenect version.

        device = &deviceID;

        depthMat = Mat(Size(640,480),CV_16UC1);
        depthf = Mat(Size(640,480),CV_8UC1);

        device->startVideo();
        device->startDepth();

        //Get an initial frame
        device->getVideo(image);
        device->getDepth(depthMat);
        depthMat.convertTo(depthf, CV_8UC1, 255.0/2048.0);

        //Connect the timer signal with the capture action
        connect(timer, SIGNAL(timeout()), this, SLOT(captureFrame()));

        // Start the timer scheduled for firing according to the frame rate
        // If the frame rate is 0, the timer will not be started.
        if(frameRate > 0) {
            timer->start(floor(1.0f/(float)frameRate*1000.0f));
        } else {
            frameRate = 0;
        }

        emit frameCaptured(image);
    } else {
        OpenCVCapture::startCapture();
    }
}

KinectCVCapture::~KinectCVCapture()
{
    device->stopVideo();
    device->stopDepth();
}

void KinectCVCapture::captureFrame()
{
    device->getVideo(image);
    device->getDepth(depthMat);
    depthMat.convertTo(depthf, CV_8UC1, 255.0/10000.0);

    if(image.empty()) {
        stopCapture();
        return;
    }

    emit frameCaptured(image);
}

Mat KinectCVCapture::getDepthImage()
{
    return depthf;
}

Mat KinectCVCapture::getRawDepth()
{
    return depthMat;
}

void KinectCVCapture::advanceCapture()
{
    if(frameRate == 0) {
        captureFrame();
    }
}
