#ifndef KINECTCVCAPTURE_H
#define KINECTCVCAPTURE_H

#include <libfreenect.hpp>
#include <QObject>
#include <QDebug>
#include "opencvcapture.h"
#include "opencvfreenect.h"


/**
 * Class for obtaining input from Kinect.
 *
 * This is built on the OpenCVCapture class and introduces
 * just one new function: getDepthImage(), which is used to
 * access the Kinect's 3D data. Other than that it works
 * exactly like OpenCVCapture, so see its documentation for
 * further information.
 */
class KinectCVCapture : public OpenCVCapture
{
    Q_OBJECT

public:
    using OpenCVCapture::startCapture;

    KinectCVCapture();
    ~KinectCVCapture();

    void startCapture();
    cv::Mat getDepthImage();
    cv::Mat getRawDepth();
    void advanceCapture();

    OpenCVFreenect* device;

private:
    //Freenect::Freenect<OpenCVFreenect> freenect;  // Use for old libfreenect version.
    Freenect::Freenect freenect; // Use for new libfreenect version.

    cv::Mat depthMat;
    cv::Mat depthf;

private slots:
    void captureFrame();
};

#endif // KINECTCVCAPTURE_H
