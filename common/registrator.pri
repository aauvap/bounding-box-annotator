HEADERS       *= registrator/registrator.h
SOURCES       *= registrator/registrator.cpp

!win32{
    LIBS *= -lopencv_core \
            -lopencv_highgui \
            -lopencv_calib3d
}

win32{
    INCLUDEPATH += "C:/OpenCV/release/install/include"
    LIBS += C:/OpenCV/release/bin/*.dll
}

INCLUDEPATH *= $${VPATH}registrator/
