#include "boxannotation.h"

using namespace cv;
using namespace std;

BoxAnnotation::BoxAnnotation()
{
	currentBox.aSet = currentBox.bSet = false;
	currentBox.objectID = -1;

	tracker = nullptr;
	activeStatus = BoxStatus::ACTIVE;

	initMetaData(QStringList());

	bonusInfo = "";
}

BoxAnnotation::BoxAnnotation(int id, QStringList metaDataNames)
{
	currentBox.aSet = currentBox.bSet = false;
    currentBox.objectID = id;
	
	tracker = nullptr;
	activeStatus = BoxStatus::ACTIVE;

	initMetaData(metaDataNames);
}

BoxAnnotation::BoxAnnotation(vector<cv::Point> corners, int id, QStringList metaDataNames, QString tag)
{
	assert(corners.size() == 2);
	activeStatus = BoxStatus::ACTIVE;

	initPoints(corners[0], corners[1]);
	initTag(tag);
	initMetaData(metaDataNames);
    currentBox.objectID = id;
	tracker = nullptr;
}

BoxAnnotation::BoxAnnotation(cv::Point a, cv::Point b, int id, QStringList metaDataNames, QString tag)
{
	initPoints(a, b);
	initTag(tag);
	initMetaData(metaDataNames);
    currentBox.objectID = id;

	activeStatus = BoxStatus::ACTIVE;
	
	tracker = nullptr;
}

void BoxAnnotation::initPoints(cv::Point a, cv::Point b)
{
	currentBox.PointA = a;
	currentBox.PointB = b;

	currentBox.aSet = currentBox.bSet = true;
	realign();
}

void BoxAnnotation::initMetaData(QStringList metaDataNames)
{
	for(int i = 0; i < (int)metaDataNames.size(); i++) {
		currentBox.metaDataValues.push_back(false);
	}
	this->currentBox.metaDataNames = metaDataNames;
}

void BoxAnnotation::initTag(QString tag)
{
	if(tag.length() > 0) {
		this->currentBox.theTag = tag;
	}
}

cv::Point BoxAnnotation::a() const
{
	return currentBox.PointA;
}

cv::Point BoxAnnotation::b() const
{
	return currentBox.PointB;
}

int BoxAnnotation::id() const
{
    return currentBox.objectID;
}

void BoxAnnotation::setId(int id)
{
    this->currentBox.objectID = id;
}


QString BoxAnnotation::aAsString() const
{
	return QString("%1,%2").arg(currentBox.PointA.x).arg(currentBox.PointA.y);
}

QString BoxAnnotation::bAsString() const
{
	return QString("%1,%2").arg(currentBox.PointB.x).arg(currentBox.PointB.y);
}

cv::Rect BoxAnnotation::box() const
{
	return currentBox.theBox;
}

void BoxAnnotation::setA(cv::Point position)
{
    addToHistory();

	currentBox.PointA = pointWithinImage(position);
	currentBox.aSet = true;
	realign();
}

void BoxAnnotation::setB(cv::Point position)
{
	addToHistory();

    currentBox.PointB = pointWithinImage(position);
	currentBox.bSet = true;
	realign();
}

bool BoxAnnotation::isFull()
{
	return (currentBox.aSet && currentBox.bSet);
}

void BoxAnnotation::realign()
{
	if(!isFull()) {
		return;
	}
	Point corner;
	corner.x = min(currentBox.PointA.x, currentBox.PointB.x);
	corner.y = min(currentBox.PointA.y, currentBox.PointB.y);
	currentBox.PointB.x = max(currentBox.PointA.x, currentBox.PointB.x);
	currentBox.PointB.y = max(currentBox.PointA.y, currentBox.PointB.y);
	currentBox.PointA = corner;

	currentBox.theBox.x = currentBox.PointA.x;
	currentBox.theBox.y = currentBox.PointA.y;
	currentBox.theBox.width = currentBox.PointB.x-currentBox.PointA.x;
	currentBox.theBox.height = currentBox.PointB.y-currentBox.PointA.y;
}

void BoxAnnotation::addToHistory()
{
    history.push_back(currentBox);
}

void BoxAnnotation::resetTracking()
{
	// Set up CamShift - but only if we have initialized the Bounding Box
	if (currentBox.aSet && currentBox.bSet && (tracker != nullptr))
	{
		tracker->resetTracking();
	}
}

void BoxAnnotation::setRectangle(cv::Rect rect)
{
	cv::Point a = cv::Point(rect.x, rect.y);
	cv::Point b = cv::Point(rect.x + rect.width, rect.y + rect.height);

	setA(a);
	setB(b);
}

cv::Point BoxAnnotation::pointWithinImage(cv::Point point)
{
	if (point.x < 0) {
		point.x = 0;
	}

	if (point.y < 0) {
		point.y = 0;
	}

	if (imageDims.x > 0) {
		if (point.x >= imageDims.x) {
			point.x = imageDims.x - 1;
		}

		if (point.y >= imageDims.y) {
			point.y = imageDims.y - 1;
		}
	}

	return point;
}

void BoxAnnotation::moveUp(int distance)
{
	if(!isFull()) {
		return;
	}
    
    addToHistory();
	currentBox.PointA.y -= distance;
	currentBox.PointB.y -= distance;
}

void BoxAnnotation::moveDown(int distance)
{
	if(!isFull()) {
		return;
	}

    addToHistory();
	currentBox.PointA.y += distance;
	currentBox.PointB.y += distance;
}

void BoxAnnotation::moveLeft(int distance)
{
	if(!isFull()) {
		return;
	}

    addToHistory();
	currentBox.PointA.x -= distance;
	currentBox.PointB.x -= distance;
}

void BoxAnnotation::moveRight(int distance)
{
	if(!isFull()) {
		return;
	}

    addToHistory();
	currentBox.PointA.x += distance;
	currentBox.PointB.x += distance;
}

void BoxAnnotation::shrinkVertical(int distance)
{
	if(!isFull()) {
		return;
	}
	if(currentBox.PointB.y-distance > currentBox.PointA.y) {
        addToHistory();
		currentBox.PointB.y -= distance;
	}
}

void BoxAnnotation::shrinkHorizontal(int distance)
{
	if(!isFull()) {
		return;
	}
	if(currentBox.PointB.x-distance > currentBox.PointA.x) {
        addToHistory();
		currentBox.PointB.x -= distance;
	}
}

void BoxAnnotation::growVertical(int distance)
{
	if(!isFull()) {
		return;
	}
    
    addToHistory();
	currentBox.PointB.y += distance;
}

void BoxAnnotation::growHorizontal(int distance)
{
	if(!isFull()) {
		return;
	}

    addToHistory();
	currentBox.PointB.x += distance;
}

QString BoxAnnotation::tag() const
{
	return currentBox.theTag;
}

void BoxAnnotation::setTag(QString tag)
{
    addToHistory();
	currentBox.theTag = tag;
}

bool BoxAnnotation::getMetaData(int metaDataId) const
{
	assert(metaDataId < (int)currentBox.metaDataValues.size());
	return currentBox.metaDataValues[metaDataId];
}

void BoxAnnotation::setMetaData(int metaDataId, bool value)
{
	assert(metaDataId < (int)currentBox.metaDataValues.size());

    addToHistory();
	currentBox.metaDataValues[metaDataId] = value;
}

QString BoxAnnotation::getMetaDataName(int metaDataId) const
{
	assert(metaDataId < (int)currentBox.metaDataValues.size());
	return currentBox.metaDataNames[metaDataId];
}

int BoxAnnotation::getMetaDataSize()
{
	return currentBox.metaDataValues.size();
}

void BoxAnnotation::trackObject(cv::Mat previousImage, cv::Mat currentImage)
{
	if (tracker == nullptr)
	{
		tracker = std::make_shared<BoxTracker>(box(), previousImage);
	}

	imageDims.x = currentImage.cols;
	imageDims.y = currentImage.rows;

	if (currentBox.aSet && currentBox.bSet)
	{
		addToHistory();

		cv::Rect trackedBox = tracker->trackObject(currentImage);

		if (trackedBox.area() > 1)
		{
			setRectangle(trackedBox);
		}
	}

}

void BoxAnnotation::undo()
{
    if (!history.empty())
    {
        // Restore previous box
        currentBox = history.back();

        // Delete what was the previous box from the history
        history.pop_back();
    }
}

BoxTracker::BoxTracker(cv::Rect currentBox, cv::Mat currentImage)
{
	this->currentBox = currentBox;
	checkBox(currentImage.size());

	cvtColor(currentImage, hsvImage, COLOR_BGR2HSV);

	inRange(hsvImage, Scalar(0, smin, vmin),
		Scalar(180, 256, vmax), mask);
	int ch[] = { 0, 0 };
	hueImage.create(hsvImage.size(), hsvImage.depth());
	cv::mixChannels(&hsvImage, 1, &hueImage, 1, ch, 1);

	resetTracking();

	camShift();
}

void BoxTracker::checkBox(cv::Size imageSize)
{
	if (currentBox.x < 0)
	{
		currentBox.x = 0;
	}

	if (currentBox.y < 0)
	{
		currentBox.y = 0;
	}

	if ((currentBox.x + currentBox.width) > imageSize.width)
	{
		currentBox.width = imageSize.width - currentBox.x;
	}

	if ((currentBox.y + currentBox.height) > imageSize.height)
	{
		currentBox.height = imageSize.height - currentBox.y;
	}
}

void BoxTracker::camShift()
{
	float hranges[] = { 0,180 };
	const float* phranges = hranges;

	calcBackProject(&hueImage, 1, 0, roiHist, backProjectedImage, &phranges);
	backProjectedImage &= mask;

	/*RotatedRect trackBox = CamShift(backProjectedImage, object,
	TermCriteria(TermCriteria::EPS | TermCriteria::COUNT, 10, 1));*/
	CamShift(backProjectedImage, currentBox,
		TermCriteria(TermCriteria::EPS | TermCriteria::COUNT, 10, 1));
}

cv::Rect BoxTracker::trackObject(cv::Mat newImage)
{
	checkBox(newImage.size());

	cvtColor(newImage, hsvImage, COLOR_BGR2HSV);

	inRange(hsvImage, Scalar(0, smin, vmin),
		Scalar(180, 256, vmax), mask);
	vector<int> ch = { 0, 0 };
	hueImage.create(hsvImage.size(), hsvImage.depth());
	cv::mixChannels(hsvImage, hueImage, ch);

	camShift();

	//// Update the current box and thus convert the RotatedRect to an ordinary Rect
	//if (trackBox.boundingRect().area() > 1)
	//{
	//	setRectangle(trackBox.boundingRect());
	//}

	return currentBox;
}

void BoxTracker::resetTracking()
{
	// Set up CamShift - but only if we have initialized the Bounding Box
	if (currentBox.area() > 0)
	{
		Mat roi(hueImage, currentBox), maskRoi(mask, currentBox);

		int nimages = 1;
		int channels[] = { 0 };
		int dims = 1;
		int histSize[] = { 16 };
		float hranges[] = { 0, 180 };
		const float *ranges[] = { hranges };

		calcHist(&roi, nimages, channels, maskRoi, roiHist, dims, histSize, ranges);
		normalize(roiHist, roiHist, 0, 255, NORM_MINMAX);


		histImg = Scalar::all(0);
		int binW = histImg.cols / histSize[0];
		Mat buf(1, histSize[0], CV_8UC3);
		for (int i = 0; i < histSize[0]; i++)
			buf.at<Vec3b>(i) = Vec3b(saturate_cast<uchar>(i*180. / histSize[0]), 255, 255);
		cvtColor(buf, buf, COLOR_HSV2BGR);

		for (int i = 0; i < histSize[0]; i++)
		{
			int val = saturate_cast<int>(roiHist.at<float>(i)*histImg.rows / 255);
			rectangle(histImg, Point(i*binW, histImg.rows),
				Point((i + 1)*binW, histImg.rows - val),
				Scalar(buf.at<Vec3b>(i)), -1, 8);
		}
	}

}
