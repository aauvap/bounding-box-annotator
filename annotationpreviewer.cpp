#include "annotationpreviewer.h"

#include <QPainter>

VerticalLabel::VerticalLabel(QWidget *parent)
	: QLabel(parent)
{

}

VerticalLabel::VerticalLabel(const QString &text, QWidget *parent)
	: QLabel(text, parent)
{
}

void VerticalLabel::paintEvent(QPaintEvent*)
{
	QPainter painter(this);
	painter.setPen(Qt::black);
	painter.setBrush(Qt::Dense1Pattern);

	painter.translate(sizeHint().width(), sizeHint().height());
	painter.rotate(270);

	painter.drawText(0, 0, text());
}

QSize VerticalLabel::minimumSizeHint() const
{
	QSize s = QLabel::minimumSizeHint();
	return QSize(s.height(), s.width());
}

QSize VerticalLabel::sizeHint() const
{
	QSize s = QLabel::sizeHint();
	return QSize(s.height(), s.width());
}

AnnotationPreviewer::AnnotationPreviewer()
{
	currentOrientation = Qt::Horizontal;
}

void AnnotationPreviewer::setAnnotations(const std::map<int, AnnotatedImage> &annotations)
{
	this->annotations = annotations;
}

void AnnotationPreviewer::setAnnotation(const int frame, const AnnotatedImage & annotation)
{
	this->annotations[frame] = annotation;
}

QWidget * AnnotationPreviewer::getPreview(int annotationId, int currentFrameNumber, 
	Qt::Orientation layoutOrientation, QWidget* &centralWidget)
{
	// Construct the preview widget
	// Next, construct the scrollable area
	QWidget* previewWidget = new QWidget();
	QLayout* previewLayout;
	this->currentOrientation = layoutOrientation;
	
	if (layoutOrientation == Qt::Horizontal) {
		previewLayout = new QHBoxLayout();
	}
	else {
		previewLayout = new QVBoxLayout();
	}
	

	//functionActions[Function::zoomIn] = this->ui->utilityToolBar->addAction(QIcon(":/icons/magnifier-zoom-in.png"), QString("Zoom in (%1)").arg(functionShortcuts[Function::zoomIn].toString()), this, SLOT(zoomIn()));
	//QList<QKeySequence> shortcuts;
	//shortcuts.append(functionShortcuts[Function::zoomIn]);
	//shortcuts.append(functionShortcuts[Function::zoomInExtra]);
	//functionActions[Function::zoomIn]->setShortcuts(shortcuts);
	//functionActions[Function::zoomOut] = this->ui->utilityToolBar->addAction(QIcon(":/icons/magnifier-zoom-out.png"), QString("Zoom out (%1)").arg(functionShortcuts[Function::zoomOut].toString()), this, SLOT(zoomOut()));

	cv::Rect prevRoi;

	bool lineInsertedBefore = false;
	bool lineInsertedAfter = false;

	for (auto annotatedFrame : annotations)
	{
		// Check if there exist an annotation 
		cv::Point a, b;
		cv::Mat currentImage = annotatedFrame.second.image;
		cv::Rect roi;
		cv::Mat roiImage;

		for (auto singleAnnotation : annotatedFrame.second.annotations)
		{
			if (singleAnnotation.id() == annotationId)
			{
				a = singleAnnotation.a();
				b = singleAnnotation.b();

				// Clone the underlying image - we have something to draw on top of it
				currentImage = annotatedFrame.second.image.clone();

				if (singleAnnotation.isFinished()) {
					rectangle(currentImage, a, b, cv::Scalar(0, 0, 255));
				}
				else {
					rectangle(currentImage, a, b, cv::Scalar(0, 255, 255));
				}
				

				// Add markers for true meta-data:
				for (int k = 0; k < singleAnnotation.getMetaDataSize(); k++) {
					if (singleAnnotation.getMetaData(k)) {
						rectangle(currentImage, cv::Point(a.x + 1 + 5 * k, a.y - 1), 
							cv::Point(a.x + 1 + 5 * k + 2, a.y - 3), cv::Scalar(0, 0, 255), -1);
					}
				}

				// Set the view rectangle
				roi.x = a.x - 10 >= 0 ? a.x - 10 : 0;
				roi.y = a.y - 10 >= 0 ? a.y - 10 : 0;

				roi.width = singleAnnotation.box().width + 20;
				roi.height = singleAnnotation.box().height + 20;

				if (roi.x + roi.width > currentImage.cols) 
				{
					roi.width = currentImage.cols - roi.x - 1;
				}

				if (roi.y + roi.height > currentImage.rows)
				{
					roi.height = currentImage.rows - roi.y - 1;
				}

				break;
			}
		}

		if (roi.area() > 0)
		{
			// ROI is set - we have found an annotation with the right id
			roiImage = currentImage(roi);
			prevRoi = roi;
		}
		else if (prevRoi.area() > 0)
		{
			roiImage = currentImage(prevRoi);
		}

		OpenCVViewer* imageViewer = new OpenCVViewer();
		imageViewer->setImage(roiImage);

		if (!lineInsertedBefore && annotatedFrame.first == currentFrameNumber)
		{
			// Insert a vertical line to indicate that we are at the current frame number
			QFrame *verticalLine = new QFrame();
			verticalLine->setFrameShape(layoutOrientation == Qt::Horizontal ? QFrame::VLine : QFrame::HLine);
			verticalLine->setFrameShadow(QFrame::Sunken);
			previewLayout->addWidget(verticalLine);
			centralWidget = imageViewer;

			lineInsertedBefore = true;
		}

		QGroupBox* frameBox;
		if (layoutOrientation == Qt::Horizontal)
		{
			frameBox = new QGroupBox(QObject::tr("Image %1").arg(annotatedFrame.first + 1));
		}
		else {
			frameBox = new QGroupBox();
		}

		QHBoxLayout* frameLayout = new QHBoxLayout();

		if (layoutOrientation == Qt::Vertical)
		{
			VerticalLabel* label = new VerticalLabel(QObject::tr("Image %1").arg(annotatedFrame.first + 1));
			frameLayout->addWidget(label);
		}

		frameLayout->addWidget(imageViewer);
		frameBox->setLayout(frameLayout);



		if (!lineInsertedAfter && annotatedFrame.first > currentFrameNumber)
		{
			// Insert a vertical line to indicate that we have surpassed the current frame number
			QFrame *verticalLine = new QFrame();
			verticalLine->setFrameShape(layoutOrientation == Qt::Horizontal ? QFrame::VLine : QFrame::HLine);
			verticalLine->setFrameShadow(QFrame::Sunken);
			previewLayout->addWidget(verticalLine);

			lineInsertedAfter = true;
		}

		previewLayout->addWidget(frameBox);
	}
	previewWidget->setLayout(previewLayout);

	return previewWidget;
}
