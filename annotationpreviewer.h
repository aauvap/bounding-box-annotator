#ifndef ANNOTATIONPREVIEWER
#define ANNOTATIONPREVIEWER

#include <vector>
#include <map>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include <QGroupBox>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QString>
#include <QStringList>
#include <QScrollArea>
#include <QWidget>

#include "boxannotation.h"
#include "common/openCVViewer/opencvviewer.h"

// https://stackoverflow.com/questions/9183050/vertical-qlabel-or-the-equivalent
class VerticalLabel : public QLabel
{
	Q_OBJECT

public:
	explicit VerticalLabel(QWidget *parent = 0);
	explicit VerticalLabel(const QString &text, QWidget *parent = 0);

protected:
	void paintEvent(QPaintEvent*);
	QSize sizeHint() const;
	QSize minimumSizeHint() const;
};


struct AnnotatedImage{
	cv::Mat image;
	QList<BoxAnnotation> annotations;
	int frameNumber;
};

class AnnotationPreviewer : QWidget
{
	Q_OBJECT;

public:
	AnnotationPreviewer();

	void setAnnotations(const std::map<int, AnnotatedImage> &annotations);
	void setAnnotation(const int frame, const AnnotatedImage &annotation);

	QWidget* getPreview(int annotationId, int currentFrameNumber,
		Qt::Orientation layoutOrientation, QWidget* &centralWidget);
	Qt::Orientation getCurrentOrientation() { return currentOrientation; }

private:
	std::map<int, AnnotatedImage> annotations;
	Qt::Orientation currentOrientation;

	QPushButton *zoomInButton;
	QPushButton *zoomOutButton;
};



#endif // !ANNOTATIONPREVIWER

