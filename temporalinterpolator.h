#ifndef TEMPORALINTERPOLATOR_H
#define TEMPORALINTERPOLATOR_H

#include <vector>
#include <map>
#include <opencv2/core/core.hpp>
#include <QString>
#include <QStringList>

#include "boxannotation.h"


class TemporalInterpolator
{
public:
	TemporalInterpolator(int currentFrameNumber, const QList<BoxAnnotation> &currentAnnotations);

	int interpolate(int currentFrameNumber, const QList<BoxAnnotation> &newAnnotations,
		const std::map<int, QList<BoxAnnotation> > &existingAnnotations);
	std::map<int, QList<BoxAnnotation> > getInterpolatedAnnotations();
	
	const int getStartFrameNumber(){ return startFrameNumber; }

private:
	int linearInterpolation(double x, double x1, double y1, double x2, double y2);
	cv::Point interpolatePoint(double currentTime, double startTime, cv::Point startPoint, double endTime, cv::Point endPoint);

	QList<BoxAnnotation> startAnnotations;
	int startFrameNumber;

	QList<BoxAnnotation> endAnnotations;
	int endFrameNumber;

	std::map<int, QList<BoxAnnotation> > interpolatedAnnotations;
};

#endif // TEMPORALINTERPOLATOR_H
