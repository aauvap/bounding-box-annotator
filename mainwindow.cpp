#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace cv;
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	setWindowTitle("AAU VAP Bounding Box Annotator");

	fileLoaded = false;
	shiftKeyPressed = false;

    // Settings
    QCoreApplication::setOrganizationName("AAU");
    QCoreApplication::setOrganizationDomain("vap.aau.dk");
    QCoreApplication::setApplicationName("Frame Annotator");
    QSettings settings;

    settings.setValue("rgbPattern", filePatterns[Image::RGB] = settings.value("rgbPattern", "SyncRGB/*.jpg").toString());
    settings.setValue("thermalPattern", filePatterns[Image::Thermal] = settings.value("thermalPattern", "SyncT/*.jpg").toString());
    settings.setValue("csvPath", csvPath = settings.value("csvPath", "annotations.csv").toString());

    settings.setValue("overlayColor", overlayColor = settings.value("overlayColor", QColor(0, 0, 255)).value<QColor>());
    settings.setValue("overlayOpacity", overlayOpacity = settings.value("overlayOpacity", 0.3).toFloat());

    settings.setValue("enableRgb", enabledModalities[Image::RGB] = settings.value("enableRgb", true).toBool());
    settings.setValue("enableThermal", enabledModalities[Image::Thermal] = settings.value("enableThermal", true).toBool());
    
    // Insert indicators to the status bar
    fileNumber = new QLabel();
    fileNumber->setText("Image: 0/0");
    ui->statusBar->addWidget(fileNumber);

	drawingInitiated = false;
	allowAnnotations = false;
	currentBox = -1;
	currentImage = 0;
	frameDistance = 1;
	numImages = 0;
	boxAdjustAmount = 1;

    initModalityWindows();

	QFile file("metaDataList.txt");
	if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		while (!file.atEnd()) {
			QString line = QString(file.readLine()).trimmed();
			if(line.length() == 0) {
				continue;
			}
			metaDataNames << line;
		}
		file.close();
	}

	QFile tagFile("suggestedTags.txt");
	if (tagFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
		while (!tagFile.atEnd()) {
			QString line = QString(tagFile.readLine()).trimmed();

			if (line.length() == 0) {
				continue;
			}

			suggestedTags << line;
		}
		tagFile.close();
	}

	ui->annotationProperties->setRowCount(3+metaDataNames.size());
	ui->annotationProperties->setColumnWidth(0, 127);
	ui->annotationProperties->setColumnWidth(1, 127);

	// Define shortcuts
	QHash<int, QKeySequence> functionShortcuts;
	functionShortcuts[Function::save] = QKeySequence::Save;
	functionShortcuts[Function::undo] = QKeySequence::Undo;
	functionShortcuts[Function::deleteAnnotations] = QKeySequence::Delete;
	functionShortcuts[Function::deleteAllFutureAnnotations] = QKeySequence("Shift+Del");
	//functionShortcuts[Function::deleteAnnotationsWithinRange] = QKeySequence("Alt+Del");
	functionShortcuts[Function::mergeAnnotations] = QKeySequence("Shift+Ins");
	functionShortcuts[Function::zoomIn] = QKeySequence::ZoomIn;
	functionShortcuts[Function::zoomInExtra] = QKeySequence(Qt::Key_Plus);
	functionShortcuts[Function::zoomOut] = QKeySequence::ZoomOut;
	functionShortcuts[Function::zoomOutExtra] = QKeySequence(Qt::Key_Minus);
	functionShortcuts[Function::zoomFit] = QKeySequence("Ctrl+9");
	functionShortcuts[Function::zoomActual] = QKeySequence("Ctrl+0");
	functionShortcuts[Function::jumpToImage] = QKeySequence("Ctrl+j");
	functionShortcuts[Function::retainPreviousAnnotation] = QKeySequence(Qt::Key_F3);
	functionShortcuts[Function::retainNextAnnotation] = QKeySequence(Qt::Key_F4);
	functionShortcuts[Function::displayTag] = QKeySequence(Qt::Key_F1);
	functionShortcuts[Function::interpolateBetweenAnnotations] = QKeySequence("Ctrl+i");
	functionShortcuts[Function::playPausePlayback] = QKeySequence(Qt::Key_F5);

	QHash<int, QList<QKeySequence>> multipleFunctionShortcuts;
	multipleFunctionShortcuts[Function::moveBBDown] = QList<QKeySequence>({ QKeySequence("S"),  QKeySequence("Shift+S") });
	multipleFunctionShortcuts[Function::moveBBUp] = QList<QKeySequence>({ QKeySequence("W"),  QKeySequence("Shift+W") });
	multipleFunctionShortcuts[Function::moveBBLeft] = QList<QKeySequence>({ QKeySequence("A"),  QKeySequence("Shift+A") });
	multipleFunctionShortcuts[Function::moveBBRight] = QList<QKeySequence>({ QKeySequence("D"),  QKeySequence("Shift+D") });
	multipleFunctionShortcuts[Function::expandBBVertical] = QList<QKeySequence>({ QKeySequence("K"),  QKeySequence("Shift+K") });
	multipleFunctionShortcuts[Function::shrinkBBVertical] = QList<QKeySequence>({ QKeySequence("I"),  QKeySequence("Shift+I") });
	multipleFunctionShortcuts[Function::expandBBHorizontal] = QList<QKeySequence>({ QKeySequence("L"),  QKeySequence("Shift+L") });
	multipleFunctionShortcuts[Function::shrinkBBHorizontal] = QList<QKeySequence>({ QKeySequence("J"),  QKeySequence("Shift+J") });
	multipleFunctionShortcuts[Function::previousImage] = QList<QKeySequence>({ QKeySequence(Qt::Key_Left), QKeySequence("U"), QKeySequence(Qt::ShiftModifier + Qt::Key_U), QKeySequence(Qt::ShiftModifier + Qt::Key_Left)});
	multipleFunctionShortcuts[Function::nextImage] = QList<QKeySequence>({ QKeySequence(Qt::Key_Right), QKeySequence("O"),QKeySequence(Qt::ShiftModifier + Qt::Key_O), QKeySequence(Qt::ShiftModifier + Qt::Key_Right) });

	imageSelectorSpinBox = new QSpinBox(this);
	imageSelectorSpinBox->setMinimum(1);
	imageSelectorSpinBox->setMaximum(1);
	imageSelectorSpinBox->setSingleStep(1);
	imageSelectorSpinBox->setValue(0);
	
	// Build toolbar
	functionActions[Function::save] = this->ui->mainToolBar->addAction(QIcon(":/icons/disk-black.png"), QString("Save (%1)").arg(functionShortcuts[Function::save].toString()), this, SLOT(saveAnnotations()));
	functionActions[Function::save]->setShortcut(functionShortcuts[Function::save]);
	functionActions[Function::save]->setEnabled(false);
	functionActions[Function::undo] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow-curve-180-left.png"), QString("Undo (%1)").arg(functionShortcuts[Function::undo].toString()), this, SLOT(undo()));
	functionActions[Function::undo]->setShortcut(functionShortcuts[Function::undo]);
	functionActions[Function::undo]->setEnabled(false);
	functionActions[Function::deleteAnnotations] = this->ui->mainToolBar->addAction(QIcon(":/icons/cross-script.png"), QString("Delete selected annotation (%1)").arg(functionShortcuts[Function::deleteAnnotations].toString()), this, SLOT(deleteCurrentAnnotation()));
	functionActions[Function::deleteAnnotations]->setShortcut(functionShortcuts[Function::deleteAnnotations]);
	functionActions[Function::deleteAnnotations]->setEnabled(false);
	functionActions[Function::deleteAllFutureAnnotations] = this->ui->mainToolBar->addAction(QIcon(":/icons/fire-big.png"), QString("Delete selected annotation in current and future frames (%1)").arg(functionShortcuts[Function::deleteAllFutureAnnotations].toString()), this, SLOT(deleteAllFutureAnnotations()));
	functionActions[Function::deleteAllFutureAnnotations]->setShortcut(functionShortcuts[Function::deleteAllFutureAnnotations]);
	functionActions[Function::deleteAllFutureAnnotations]->setEnabled(false);
	//functionActions[Function::deleteAnnotationsWithinRange] = this->ui->mainToolBar->addAction(QIcon(":/icons/fire-pencil.png"), QString("Delete selected annotation within a user-specified range of frames (%1)").arg(functionShortcuts[Function::deleteAnnotationsWithinRange].toString()), this, SLOT(deleteAnnotationsWithinRange()));
	//functionActions[Function::deleteAnnotationsWithinRange]->setShortcut(functionShortcuts[Function::deleteAnnotationsWithinRange]);
	//functionActions[Function::deleteAnnotationsWithinRange]->setEnabled(false);
	functionActions[Function::mergeAnnotations] = this->ui->mainToolBar->addAction(QIcon(":/icons/sql-join-outer.png"), QString("Merge selected annotation and another annotation in current and future frames (%1)").arg(functionShortcuts[Function::mergeAnnotations].toString()), this, SLOT(mergeAnnotations()));
	functionActions[Function::mergeAnnotations]->setShortcut(functionShortcuts[Function::mergeAnnotations]);
	functionActions[Function::mergeAnnotations]->setEnabled(false);
	this->ui->mainToolBar->addSeparator();
	functionActions[Function::moveBBUp] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow-090.png"), QString("Move bounding box up (%1)").arg(multipleFunctionShortcuts[Function::moveBBUp].first().toString()), this, SLOT(moveBBUp()));
	functionActions[Function::moveBBUp]->setShortcuts(multipleFunctionShortcuts[Function::moveBBUp]);
	functionActions[Function::moveBBUp]->setEnabled(false);
	functionActions[Function::moveBBDown] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow-270.png"), QString("Move bounding box down (%1)").arg(multipleFunctionShortcuts[Function::moveBBDown].first().toString()), this, SLOT(moveBBDown()));
	functionActions[Function::moveBBDown]->setShortcuts(multipleFunctionShortcuts[Function::moveBBDown]);
	functionActions[Function::moveBBDown]->setEnabled(false);
	functionActions[Function::moveBBLeft] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow-180.png"), QString("Move bounding box left (%1)").arg(multipleFunctionShortcuts[Function::moveBBLeft].first().toString()), this, SLOT(moveBBLeft()));
	functionActions[Function::moveBBLeft]->setShortcuts(multipleFunctionShortcuts[Function::moveBBLeft]);
	functionActions[Function::moveBBLeft]->setEnabled(false);
	functionActions[Function::moveBBRight] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow.png"), QString("Move bounding box right (%1)").arg(multipleFunctionShortcuts[Function::moveBBRight].first().toString()), this, SLOT(moveBBRight()));
	functionActions[Function::moveBBRight]->setShortcuts(multipleFunctionShortcuts[Function::moveBBRight]);
	functionActions[Function::moveBBRight]->setEnabled(false);

	functionActions[Function::expandBBHorizontal] = this->ui->mainToolBar->addAction(QIcon(":/icons/fill-medium.png"), QString("Expand bounding box horizontal (%1)").arg(multipleFunctionShortcuts[Function::expandBBHorizontal].first().toString()), this, SLOT(expandBBHorizontal()));
	functionActions[Function::expandBBHorizontal]->setShortcuts(multipleFunctionShortcuts[Function::expandBBHorizontal]);
	functionActions[Function::expandBBHorizontal]->setEnabled(false);
	functionActions[Function::shrinkBBHorizontal] = this->ui->mainToolBar->addAction(QIcon(":/icons/fill-medium-180.png"), QString("Shrink bounding box horizontal (%1)").arg(multipleFunctionShortcuts[Function::shrinkBBHorizontal].first().toString()), this, SLOT(shrinkBBHorizontal()));
	functionActions[Function::shrinkBBHorizontal]->setShortcuts(multipleFunctionShortcuts[Function::shrinkBBHorizontal]);
	functionActions[Function::shrinkBBHorizontal]->setEnabled(false);
	functionActions[Function::shrinkBBVertical] = this->ui->mainToolBar->addAction(QIcon(":/icons/fill-medium-090.png"), QString("Shrink bounding box vertical (%1)").arg(multipleFunctionShortcuts[Function::shrinkBBVertical].first().toString()), this, SLOT(shrinkBBVertical()));
	functionActions[Function::shrinkBBVertical]->setShortcuts(multipleFunctionShortcuts[Function::shrinkBBVertical]);
	functionActions[Function::shrinkBBVertical]->setEnabled(false);
	functionActions[Function::expandBBVertical] = this->ui->mainToolBar->addAction(QIcon(":/icons/fill-medium-270.png"), QString("Expand bounding box vertical (%1)").arg(multipleFunctionShortcuts[Function::expandBBVertical].first().toString()), this, SLOT(expandBBVertical()));
	functionActions[Function::expandBBVertical]->setShortcuts(multipleFunctionShortcuts[Function::expandBBVertical]);
	functionActions[Function::expandBBVertical]->setEnabled(false);
	this->ui->mainToolBar->addSeparator();


	functionActions[Function::retainPreviousAnnotation] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow-return-270-left.png"), QObject::tr("Retain annotation boxes when loading previous (%1)").arg(functionShortcuts[Function::retainPreviousAnnotation].toString()), this, SLOT(on_retainBoxes_clicked()));
	functionActions[Function::retainPreviousAnnotation]->setCheckable(true);
	functionActions[Function::retainPreviousAnnotation]->setShortcut(functionShortcuts[Function::retainPreviousAnnotation]);
	functionActions[Function::retainNextAnnotation] = this->ui->mainToolBar->addAction(QIcon(":/icons/arrow-return-270.png"), QObject::tr("Retain annotation boxes when loading next (%1)").arg(functionShortcuts[Function::retainNextAnnotation].toString()), this, SLOT(on_retainBoxes_clicked()));
	functionActions[Function::retainNextAnnotation]->setCheckable(true);
	functionActions[Function::retainNextAnnotation]->setShortcut(functionShortcuts[Function::retainNextAnnotation]);
	functionActions[Function::interpolateBetweenAnnotations] = this->ui->mainToolBar->addAction(QIcon(":/icons/layer-shape-line.png"), QObject::tr("Interpolate between annotations when stepping >1 frames (%1)").arg(functionShortcuts[Function::interpolateBetweenAnnotations].toString()));
	functionActions[Function::interpolateBetweenAnnotations]->setCheckable(true);
	functionActions[Function::interpolateBetweenAnnotations]->setShortcut(functionShortcuts[Function::interpolateBetweenAnnotations]);

	interpolationStepSizeSpinBox = new QSpinBox(this);
	interpolationStepSizeSpinBox->setMinimum(2);
	interpolationStepSizeSpinBox->setMaximum(99);
	interpolationStepSizeSpinBox->setValue(5); // Default value
	interpolationStepSizeSpinBox->setToolTip(QObject::tr("Set interpolation step size (Shift + number)"));
	
	this->ui->mainToolBar->addWidget(new QLabel("  Step size: "));
	this->ui->mainToolBar->addWidget(interpolationStepSizeSpinBox);
	
	//	this->ui->mainToolBar->addSeparator();
	//	functionActions[Function::trackObject] = this->ui->mainToolBar->addAction(QIcon(":/icons/binocular.png"), QObject::tr("Track object"), this, SLOT(on_resetTrackingButton_clicked()));
	//	functionActions[Function::trackObject]->setCheckable(true);
	//	functionActions[Function::resetTracking] = this->ui->mainToolBar->addAction(QIcon(":/icons/binocular--exclamation.png"), QObject::tr("Reset tracking"));
	//functionActions[Function::resetTracking]->setCheckable(true);

functionActions[Function::zoomIn] = this->ui->utilityToolBar->addAction(QIcon(":/icons/magnifier-zoom-in.png"), QString("Zoom in (%1)").arg(functionShortcuts[Function::zoomIn].toString()), this, SLOT(zoomIn()));
QList<QKeySequence> shortcuts;
shortcuts.append(functionShortcuts[Function::zoomIn]);
shortcuts.append(functionShortcuts[Function::zoomInExtra]);
functionActions[Function::zoomIn]->setShortcuts(shortcuts);
functionActions[Function::zoomOut] = this->ui->utilityToolBar->addAction(QIcon(":/icons/magnifier-zoom-out.png"), QString("Zoom out (%1)").arg(functionShortcuts[Function::zoomOut].toString()), this, SLOT(zoomOut()));
shortcuts.clear();
shortcuts.append(functionShortcuts[Function::zoomOut]);
shortcuts.append(functionShortcuts[Function::zoomOutExtra]);
functionActions[Function::zoomOut]->setShortcuts(shortcuts);
functionActions[Function::zoomFit] = this->ui->utilityToolBar->addAction(QIcon(":/icons/magnifier-zoom-fit.png"), QString("Zoom fit (%1)").arg(functionShortcuts[Function::zoomFit].toString()), this, SLOT(zoomFit()));
functionActions[Function::zoomFit]->setShortcut(functionShortcuts[Function::zoomFit]);
functionActions[Function::zoomActual] = this->ui->utilityToolBar->addAction(QIcon(":/icons/magnifier-zoom-actual.png"), QString("Zoom 1:1 (%1)").arg(functionShortcuts[Function::zoomActual].toString()), this, SLOT(zoomActual()));
functionActions[Function::zoomActual]->setShortcut(functionShortcuts[Function::zoomActual]);
this->ui->utilityToolBar->addSeparator();
functionActions[Function::previousImage] = this->ui->utilityToolBar->addAction(QIcon(":/icons/navigation-180.png"), QString("Previous image (%1)").arg(multipleFunctionShortcuts[Function::previousImage].first().toString()), this, SLOT(previousImage()));
functionActions[Function::previousImage]->setShortcuts(multipleFunctionShortcuts[Function::previousImage]);
functionActions[Function::nextImage] = this->ui->utilityToolBar->addAction(QIcon(":/icons/navigation.png"), QString("Next image (%1)").arg(multipleFunctionShortcuts[Function::nextImage].first().toString()), this, SLOT(nextImage()));
functionActions[Function::nextImage]->setShortcuts(multipleFunctionShortcuts[Function::nextImage]);
this->ui->utilityToolBar->addWidget(new QLabel("  Jump to image: ", this));
this->ui->utilityToolBar->addWidget(imageSelectorSpinBox);
functionActions[Function::jumpToImage] = this->ui->utilityToolBar->addAction(QIcon(":/icons/control-skip.png"), QString("Jump to image (%1)").arg(functionShortcuts[Function::jumpToImage].toString()), this, SLOT(jumpToImage()));
functionActions[Function::jumpToImage]->setShortcut(functionShortcuts[Function::jumpToImage]);
functionActions[Function::playPausePlayback] = this->ui->utilityToolBar->addAction(QIcon(":/icons/control.png"), QString("Start/pause playback (%1)").arg(functionShortcuts[Function::playPausePlayback].toString()), this, SLOT(playPausePlayback()));
functionActions[Function::playPausePlayback]->setShortcut(functionShortcuts[Function::playPausePlayback]);

this->ui->utilityToolBar->addSeparator();
functionActions[Function::showDontCareMask] = this->ui->utilityToolBar->addAction(QIcon(":/icons/layer-shade.png"), QObject::tr("Show don't care mask"), this, SLOT(showDontCareMask()));
functionActions[Function::showDontCareMask]->setCheckable(true);
functionActions[Function::displayTag] = this->ui->utilityToolBar->addAction(QIcon(":/icons/tag.png"), QObject::tr("Display tag (%1)").arg(functionShortcuts[Function::displayTag].toString()), this, SLOT(showDontCareMask()));
functionActions[Function::displayTag]->setCheckable(true);
functionActions[Function::displayTag]->setChecked(true);
functionActions[Function::displayTag]->setShortcut(functionShortcuts[Function::displayTag]);

if (!settings.value("useMask").toBool()) {
	functionActions[Function::showDontCareMask]->setEnabled(false);
}
else {
	functionActions[Function::showDontCareMask]->setChecked(true);
}

ui->utilityToolBar->setEnabled(false);

ui->annotationsPreviewWidget->installEventFilter(this);
ui->currentAnnotationsDock->installEventFilter(this);
ui->annotationPropertiesDock->installEventFilter(this);

connect(imageSelectorSpinBox, SIGNAL(editingFinished()), this, SLOT(jumpToImage()));

connect(ui->action_Save_and_continue, SIGNAL(triggered()), this, SLOT(on_saveBtn_clicked()));
connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));
connect(ui->annotationPropertiesDock, SIGNAL(visibilityChanged(bool)), ui->actionView_properties, SLOT(setChecked(bool)));
connect(ui->actionView_properties, SIGNAL(toggled(bool)), ui->annotationPropertiesDock, SLOT(setShown(bool)));

updatePropertiesTable();

settings.endGroup();

// Save default window settings
writeSettings("default");

// Read window settings
readSettings("");

if (settings.value("useDarkMode").toBool())
{
	ui->actionUse_Dark_Mode->setChecked(true);
	on_actionUse_Dark_Mode_triggered();
}

}

MainWindow::~MainWindow()
{
	outFile.close();

	// Remember the window settings
	writeSettings("");

	delete ui;
}

void MainWindow::on_actionReset_layout_triggered()
{
	readSettings("default");
}

bool MainWindow::eventFilter(QObject * obj, QEvent * event)
{
	if (event->type() == QEvent::Resize && obj == ui->annotationsPreviewWidget) {
		QResizeEvent *resizeEvent = static_cast<QResizeEvent*>(event);
		Qt::Orientation oldOrientation =
			resizeEvent->oldSize().height() < resizeEvent->oldSize().width() ?
			Qt::Horizontal : Qt::Vertical;
		Qt::Orientation newOrientation = resizeEvent->size().height() < resizeEvent->size().width() ?
			Qt::Horizontal : Qt::Vertical;

		if (oldOrientation != newOrientation) {
			drawAnnotations(currentBox);
		}
	}

	if (obj == ui->annotationsPreviewWidget || obj == ui->currentAnnotationsDock || obj == ui->annotationPropertiesDock)
	{
		if (event->type() == QEvent::KeyPress) {
			QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
			keyPressEvent(keyEvent);
		}
		else if (event->type() == QEvent::KeyRelease) {
			QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
			keyReleaseEvent(keyEvent);
		}
	}
	
	return QWidget::eventFilter(this, event);
}

void MainWindow::initModalityWindows()
{
    images.clear();
    displayImages.clear();

    Mat empty = Mat::zeros(1, 1, CV_8UC3);
    images << empty << empty << empty << empty;
    for (int i = 0; i < images.size(); i++) {
        displayImages << images[i].clone();
    }

    QStringList windowNames;
    for (int i = Image::RGB; i <= Image::Mask; ++i)
    {
        if (enabledModalities[i])
        {
            windowNames << getModalityName(i);
        }
    }

    viewers.clear();

    scrollAreas.clear();

    // Clear the existing subwindows
    for (int i = 0; i < workWindows.size(); ++i)
    {
        ui->mainArea->removeSubWindow(workWindows[i]);
    }
    workWindows.clear();

    for (int i = 0; i < windowNames.size(); i++) {
        QMdiSubWindow* subwindow = new QMdiSubWindow(this);
        subwindow->setAttribute(Qt::WA_DeleteOnClose);
        subwindow->setWindowTitle(windowNames[i]);
        subwindow->installEventFilter(this);
        QScrollArea* scrollArea = new QScrollArea();
        scrollArea->setBackgroundRole(QPalette::Dark);
        scrollArea->setAlignment(Qt::AlignCenter);
        scrollArea->setWidgetResizable(true);
        scrollArea->installEventFilter(this);
        subwindow->setWidget(scrollArea);
        OpenCVViewer *viewer = new OpenCVViewer();
        viewer->setImage(images[i]);
        //viewer->setMouseTracking(true);
        viewer->installEventFilter(this);
        scrollArea->setWidget(viewer);
        connect(viewer, SIGNAL(wheelScrolled(QWheelEvent*)), this, SLOT(scrollHandler(QWheelEvent*)));
        this->ui->mainArea->addSubWindow(subwindow);
        viewers.append(viewer);
        workWindows.append(subwindow);
        scrollAreas.append(scrollArea);
        subwindow->show();
        // This automatic connection code is replace with the below connections to provide Qt5 compatibility.
        //connect(viewer, SIGNAL(mouseClicked(QMouseEvent*)), this, qFlagLocation(QString("1%1MouseHandler(QMouseEvent*)\0" __FILE__ ":" QTOSTRING(__LINE__)).arg(windowNames[i]).toUtf8().constData()));
        //connect(viewer, SIGNAL(mouseMoved(QMouseEvent*)), this, qFlagLocation(QString("1%1MouseHandler(QMouseEvent*)\0" __FILE__ ":" QTOSTRING(__LINE__)).arg(windowNames[i]).toUtf8().constData()));
    }
    this->ui->mainArea->tileSubWindows();

    // Link scroll between viewports
    for (int i = Image::RGB; i <= Image::Mask; i++) {
        if (enabledModalities[i]) {
            for (int j = 0; j <= Image::Mask; j++) {
                if (j != i && enabledModalities[j]) {
                    connect((QObject*)scrollAreas[getModalityIndex(i)]->horizontalScrollBar(), SIGNAL(valueChanged(int)), (QObject*)scrollAreas[getModalityIndex(j)]->horizontalScrollBar(), SLOT(setValue(int)));
                    connect((QObject*)scrollAreas[getModalityIndex(i)]->verticalScrollBar(), SIGNAL(valueChanged(int)), (QObject*)scrollAreas[getModalityIndex(j)]->verticalScrollBar(), SLOT(setValue(int)));
                }
            }
        }
    }

    // Mouse handling - TODO; find a way to unify the mouse handlers to clean up the switch statement
    for (int i = Image::RGB; i <= Image::Mask; i++) {
        if (enabledModalities[i]) {
            switch (i)
            {
            case Image::RGB:
            {
                connect(viewers[getModalityIndex(Image::RGB)], SIGNAL(mouseClicked(QMouseEvent*)), this, SLOT(RGBMouseHandler(QMouseEvent*)));
                connect(viewers[getModalityIndex(Image::RGB)], SIGNAL(mouseMoved(QMouseEvent*)), this, SLOT(RGBMouseHandler(QMouseEvent*)));
                connect(viewers[getModalityIndex(Image::RGB)], SIGNAL(mouseReleased(QMouseEvent*)), this, SLOT(RGBMouseHandler(QMouseEvent*)));
                break;
            }
            case Image::Thermal:
            {
                connect(viewers[getModalityIndex(Image::Thermal)], SIGNAL(mouseClicked(QMouseEvent*)), this, SLOT(ThermalMouseHandler(QMouseEvent*)));
                connect(viewers[getModalityIndex(Image::Thermal)], SIGNAL(mouseMoved(QMouseEvent*)), this, SLOT(ThermalMouseHandler(QMouseEvent*)));
                connect(viewers[getModalityIndex(Image::Thermal)], SIGNAL(mouseReleased(QMouseEvent*)), this, SLOT(ThermalMouseHandler(QMouseEvent*)));
                break;
            }
            default:
                break;
            }
        }
    }
}

void MainWindow::on_action_Settings_triggered()
{
    int index = currentImage;
    
    if (index < 0) {
        index = 0;
    }

    Mat thermalImg;

    if (index < fileLists[Image::Thermal].size()) {
        thermalImg = readImageEvenIfNonAscii(fileLists[Image::Thermal][index], 1);
    }

    SettingsDialog d(this, thermalImg);
    bool modalitiesChanged = false;

    if (d.exec()) {
        QSettings settings;
        
        filePatterns[Image::RGB] = settings.value("rgbPattern", "rgb/*.png").toString();
        filePatterns[Image::Thermal] = settings.value("thermalPattern", "thermal/*.png").toString();
        csvPath = settings.value("csvPath", "annotations.csv").toString();

        overlayColor = settings.value("overlayColor", QColor(0, 0, 255)).value<QColor>();
        overlayOpacity = settings.value("overlayOpacity", 0.3).toFloat();


        if (enabledModalities[Image::RGB] != settings.value("enableRgb", true).toBool()) {
            modalitiesChanged = true;
        }
        else if (enabledModalities[Image::Thermal] != settings.value("enableThermal", true).toBool()) {
            modalitiesChanged = true;
        }

        enabledModalities[Image::RGB] = settings.value("enableRgb", true).toBool();
        enabledModalities[Image::Thermal] = settings.value("enableThermal", true).toBool();

        if (modalitiesChanged) {
            // Modalities have changed. Reset ui
            fileLists.clear();
            annotations.clear();
            initModalityWindows();
        }

        loadImage(currentImage);
    }
}

void MainWindow::openConfiguration(QString dir, QString calibPath, QString maskPath)
{
    QCoreApplication::setOrganizationName("AAU");
    QCoreApplication::setOrganizationDomain("vap.aau.dk");
    QCoreApplication::setApplicationName("Frame Annotator");
    QSettings settings;

    if (dir.isEmpty())
    {
        dir = settings.value("baseFolder").toString();

        dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), dir, QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    }

    if (dir.length() > 0) {

        settings.setValue("baseFolder", dir);
        baseDir.setPath(dir);

        // Load registration parameters if other modalities than RGB is present
        if (enabledModalities[Image::Depth] || enabledModalities[Image::Thermal])
        {
            // Use advanced, multiple homographies to map between points. Depth is supported.
			QDir topLevelDir = baseDir;
			topLevelDir.cdUp();

            if (calibPath.isEmpty()) {
                if (baseDir.exists("calibVars.yml")) {
                    calibPath = baseDir.absoluteFilePath("calibVars.yml");
                }
				else if (topLevelDir.exists("calibVars.yml")) {
					calibPath = topLevelDir.absoluteFilePath("calibVars.yml");
				}
				else {
                    calibPath = QFileDialog::getOpenFileName(this, tr("Open multi-modal calibration file"), topLevelDir.absolutePath(), "*.yml");
                }
            }

            if (!calibPath.isEmpty()) {
				registrator.loadHomography(calibPath.toStdString());
                settings.setValue("calibBaseFolder", calibPath);
                this->calibPath = calibPath;

            }
            else {
                QMessageBox::critical(this, tr("No registration file present"),
                    QString(tr("A file named calibVars.yml containing the image registration parameters must be present in the directory. Cancelling operation.")),
                    QMessageBox::Ok);
                return;
            }
        }

        baseDir.setFilter(QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);
        baseDir.setSorting(QDir::Name);

        bool missingFiles = false;
        bool inqeualNumberOfFiles = false;

        for (int i = Image::RGB; i <= Image::Thermal; i++) {
            QDir currentDir = baseDir;
            currentDir.cd(filePatterns[i].left(filePatterns[i].lastIndexOf(QRegExp("[\\\\|/]")))); // If the pattern contains folders, cd into the proper folder (the regex matches backslashes or forward slashes. Lots of escaping involved.).
            fileLists[i] = currentDir.entryList(QStringList(filePatterns[i].section(QRegExp("[\\\\|/]"), -1)));
            for (int j = 0; j < fileLists[i].size(); j++) {
                fileLists[i][j] = currentDir.absoluteFilePath(fileLists[i][j]);
            }

            if (enabledModalities[i] && fileLists[i].empty()) {
                missingFiles = true;
            }

            if (enabledModalities[i]) {
                if (i == Image::RGB) {
                    numImages = fileLists[i].size();
                }
                else if (numImages > fileLists[i].size()) {
                    numImages = fileLists[i].size();  // Set the number of images to the lowest common denominator.
                    inqeualNumberOfFiles = true;
                }
            }
        }

        if (missingFiles) {
            QString errorMessage = "There must be at least 1 image for each modality.\n\n";

            for (int i = Image::RGB; i <= Image::Thermal; ++i) {
                if (enabledModalities[i]) {
                    errorMessage.append(QString(getModalityName(i) + " has %1 images.\n").arg(fileLists[i].size()));
                }
            }
            errorMessage.append("\nCheck the file patterns in Settings.");

            QMessageBox::critical(this, "Missing image files", errorMessage, QMessageBox::Ok);
            fileLists.clear();
            return;
        }

        if (inqeualNumberOfFiles) {
            QString warningMessage = "Beware that there is not an equal number of images for all modalities.\n\n";

            for (int i = Image::RGB; i <= Image::Thermal; ++i) {
                if (enabledModalities[i]) {
                    warningMessage.append(getModalityName(i) + QString(" has %1 images.\n").arg(fileLists[i].count()));
                }
            }
            warningMessage.append(QString("\nOnly the first %1 images will be annotated").arg(numImages));

            QMessageBox::warning(this, "Unequal number of image files", warningMessage, QMessageBox::Ok);
        }
		imageSelectorSpinBox->setMaximum(numImages);


        //setCurrentAnnotations(QSet<int>());
        annotations.clear();
        outFileLines.clear();

        outFile.setFileName(baseDir.absoluteFilePath(csvPath));
        if (baseDir.exists(csvPath)) { // Read existing annotations
                                       // Open existing annotation file
                                       // Backup existing annotation file
            baseDir.mkpath("Backup");
            QDir backupDir = baseDir;
            backupDir.cd("Backup");
            outFile.copy(backupDir.absoluteFilePath("annotations-" + QDateTime::currentDateTime().toString("yyyy.MM.dd-hh.mm") + ".csv"));


            outFile.open(QFile::ReadOnly);
            QTextStream stream;
            stream.setDevice(&outFile);
            QStringList topEntry = stream.readLine().split(";", QString::SkipEmptyParts);

            if (topEntry.size() > 7) {
                metaDataNames = topEntry[7].split(',');
                ui->annotationProperties->setRowCount(3 + metaDataNames.size());
            }
            else {
                ui->annotationProperties->setRowCount(3);
                metaDataNames.clear();
            }

            while (!stream.atEnd()) {
                QString line = stream.readLine();
                outFileLines << line;

                QStringList split = line.split(";", QString::SkipEmptyParts);

                if (split.size() > 3) {
                    //Populate the suggested tags
                    if (!suggestedTags.contains(split[2])) {
                        suggestedTags << split[2];
                    }

					int annotationId = split[1].toInt();

					if (!globalIdList.contains(annotationId))
					{
						globalIdList.insert(annotationId, true);
					}
					
                }

				
					
            }
            outFile.close();
        }

        // Disable changing of meta-data fields, since the output file header has been written:
        ui->actionEdit_meta_data_fields->setEnabled(false);

        // Proceed to load the image to obtain image dimensions needed for the registrator module
        loadImage(0, false, false);

        // Set the image dimensions in the registrator module
        if (images.size() > 3 && !images[Image::Thermal].empty()) {
            registrator.setImageWidth(images[Image::Thermal].cols);
            registrator.setImageHeight(images[Image::Thermal].rows);
        }

        // If we have enabled the don't care mask, prompt the user to load it
        QSettings settings;
        bool showMask = settings.value("useMask").toBool();

        if (showMask || !maskPath.isEmpty()) {

			QDir topLevelDir = baseDir;
			QString currentDirMask = baseDir.dirName() + "-mask.png";
			topLevelDir.cdUp();

			if (maskPath.isEmpty())
			{
				if (baseDir.exists("mask.png")) {
					maskPath = baseDir.absoluteFilePath("mask.png");
				}
				else if (topLevelDir.exists("mask.png")) {
					maskPath = topLevelDir.absoluteFilePath("mask.png");
				}
				else if (baseDir.exists(currentDirMask)) {
					maskPath = baseDir.absoluteFilePath(currentDirMask);
				}
				else if (topLevelDir.exists(currentDirMask)) {
					maskPath = topLevelDir.absoluteFilePath(currentDirMask);
				}
				else {
					maskPath = settings.value("maskFolder").toString();
					maskPath = QFileDialog::getOpenFileName(this, tr("Open don't care mask"), topLevelDir.absolutePath(), "*.png");
				}
			}

            if (!maskPath.isEmpty()) {
                settings.setValue("maskFolder", maskPath);
                this->maskPath = maskPath;
				
                Mat invDontCareMaskRgb = readImageEvenIfNonAscii(maskPath, cv::IMREAD_GRAYSCALE);

                // If the modality is thermal, we need to map the mask to the thermal domain
                Mat invDontCareMaskThermal = Mat::zeros(invDontCareMaskRgb.size(), invDontCareMaskRgb.type()), emptyMask;

                registrator.drawRegisteredContours(invDontCareMaskRgb, emptyMask, invDontCareMaskThermal, emptyMask, MAP_FROM_RGB);

                // Invert the don't care masks
                cv::bitwise_not(invDontCareMaskRgb, dontCareMaskRgb);
                cv::bitwise_not(invDontCareMaskThermal, dontCareMaskThermal);
            }
            else {
                settings.setValue("useMask", false);
            }
        }


        for (int i = Image::RGB; i <= Image::Mask; i++) {
            if (enabledModalities[i]) {
                viewers[getModalityIndex(i)]->resize(QSize(viewers[getModalityIndex(i)]->size().width() - 1, viewers[getModalityIndex(i)]->size().height() - 1));
            }
        }

        ui->actionSave_configuration->setEnabled(true);
        allowAnnotations = true;

        // Load the image again, this time with annotations allowed by default
        outFile.open(QFile::WriteOnly);
		loadImage(0);
    }
}


void MainWindow::on_actionOpen_folder_triggered()
{
    openConfiguration();
}

void MainWindow::on_actionUse_Dark_Mode_triggered()
{
	QSettings settings;

	if (ui->actionUse_Dark_Mode->isChecked())
	{
		QFile f(":qdarkstyle/style.qss");
		if (!f.exists())
		{
			printf("Unable to set stylesheet, file not found\n");
		}
		else
		{
			f.open(QFile::ReadOnly | QFile::Text);
			QTextStream ts(&f);
			qApp->setStyleSheet(ts.readAll());

			settings.setValue("useDarkMode", true);
		}
	}
	else {
		qApp->setStyleSheet("");
		settings.setValue("useDarkMode", false);
	}
}

 //   // Settings
 //   QCoreApplication::setOrganizationName("AAU");
 //   QCoreApplication::setOrganizationDomain("vap.aau.dk");
 //   QCoreApplication::setApplicationName("Frame Annotator");
 //   QSettings settings;

 //   QString homeDir = settings.value("homeDir").toString();
 //   
	//QString fileName = QFileDialog::getOpenFileName(this, tr("Open track annotation file"), homeDir, "*.csv");

	//if(fileName.isNull()) {
	//	return;
	//}
 //   
 //   settings.setValue("homeDir", fileName);

	//// Define some directories and create the output directory:
	//baseDirectory.setPath(fileName.section("/", 0, -2));
	//QString saveDir = QString("frameAnnotations-").append(fileName.section("/", -1).section(".", 0,-2));
	//baseDirectory.mkdir(saveDir);
	//saveDirectory.setPath(baseDirectory.absoluteFilePath(saveDir));
	//QString outFileName = "frameAnnotations.csv";



	//if(saveDirectory.exists(outFileName)) {
	//	// If an annotation file already exist, make sure that the user intents to overwrite that.
	//	int ret = QMessageBox::question(this, "Annotation file exists already",
	//								   "An annotation file for this track collection exists already!\n\n"
	//								   "If you continue, the current annotations will be loaded in each frame.\n"
	//								   "Edits will be saved to the file frameAnnotationEdits.csv instead of the standard frameAnnotations.csv.\n"
	//								   "Do you still wish to open this file?",
	//									QMessageBox::Open | QMessageBox::Cancel,
	//									QMessageBox::Cancel);
	//	if(ret == QMessageBox::Cancel) {
	//		return;
	//	}

	//	editAnnotations = true;
	//	inFile.setFileName(saveDirectory.absoluteFilePath(outFileName));
	//	inFile.open(QIODevice::ReadOnly | QIODevice::Text);
	//	outFileName = "frameAnnotationEdits.csv";
 //       ui->retainBoxes->setChecked(false);
 //       ui->retainBoxes->setEnabled(false);
	//}

	//// Read the track collection file:
	//trackCollectionFile.setFileName(fileName);
	//if (!trackCollectionFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
	//	return;
	//}

	//trackCollectionFile.readLine(); // Read and discard the csv-header line.

	//// Prepare output-file:
	//outFile.setFileName(saveDirectory.absoluteFilePath(outFileName));
	//outFile.open(QFile::WriteOnly | QFile::Truncate);
	//outStream.setDevice(&outFile);
	//if(editAnnotations) {
	//	QString header = inFile.readLine();
	//	QStringList fields = header.split(';');
	//	metaDataNames = fields[6].split(',');
	//	ui->annotationProperties->setRowCount(1+metaDataNames.size());
	//	outStream << header;
 //       //currentInputLine = inFile.readLine();
 //       QTextStream inStream(&inFile);
 //       inFileLines = inStream.readAll().split('\n');
 //       qDebug() << inFileLines[0];
	//} else {
	//	outStream << "Filename;Annotation tag;Upper left corner X;Upper left corner Y;Lower right corner X;Lower right corner Y;";
	//	if(metaDataNames.size() > 0) {
	//		outStream << metaDataNames.join(",") << ";";
	//	}
	//	outStream << "Origin file;Origin frame number;Origin track;Origin track frame number\n";
	//}


	//loadNextVideo(); // Load in the first video.
//}

void MainWindow::saveCSVFile()
{
    // Clear and rewrite the file
    outFile.resize(0);
    QTextStream outStream(&outFile);
    
    // Write file header
    outStream << "Filename;Object ID;Annotation tag;Upper left corner X;Upper left corner Y;Lower right corner X;Lower right corner Y;";
    if(metaDataNames.size() > 0) {
    	outStream << metaDataNames.join(",") << ";";
    }
    outStream << '\n';
    for (QStringList::Iterator it = outFileLines.begin(); it != outFileLines.end(); ++it) {
        outStream << *it << '\n';
    }

    outStream.flush();
}

void MainWindow::loadNextImage()
{


    loadImage(currentImage + 1);
}

cv::Mat MainWindow::readImageEvenIfNonAscii(const QString& imagePath, const int imreadFlags)
{
	bool containsNonAscii = imagePath.contains(QRegularExpression(QStringLiteral("[^\\x{0000}-\\x{007F}]")));
	cv::Mat image;

	if (containsNonAscii) {
		// https://github.com/opencv/opencv/issues/4292
		// CV::IMREAD wont work on Windows - use the following approach instead
		// We apply method 2 from https://stackoverflow.com/questions/39356362/c-opencv-fastest-way-to-read-a-file-containing-non-ascii-characters-on-windo

		QFile file(imagePath);
		QByteArray buffer;
		
		if (file.open(QIODevice::ReadOnly)) {
			buffer = file.readAll();
		}

		cv::Mat matBuffer(1, buffer.size(), CV_8U, buffer.data());
		image = cv::imdecode(matBuffer, imreadFlags);
	}
	else {
		// Only contains ASCII-characters, go with the conventional approach
		image = cv::imread(imagePath.toStdString(), imreadFlags);
	}
	
	return image;
}

QString MainWindow::maskFilename(int index, int modality)
{
    assert(index >= 0 && index < numImages);
    assert(modality >= Image::RGB && modality <= Image::Thermal);

    QString maskFile;

    if (enabledModalities[modality]) {
        QRegExp extractFilename("([^/]+)\\.[^\\.]+$");
        extractFilename.indexIn(fileLists[modality][index]);
        maskFile = extractFilename.cap(1);
        maskFile += ".png";
    }
    return maskFile;
}

void MainWindow::loadImage(int index, bool saveChanges, bool showAnnotations, bool isNeighbour)
{
    if (index < 0 || index >= numImages) {
        return;
    }

	// Get the old id of the annotation
	int oldId = 0;

	if (currentBox >= 0 && currentBox < annotations.size()) {
		oldId = annotations[currentBox].id();
	}

    if (saveChanges) {
        saveAnnotations();
    }

    actionHistory.clear();
    deletedAnnotations.clear();
    QString readableFileInfo;

	QList<cv::Mat> previousImages = images;


    for (int i = Image::RGB; i <= Image::Thermal; i++) {
        if (enabledModalities[i]) {
            			

			images[i] = readImageEvenIfNonAscii(fileLists[i][index], cv::IMREAD_COLOR);

			bool greyscale = checkForGreyscale(images[i]);

			if (greyscale) {
				// Image is grayscale. Open as dedicated greyscale image instead
				images[i] = readImageEvenIfNonAscii(fileLists[i][index], cv::IMREAD_ANYDEPTH);

				if (i == Image::RGB) {
					// Perform histogram equalization

					try {
						cv::Mat srcCopy = images[i].clone(); // TODO: Perform check for image depth - 8 bit or 16 bit?

						int depth = images[i].depth();

						switch (depth)
						{
						case CV_8U:
						{
							cv::normalize(srcCopy, images[i], 0, 255, cv::NORM_MINMAX);
							break;
						}
						case CV_16U:
						{
							cv::normalize(srcCopy, images[i], 0, 65535, cv::NORM_MINMAX);
							break;
						}
						}

					}
					catch (cv::Exception e) {
						qDebug() << e.what();
					}
				}

			}

            if (i == Image::Thermal) {
                // If the image is thermal, adjust the contrast and brightness
                QSettings settings;
                double alpha = settings.value("thermalContrast", 1).toDouble();
                double beta = settings.value("thermalBrightness", 0).toDouble();

                if (alpha != 1 || beta != 0) {
                    for (auto y = 0; y < images[i].rows; ++y) {
                        for (auto x = 0; x < images[i].cols; ++x) {

							if (!greyscale) {
								int newVal = cv::saturate_cast<uchar>(alpha*(images[i].at<cv::Vec3b>(y, x)[0]) + beta);
								images[i].at<cv::Vec3b>(y, x)[0] = newVal;
								images[i].at<cv::Vec3b>(y, x)[1] = newVal;
								images[i].at<cv::Vec3b>(y, x)[2] = newVal;
							}
							else {
								int depth = images[i].depth();

								if (depth == CV_8U) {
									int newVal = cv::saturate_cast<uchar>(alpha*(images[i].at<cv::Vec3b>(y, x)[0]) + beta);
									images[i].at<uchar>(y, x) = newVal;
								}
								else if (depth == CV_16U){
									auto newVal = alpha*(images[i].at<unsigned short>(y, x)) + beta;
									images[i].at<unsigned short>(y, x) = newVal;
								}
							}

                        }
                    }
                }
            }

            readableFileInfo.append("\t\t" + getModalityName(i) + ": ");
            QFileInfo fileInfo(fileLists[i][index]);
            readableFileInfo.append(fileInfo.baseName());
        }
    }

	// Read annotations and images for the previous and next five frames, if they exist.
	// At the current stage, only RGB is supported
	std::map<int, AnnotatedImage> existingAnnotations;

	for (auto i = index - 5; i <= index + 5; ++i)
	{
		if ((i == index) || (i < 0) || (i >= numImages))
		{
			continue;
		}

		AnnotatedImage annotatedImage;
		annotatedImage.image = readImageEvenIfNonAscii(fileLists[Image::RGB][i], 1);

		QString maskFile = maskFilename(i, Image::RGB);

		QStringList ann = outFileLines.filter(maskFile);
		for (int j = 0; j < ann.size(); j++) {
			annotatedImage.annotations.append(csvToAnnotation(ann[j]));
		}

		existingAnnotations[i] = annotatedImage;
	}

	annotationPreviewer.setAnnotations(existingAnnotations);


    QList<BoxAnnotation> oldAnnotations = annotations;
    annotations.clear();
    QString maskFile = maskFilename(index, Image::RGB);

    if (maskFile.length() > 0) {
        
        QStringList existingAnnotations = outFileLines.filter(maskFile);
        for (int i = 0; i < existingAnnotations.size(); i++) {
            annotations.append(csvToAnnotation(existingAnnotations[i]));            
        }
    }

    bool retainAnnotations = false;

    if (index == (currentImage + 1) || 
		(isNeighbour && index > currentImage && index < (currentImage + interpolationStepSizeSpinBox->value() + 1))) {
        if (functionActions[Function::retainNextAnnotation]->isChecked()) {
            retainAnnotations = true;
        }
    }
    if (index == (currentImage - 1) || 
		(isNeighbour && index < currentImage && index > (currentImage - interpolationStepSizeSpinBox->value() - 1))) {
        if (functionActions[Function::retainPreviousAnnotation]->isChecked()) {
            retainAnnotations = true;
        }
    }

    if (retainAnnotations) {
        for (int i = 0; i < oldAnnotations.size(); i++) {
			if (!oldAnnotations[i].isFinished()) {
				if (!idInList(oldAnnotations[i].id(), annotations)) {
					// Check if the isFinished flag has been set in the previous annotations with the same id
					// If so, we should not append this annotation
					bool isPreviouslyFinished = false;

					auto it = existingAnnotations.begin();

					while (it != existingAnnotations.end() && it->first != index) {
						for (auto previousAnnnotation : it->second.annotations) {
							if (previousAnnnotation.id() == oldAnnotations[i].id() &&
								previousAnnnotation.isFinished()) {
								isPreviouslyFinished = true;
								break;
							}
						}

						it++;
					}

					if (!isPreviouslyFinished) {
						annotations.append(oldAnnotations[i]);
					}
				}
			}
        }
    }

	// Try to find the id of the old annotation in the current annotations and, if possible
	// set this to the current annotation

	bool idFound = false;

	if (currentBox >= 0 && currentBox < annotations.size()) {
		if (annotations[currentBox].id() == oldId) {
			idFound = true;
		}
	}

	if (!idFound) {
		for (int i = 0; i < annotations.size(); ++i) {
			if (annotations[i].id() == oldId) {
				idFound = true;
				currentBox = i;
			}
		}
	}

	if (!idFound) {
		currentBox = annotations.size() - 1;
		drawingInitiated = false;
	}

	currentImage = index;

    if (showAnnotations) {
        drawAnnotations(currentBox);
        updatePropertiesTable();

    }

	updateControlButtons();
	updateCurrentAnnotationsTable();

    imageSelectorSpinBox->setValue(currentImage + 1);
    fileNumber->setText(QString("Image: %1/%2 %3").arg(currentImage + 1).arg(numImages).arg(readableFileInfo));

}

bool MainWindow::idInList(int id, QList<BoxAnnotation> list)
{
    for (int i = 0; i < list.size(); i++) {
        if (id == list[i].id()) {
            return true;
        }
    }
    return false;
}

BoxAnnotation MainWindow::csvToAnnotation(const QString& csv)
{   
    qDebug() << "Loading csv:" << csv;
    QStringList fields = csv.split(";");

    if (fields.length() < 8) {
        // Return empty annotation
        BoxAnnotation annotation(0);
        return annotation;
    }

    BoxAnnotation annotation(fields[1].toInt(), metaDataNames);
    annotation.setA(Point(fields[3].toInt(), fields[4].toInt()));
    annotation.setB(Point(fields[5].toInt(), fields[6].toInt()));

    QString tag = fields[2];
    int index = replaceInTrack.indexOf(tag);
    if (index > 0 && index <= replaceInTrack.size()) {
        tag = replaceInTrack[index - 1];
    }
    annotation.setTag(tag);

    QStringList meta = fields[7].split(',');
    for (int j = 0; j < (int)meta.size(); j++) {
        if (!meta[j].isEmpty() && (j < metaDataNames.size())) {
            if (meta[j].toInt() == 1) {
                annotation.setMetaData(j, true);
            }
            else {
                annotation.setMetaData(j, false);
            }
        }
    }

	if (fields.size() > 8) {
		bool isFinished = fields[8].toInt() > 0 ? true : false;
		annotation.setIsFinished(isFinished);
	}

	if (fields.size() > 9) {
		annotation.setBonusInfo(fields[9].toStdString());
	}

    return annotation;
}

QString MainWindow::changeAnnotationId(const QString& csv, int newId)
{
	QStringList fields = csv.split(";");

	// Change ID
	if (fields.size() >= 2)
	{
		fields[1] = QString::number(newId);
	}

	return fields.join(";");
}

QString MainWindow::annotationToCsv(int frameNumber, const BoxAnnotation &annotation)
{
	QStringList line;
	QString fileName = maskFilename(frameNumber, Image::RGB);

	line << fileName << ";" << QString::number(annotation.id()) << ";" << annotation.tag() << ";" << QString::number(annotation.a().x) << ";" << QString::number(annotation.a().y) << ";" << QString::number(annotation.b().x) << ";" << QString::number(annotation.b().y) << ";";
	for (int j = 0; j < (int)metaDataNames.size(); j++) {
		line << QString::number(annotation.getMetaData(j));
		if (j < (int)metaDataNames.size() - 1) {
			line << ",";
		}
	}

	line << ";";

	// Insert the isFinished information at the end of the string
	QString isFinished = annotation.isFinished() ? "1" : "0";
	line << isFinished << ";";
	line << QString::fromStdString(annotation.getBonusInfo()) << ";";

	qDebug() << "Writing CSV: " << line.join("");


	return line.join("");
}

void MainWindow::insertAnnotationInList(int frameNumber, const BoxAnnotation &annotation)
{
	QString line = annotationToCsv(frameNumber, annotation);

	// Search for existing annotations with the same frame name and object id
	qDebug() << QString("%1;%2").arg(maskFilename(currentImage, Image::RGB)).arg(QString("%1").arg(annotation.id()) + "(;[^;]*){1,9};");
	if (!outFileLines.empty()) {
		qDebug() << outFileLines[0];
	}

	QStringList existingAnnotations = getExistingAnnotations(maskFilename(frameNumber, Image::RGB), annotation.id());//outFileLines.filter(QRegExp(QString("%1;%2").arg(maskFilename(currentImage, Image::RGB)).arg(QString("%1").arg(annotations[i].id()) + "(;[^;]*){1,9};")));
	if (existingAnnotations.size() > 0) {
		outFileLines[outFileLines.indexOf(existingAnnotations[0])] = line;
	}
	else {
		outFileLines << line;
	}
}

void MainWindow::drawAnnotations(int highlight, Point boxInProgressPointB) {

	Scalar normalColor = Scalar(0, 255, 0);
	Scalar highlightColor = Scalar(0,255,255);
	Scalar isFinishedColor = Scalar(0, 0, 255);

	QSettings settings;
	if (!settings.value("useRedColorIsFinished", 1).toBool())
	{
		isFinishedColor = normalColor;
	}

    bool showMask = settings.value("useMask").toBool() && functionActions[Function::showDontCareMask]->isChecked();
    vector<Scalar> colors = cvUtils::distinctColors(1, overlayColor.hue());

    for (int i = Image::RGB; i < Image::Mask; ++i) {
        if (enabledModalities[i]) {
            displayImages[i] = images[i].clone();

            // Overlay the don't care mask if appropriate
            if (showMask && (dontCareMaskRgb.rows > 0)) {
                if (i == Image::Thermal) {
                    displayImages[i] = cvUtils::colorOverlay(Scalar(overlayColor.blue(),
                        overlayColor.green(), overlayColor.red()), overlayOpacity, displayImages[i],
                        dontCareMaskThermal);

                }
                else {
                    displayImages[i] = cvUtils::colorOverlay(Scalar(overlayColor.blue(),
                        overlayColor.green(), overlayColor.red()), overlayOpacity, displayImages[i],
                        dontCareMaskRgb);
                }

            }
        }
    }

	QList<Mat> rectangleImages = displayImages; // Make shallow copy

	if (ui->nonSelectedAnnotationsTransparencySlider->value() != 0)
	{
		// If the transparency slider is activated, we need some additional measures to produce 
		// transparent bounding boxes
		for (int imageIndex = 0; imageIndex < rectangleImages.size(); ++imageIndex)
		{
			rectangleImages[imageIndex] = displayImages[imageIndex].clone();
		}
	}

	for(int i = 0; i < (int)annotations.size(); i++) {

        for (int j = Image::RGB; j < Image::Mask; ++j) {
            if (enabledModalities[j]) {
                // Get the coordinates for the current modality
                Point2f aCoord = annotations[i].a();
                Point2f bCoord = annotations[i].b();
                Point2f inProgressPoint = boxInProgressPointB;

                if (j == Image::Thermal) {
                    // If the current modality is thermal, we need to transfer the coordinates from RGB to thermal
                    vector<Point2f> vecRgbCoord, vecTCoord, vecDCoord;

                    vecRgbCoord.push_back(aCoord);
                    vecRgbCoord.push_back(bCoord);
                    vecRgbCoord.push_back(inProgressPoint);
                    registrator.computeCorrespondingThermalPointFromRgb(vecRgbCoord, vecTCoord, vecDCoord);

                    assert(vecTCoord.size() == 3);
                    aCoord = vecTCoord[0];
                    bCoord = vecTCoord[1];
                    inProgressPoint = vecTCoord[2];
                }

                // Draw the box for the annotation:
                if (i == highlight) {
                    if (inProgressPoint.x >= 0 && inProgressPoint.y >= 0) {
                        rectangle(displayImages[j], aCoord, inProgressPoint, highlightColor);
                    }
                    else {
                        rectangle(displayImages[j], aCoord, bCoord, highlightColor);
                    }

					if (ui->nonSelectedAnnotationsTransparencySlider->value() != 0)
					{
						if (inProgressPoint.x >= 0 && inProgressPoint.y >= 0) {
							rectangle(rectangleImages[j], aCoord, inProgressPoint, highlightColor);
						}
						else {
							rectangle(rectangleImages[j], aCoord, bCoord, highlightColor);
						}
					}

					// Update current annotations table
					ui->currentAnnotationsTable->selectRow(highlight);
                }
                else {
					if (ui->nonSelectedAnnotationsTransparencySlider->value() != 0)
					{
						if (annotations[i].isFinished()) {
							rectangle(rectangleImages[j], aCoord, bCoord, isFinishedColor); // Mark in red if last frame of box
						}
						else {
							rectangle(rectangleImages[j], aCoord, bCoord, normalColor);
						}

						// Add text if a tag is set:
						if (annotations[i].tag().length() > 0 && functionActions[Function::displayTag]->isChecked()) {
							putText(rectangleImages[j], annotations[i].tag().toStdString(), Point(aCoord.x, aCoord.y - 2), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 255), 1);
						}
					}
					else {

						if (annotations[i].isFinished()) {
							rectangle(displayImages[j], aCoord, bCoord, isFinishedColor); // Mark in red if last frame of box
						}
						else {
							rectangle(displayImages[j], aCoord, bCoord, normalColor);
						}

						// Add text if a tag is set:
						if (annotations[i].tag().length() > 0 && functionActions[Function::displayTag]->isChecked()) {
							putText(displayImages[j], annotations[i].tag().toStdString(), Point(aCoord.x, aCoord.y - 2), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 255), 1);
						}
					}
                }

                // Add markers for true meta-data:
                for (int k = 0; k < annotations[i].getMetaDataSize(); k++) {
                    if (annotations[i].getMetaData(k)) {
                        rectangle(displayImages[j], Point(aCoord.x + 1 + 5 * k, aCoord.y - 1), Point(aCoord.x + 1 + 5 * k + 2, aCoord.y - 3), Scalar(0, 0, 255), -1);
                    }
                }
            }
        }
	}    

	if (ui->nonSelectedAnnotationsTransparencySlider->value() != 0)
	{
		// Blend the rectangle and the image
		double alpha = ((double)ui->nonSelectedAnnotationsTransparencySlider->value()) / 100.;

		for (auto imageIndex = 0; imageIndex < rectangleImages.size(); ++imageIndex)
		{
			addWeighted(rectangleImages[imageIndex], 1. - alpha, displayImages[imageIndex], alpha, 0., displayImages[imageIndex]);
		}
	}
    
    for (int i = Image::RGB; i < Image::Mask; ++i) {
        if (enabledModalities[i]) {
            viewers[getModalityIndex(i)]->setImage(displayImages[i]);
        }
    }

	// Find orientation of annotation preview widget
	Qt::Orientation currentOrientation =
		ui->annotationsPreviewWidget->size().height() < ui->annotationPreviewArea->size().width() ?
		Qt::Horizontal : Qt::Vertical;

	// Update current annotation for preview
	AnnotatedImage currentAnnotation;
	currentAnnotation.image = images[Image::RGB];
	currentAnnotation.frameNumber = currentImage;
	currentAnnotation.annotations = annotations;
	annotationPreviewer.setAnnotation(currentImage, currentAnnotation);
	QWidget* centralWidget = NULL;

	if (annotations.size() > highlight && (highlight >= 0)) {
		ui->annotationPreviewArea->setWidget(annotationPreviewer.getPreview(annotations[highlight].id(), 
			currentImage, currentOrientation, centralWidget));
	}
	else {
		ui->annotationPreviewArea->setWidget(annotationPreviewer.getPreview(-1, currentImage,
			currentOrientation, centralWidget));
	}

	ui->annotationPreviewArea->ensureWidgetVisible(centralWidget);

}

int MainWindow::inAnyBox(Point position)
{
	for(int i = 0; i < (int)annotations.size(); i++) {
		if(annotations[i].box().contains(position)) {
			return i;
		}
	}
	return -1;
}

void MainWindow::imageMouseHandler(int image, QMouseEvent* event)
{
	if(!allowAnnotations) {
		return;
	}

    Point pt = viewers[getModalityIndex(image)]->mapPoint(Point(event->x(), event->y()));

    if (image == Image::Thermal) {
        // If we have marked something in the thermal image, we need to map the coordinates to the RGB domain
        vector<Point2f> vecRgbCoord, vecTCoord;
        vecTCoord.push_back(pt);

        registrator.computeCorrespondingRgbPointFromThermal(vecTCoord, vecRgbCoord);
        assert(vecRgbCoord.size() == 1);
        
        pt = vecRgbCoord[0];
    }

	if(drawingInitiated) {
		Scalar color;
		if(event->button() == Qt::LeftButton) {
			drawingInitiated = false;
			annotations[currentBox].setB(pt);
			drawAnnotations(currentBox);
            actionHistory.append(-2); // -2 indicates that we have added a new annotation

			if (annotations[currentBox].tag().isEmpty()) {
				changeTag();
			}

		} else {
			drawAnnotations(currentBox, pt);
		}

	} else if(event->button() == Qt::LeftButton) {
		int boxId = inAnyBox(pt);
		if(boxId >= 0) {
			currentBox = boxId;
			drawAnnotations(currentBox);
			return; 
		}

		annotations.push_back(BoxAnnotation(getNewAnnotationId(), metaDataNames));
		currentBox = annotations.size()-1;
		annotations[currentBox].setA(pt);
		drawingInitiated = true;


    }
    else if (event->button() == Qt::RightButton) {
        // Right click does not draw anything, only selects boxes
        int boxId = inAnyBox(pt);
        if (boxId >= 0) {
            currentBox = boxId;
            drawAnnotations(currentBox);
            return;
        }
    }

	updatePropertiesTable();
	updateCurrentAnnotationsTable();
}

void MainWindow::RGBMouseHandler(QMouseEvent* event)
{
    imageMouseHandler(Image::RGB, event);
}

void MainWindow::ThermalMouseHandler(QMouseEvent* event)
{
    imageMouseHandler(Image::Thermal, event);
}

void MainWindow::scrollHandler(QWheelEvent* event)
{
	if (event->orientation() == Qt::Vertical && QApplication::keyboardModifiers() == Qt::ControlModifier) {
		if (event->delta() > 0) {
			zoom(1);
		}
		else {
			zoom(-1);
		}
	}
	else {
		event->ignore();
	}
}

void MainWindow::showDontCareMask()
{
	drawAnnotations();
}

void MainWindow::zoom(int direction, float magnitude)
{
	int dirMultiplier;
	if (direction >= 0) {
		dirMultiplier = 1;
	}
	else {
		dirMultiplier = -1;
	}

	QSize newSize = viewers[getModalityIndex(Image::RGB)]->size() + viewers[getModalityIndex(Image::RGB)]->size()*magnitude*dirMultiplier;

	if (newSize.width() > 50) {
		float hScroll = 0.5, vScroll = 0.5;
		// Find the current scoll position:
		if (scrollAreas[Image::RGB]->horizontalScrollBar()->maximum() > 0) {
			hScroll = (float)scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->value() / (scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->maximum() - scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->minimum());
			vScroll = (float)scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->value() / (scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->maximum() - scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->minimum());
		}
		
		if (enabledModalities[Image::RGB]) {
			scrollAreas[getModalityIndex(Image::RGB)]->setWidgetResizable(false);
			viewers[getModalityIndex(Image::RGB)]->resize(newSize);
		}
		if (enabledModalities[Image::Thermal]) {
			scrollAreas[getModalityIndex(Image::Thermal)]->setWidgetResizable(false);
			viewers[getModalityIndex(Image::Thermal)]->resize(newSize);
		}
		

		// Set scroll to the same spot:
		if (scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->maximum() > 0) {
			scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->setValue((scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->maximum() - scrollAreas[getModalityIndex(Image::RGB)]->horizontalScrollBar()->minimum())*hScroll);
			scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->setValue((scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->maximum() - scrollAreas[getModalityIndex(Image::RGB)]->verticalScrollBar()->minimum())*vScroll);
		}
	}
}

void MainWindow::manipulateCurrentBox(int xShift, int yShift, int verticalChange, int horisontalChange)
{
	if (currentBox < 0) {
		return;
	}

	

	if (xShift < 0) {
		annotations[currentBox].moveLeft(xShift * (-1));
	}
	else if (xShift > 0) {
		annotations[currentBox].moveRight(xShift);
	}

	if (yShift < 0) {
		annotations[currentBox].moveUp(yShift * (-1));
	}
	else if (yShift > 0) {
		annotations[currentBox].moveDown(yShift);
	}

	if (verticalChange < 0) {
		annotations[currentBox].shrinkVertical(verticalChange * (-1));
	} 
	else if (verticalChange > 0) {
		annotations[currentBox].growVertical(verticalChange);
	}

	if (horisontalChange < 0) {
		annotations[currentBox].shrinkHorizontal(horisontalChange * (-1));
	} 
	else if (horisontalChange > 0) {
		annotations[currentBox].growHorizontal(horisontalChange);
	}

	actionHistory.append(currentBox);
	drawAnnotations(currentBox);
	updatePropertiesTable();
}

void MainWindow::zoomIn()
{
	zoom(1);
}

void MainWindow::zoomOut()
{
	zoom(-1);
}

void MainWindow::zoomFit()
{
	if (enabledModalities[Image::RGB]) {
		scrollAreas[getModalityIndex(Image::RGB)]->setWidgetResizable(true);
	}

	if (enabledModalities[Image::Thermal]) {
		scrollAreas[getModalityIndex(Image::Thermal)]->setWidgetResizable(true);
	}
}

void MainWindow::zoomActual()
{	
	if (enabledModalities[Image::RGB]) {
		scrollAreas[getModalityIndex(Image::RGB)]->setWidgetResizable(false);
		viewers[getModalityIndex(Image::RGB)]->resize(QSize(displayImages[Image::RGB].cols, displayImages[Image::RGB].rows));
	}

	if (enabledModalities[Image::Thermal]) {
		scrollAreas[getModalityIndex(Image::Thermal)]->setWidgetResizable(false);
		viewers[getModalityIndex(Image::Thermal)]->resize(QSize(displayImages[Image::Thermal].cols, displayImages[Image::Thermal].rows));
	}
}

void MainWindow::moveBBUp()
{
	manipulateCurrentBox(0, -1 * boxAdjustAmount, 0, 0);
}

void MainWindow::moveBBDown()
{
	manipulateCurrentBox(0, 1 * boxAdjustAmount, 0, 0);
}

void MainWindow::moveBBLeft()
{
	manipulateCurrentBox(-1 * boxAdjustAmount, 0, 0, 0);
}

void MainWindow::moveBBRight()
{
	manipulateCurrentBox(1 * boxAdjustAmount, 0, 0, 0);
}

void MainWindow::expandBBVertical()
{
	manipulateCurrentBox(0, 0, 1 * boxAdjustAmount, 0);
}

void MainWindow::shrinkBBVertical()
{
	manipulateCurrentBox(0, 0, -1 * boxAdjustAmount, 0);
}

void MainWindow::expandBBHorizontal()
{
	manipulateCurrentBox(0, 0, 0, 1 * boxAdjustAmount);
}

void MainWindow::shrinkBBHorizontal()
{
	manipulateCurrentBox(0, 0, 0, -1 * boxAdjustAmount);
}

void MainWindow::previousImage()
{
	int step = shiftKeyPressed ? interpolationStepSizeSpinBox->value() : 1;

	if (step > 1) {
		std::shared_ptr<TemporalInterpolator> interpolator = nullptr;

		if (functionActions[Function::retainPreviousAnnotation]->isChecked() && 
			functionActions[Function::interpolateBetweenAnnotations]->isChecked())
		{
			// Save the current annotations to use when we are moving frames
			interpolator = std::make_shared<TemporalInterpolator>(currentImage, annotations);
		}

		// Load the next image n steps before
		loadImage(currentImage - step, true, true, true);

		if (interpolator != nullptr)
		{
			// Push back the current annotations to the interpolator, such that 
			// it is available when the next frames are saved
			annotationInterpolators.push_back(interpolator);
		}
	}
	else {
		loadImage(currentImage - step);
	}

}

void MainWindow::nextImage()
{
	int step = shiftKeyPressed ? interpolationStepSizeSpinBox->value() : 1;

	if (step > 1) {
		std::shared_ptr<TemporalInterpolator> interpolator = nullptr;

		if (functionActions[Function::retainNextAnnotation]->isChecked() && 
			functionActions[Function::interpolateBetweenAnnotations]->isChecked())
		{
			// Save the current annotations to use when we are moving frames
			interpolator = std::make_shared<TemporalInterpolator>(currentImage, annotations);
		}

		// Load the next image n steps ahead
		loadImage(currentImage + step, true, true, true);

		if (interpolator != nullptr)
		{
			// Push back the current annotations to the interpolator, such that 
			// it is available when the next frames are saved
			annotationInterpolators.push_back(interpolator);
		}
	}
	else {
		loadImage(currentImage + step);
	}
}

void MainWindow::jumpToImage()
{
	loadImage(imageSelectorSpinBox->value() - 1);
}

void MainWindow::writeSettings(QString prefix)
{
	QSettings settings;
	settings.beginGroup("mainWindow");
	settings.setValue(prefix + "geometry", saveGeometry());
	settings.setValue(prefix + "windowState", saveState());
	settings.endGroup();
}

void MainWindow::readSettings(QString prefix)
{
	QSettings settings;
	settings.beginGroup("mainWindow");
	restoreGeometry(settings.value(prefix + "geometry").toByteArray());
	restoreState(settings.value(prefix + "windowState").toByteArray());
	settings.endGroup();
}


int MainWindow::getNewAnnotationId()
{
    int id = 1;

	for (auto& key : globalIdList.keys())
	{
		if (key == id)
		{
			id++;
		}
	}

    for (int i = 0; i < annotations.size(); i++) {
        if (annotations[i].id() == id) {
            id = annotations[i].id() + 1;
        }
    }

	globalIdList.insert(id, true);

    return id;
}

void MainWindow::on_annotationProperties_cellDoubleClicked(int row, int column)
{
	if(column == 0) {
		return; // We're never interested in detecting changes for the first column, since the user cannot change them.
	}
    if (row == 0) {
        changeTag();
    } else if (row == 1){
        // Change the annotation ID
        return;
	} else if(row > 1 && row < annotations[currentBox].getMetaDataSize() + 2) {
		toggleMeta(row-2);
	}
	else if (row > 1 && row == annotations[currentBox].getMetaDataSize() + 2) {
		toggleIsFinishedState();
	}
	else {
		return;
	}

	drawAnnotations(currentBox);
	updatePropertiesTable();
}

void MainWindow::on_annotationProperties_cellChanged(int row, int column)
{
    if (annotations.size() == 0) {
        return;
    }

    if (row == 1 && column == 1) {
        // We are currently trying to overwrite the id
        bool overwriting = false;
        for (int i = 0; i < annotations.size(); i++) {
            if (annotations[i].id() == ui->annotationProperties->item(row, column)->text().toInt()) {
                overwriting = true;
                break;
            }
        }

        if (overwriting) {
            if (QMessageBox::No == QMessageBox::question(this, "Merge with existing annotation?", "You are about to change the ID of an annotation to another ID which exists already. This will cause the two annotations to be merged, and may lead to lost data. Are you sure you want to continue?", QMessageBox::No, QMessageBox::Yes)) {
                return;
            }
        }

		int newId = ui->annotationProperties->item(row, column)->text().toInt();

		if (!overwriting)
		{
			std::map<int, BoxAnnotation> sameIdTagAnnotations;
			findFutureAnnotationsWithSameIdTag(sameIdTagAnnotations);

			for (auto& annotation : sameIdTagAnnotations)
			{
				QString line = annotationToCsv(annotation.first, annotation.second);

				int idx = outFileLines.indexOf(line);

				if (idx >= 0 && idx < outFileLines.size())
				{
					annotation.second.setId(newId);
					outFileLines[idx] = annotationToCsv(annotation.first, annotation.second); // Replace with new id
				}
			}
		}

        int oldId = annotations[currentBox].id();
        annotations[currentBox].setId(newId);
        deleteById(oldId); // Remove the old annotation.
		globalIdList.insert(newId, true);

		drawAnnotations(currentBox);
		updatePropertiesTable();
		updateCurrentAnnotationsTable();
    }
}

void MainWindow::on_currentAnnotationsTable_itemSelectionChanged()
{
	if (!ui->currentAnnotationsTable->selectionModel()->selectedRows().isEmpty())
	{
		int row = ui->currentAnnotationsTable->selectionModel()->selectedRows().first().row();

		if (row >= 0 && row < annotations.size()) {
			currentBox = row;
			drawAnnotations(currentBox);
			updatePropertiesTable();
		}
	}
}

void MainWindow::on_nonSelectedAnnotationsTransparencySlider_valueChanged(int value)
{
	drawAnnotations(currentBox);
}


void MainWindow::deleteAnnotations(QList<int> deleteIds, bool markInHistory)
{
    for (int i = 0; i < deleteIds.size(); i++) {
        for (int j = 0; j < annotations.size(); j++) {
            if (annotations[j].id() == deleteIds[i]) {
                deleteById(annotations[j].id());

                if (markInHistory) {
                    deletedAnnotations.append(annotations.takeAt(j));
                    actionHistory.append(-1); // -1 marks that we have deleted an annotation
                }
                else {
                    annotations.removeAt(j);
                }

                break;
            }

        }
    }
}

void MainWindow::deleteById(int id)
{
	QRegularExpression expression(QString("%1;%2").arg(maskFilename(currentImage, Image::RGB)).arg(QString("%1").arg(id) + "(;[^;]*){1,9};"));

	QStringList annnotationsToDelete = outFileLines.filter(expression);

    if (annnotationsToDelete.size() > 0) {
		int idx = outFileLines.indexOf(annnotationsToDelete[0]);

		if (idx >= 0 && idx < outFileLines.size())
		{
			outFileLines.removeAt(idx);
		}
    }
}

void MainWindow::updateControlButtons()
{
	ui->utilityToolBar->setEnabled(true);

	if (currentImage < numImages - 1) {
		functionActions[Function::nextImage]->setEnabled(true);
	}
	else {
		functionActions[Function::nextImage]->setEnabled(false);
	}
	if (currentImage > 0) {
		functionActions[Function::previousImage]->setEnabled(true);
	}
	else {
		functionActions[Function::previousImage]->setEnabled(false);
	}
	if (numImages > -1) {
		functionActions[Function::jumpToImage]->setEnabled(true);
		imageSelectorSpinBox->setEnabled(true);
	}
	else {
		functionActions[Function::jumpToImage]->setEnabled(false);
		/*for(int i = Tool::selector; i <= Tool::subtractFromMask; i++) {
		toolActions[i]->setEnabled(false);
		}*/
		imageSelectorSpinBox->setEnabled(false);
	}
	
	for (int i = Function::save; i <= Function::shrinkBBHorizontal; i++) {
		functionActions[i]->setEnabled(numImages > 0);
	}
	

}

void MainWindow::updatePropertiesTable()
{
    ui->annotationProperties->blockSignals(true);
	ui->annotationProperties->clearContents();

	QTableWidgetItem* currentItem = new QTableWidgetItem("Tag:");
	currentItem->setFlags(Qt::ItemIsEnabled);
	ui->annotationProperties->setItem(0, 0, currentItem);

    currentItem = new QTableWidgetItem("ID:");
    currentItem->setFlags(Qt::ItemIsEnabled);
    ui->annotationProperties->setItem(1, 0, currentItem);

	for(int i = 0; i < (int)metaDataNames.size(); i++) {
		currentItem = new QTableWidgetItem(QString("%1:").arg(metaDataNames[i]));
		currentItem->setFlags(Qt::ItemIsEnabled);
		ui->annotationProperties->setItem(i+2, 0, currentItem);
	}

	currentItem = new QTableWidgetItem("Status:");
	currentItem->setFlags(Qt::ItemIsEnabled);
	ui->annotationProperties->setItem(metaDataNames.size() + 2, 0, currentItem);

    if (currentBox >= 0 && (annotations.size() <= currentBox)) {
        currentBox = -1;
    }

	if(currentBox >= 0) {
		currentItem = new QTableWidgetItem(annotations[currentBox].tag());
		currentItem->setFlags(Qt::ItemIsEnabled);
		ui->annotationProperties->setItem(0, 1, currentItem);

        currentItem = new QTableWidgetItem(QString::number(annotations[currentBox].id()));
        ui->annotationProperties->setItem(1, 1, currentItem);

		for(int i = 0; i < (int)metaDataNames.size(); i++) {
			if(annotations[currentBox].getMetaData(i)) {
				currentItem = new QTableWidgetItem("True");
				currentItem->setFlags(Qt::ItemIsEnabled);
				ui->annotationProperties->setItem(i+2, 1, currentItem);
			} else {
				currentItem = new QTableWidgetItem("False");
				currentItem->setFlags(Qt::ItemIsEnabled);
				ui->annotationProperties->setItem(i+2, 1, currentItem);
			}
		}

		QString statusText;
		if (annotations[currentBox].isFinished()) {
			statusText = QObject::tr("Last frame reached");
		}
		else {
			statusText = QObject::tr("Active");
		}
		currentItem = new QTableWidgetItem(statusText);
		currentItem->setFlags(Qt::ItemIsEnabled);
		ui->annotationProperties->setItem(metaDataNames.size() + 2, 1, currentItem);

		ui->annotationProperties->setEnabled(true);
	} else {
		ui->annotationProperties->setEnabled(false);
	}

    ui->annotationProperties->blockSignals(false);

}

void MainWindow::updateCurrentAnnotationsTable()
{
	ui->currentAnnotationsTable->blockSignals(true);
	ui->currentAnnotationsTable->clearContents();
	ui->currentAnnotationsTable->setRowCount(annotations.size());

	for (auto i = 0; i < annotations.size(); ++i) {
		QTableWidgetItem * id = new QTableWidgetItem(QString("%1").arg(annotations[i].id()));
		QTableWidgetItem * tag = new QTableWidgetItem(annotations[i].tag());
		
		ui->currentAnnotationsTable->setItem(i, 0, id);
		ui->currentAnnotationsTable->setItem(i, 1, tag);
	}

	if (currentBox >= 0 && currentBox < annotations.size()) {
		ui->currentAnnotationsTable->selectRow(currentBox);
	}

	ui->currentAnnotationsTable->blockSignals(false);
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
	// These events must be taken care of whether or not a box is selected:
    //if (e->modifiers() == Qt::ControlModifier && e->key() == Qt::Key_Z) {
    //    undo();
    //    return;
    //}

	switch(e->key()) {
	case Qt::Key_E:
		if(annotations.size() > 0) {
			currentBox++;
			if(currentBox > (int)annotations.size()-1) {
				currentBox = 0;
			}
		}
		break;
	case Qt::Key_Q:
		if(annotations.size() > 0) {
			currentBox--;
			if(currentBox < 0) {
				currentBox = annotations.size()-1;
			}
		}
		break;
	case Qt::Key_Return:
		on_saveBtn_clicked();
		break;
	}

	// Events below here must only be taken care of, if a box is selected.

	if(e->modifiers() == Qt::ShiftModifier) {
		boxAdjustAmount = 10;
		shiftKeyPressed = true;
		qDebug() << "Shift pressed";
	}

	switch(e->key()) {
	/*case Qt::Key_W:
		annotations[currentBox].moveUp(adjustAmount);
        actionHistory.append(currentBox);
		break;
	case Qt::Key_S:
		annotations[currentBox].moveDown(adjustAmount);
        actionHistory.append(currentBox);
		break;
	case Qt::Key_A:
		annotations[currentBox].moveLeft(adjustAmount);
        actionHistory.append(currentBox);
		break;
	case Qt::Key_D:
		annotations[currentBox].moveRight(adjustAmount);
        actionHistory.append(currentBox);
		break;
	case Qt::Key_I:
		annotations[currentBox].shrinkVertical(adjustAmount);
        actionHistory.append(currentBox);
		break;
	case Qt::Key_K:
		annotations[currentBox].growVertical(adjustAmount);
        actionHistory.append(currentBox);
		break;
	case Qt::Key_J:
		annotations[currentBox].shrinkHorizontal(adjustAmount);
        actionHistory.append(currentBox);
		break;
	case Qt::Key_L:
		annotations[currentBox].growHorizontal(adjustAmount);
        actionHistory.append(currentBox);
		break;*/
	case Qt::Key_F2:
		changeTag();
		break;
	case Qt::Key_F5:
		toggleMeta(0);
		break;
	case Qt::Key_F6:
		toggleMeta(1);
		break;
	case Qt::Key_F7:
		toggleMeta(2);
		break;
	case Qt::Key_F8:
		toggleMeta(3);
		break;
	case Qt::Key_F9:
		toggleMeta(4);
		break;
	case Qt::Key_F10:
		toggleMeta(5);
		break;
	case Qt::Key_F11:
		toggleMeta(6);
		break;
	case Qt::Key_F12:
		toggleMeta(7);
		break;
	default:
		//e->ignore();
		break;
	}


	drawAnnotations(currentBox);
	updatePropertiesTable();
}

void MainWindow::keyReleaseEvent(QKeyEvent * e)
{	
	if (e->key() == Qt::Key_Shift) {
		qDebug() << "Shift released";
		boxAdjustAmount = 1;
		shiftKeyPressed = false;
	}

	if (QGuiApplication::queryKeyboardModifiers().testFlag(Qt::AltModifier))
	{
		qDebug() << "Alt is pressed, Key:" << e->key() << "; A=" << Qt::Key_A;

		qDebug() << e->text();
		
		if (e->key() >= Qt::Key_0 && e->key() <= Qt::Key_9)
		{
			int numberPressed = e->key() - Qt::Key_0;
			interpolationStepSizeSpinBox->setValue(numberPressed);
		}
	}
}

bool MainWindow::changeTag()
{
	if(currentBox < 0) {
		return false;
	}

	QString currentTag = annotations[currentBox].tag();

	QSettings settings;
	bool limitToSugggestedTags = settings.value("limitTagsToSuggested", 0).toBool();

	if (limitToSugggestedTags) {
		// If we limit the tags to strictly use the suggested list, use a QInputDialog instead
		// of the AutoCompleteDialog

		QString newTag;

		if (suggestedTags.size() == 1)
		{
			newTag = suggestedTags.first();
		}
		else {
			newTag = QInputDialog::getItem(this, QObject::tr("Change tag"), QObject::tr("New tag"), suggestedTags, suggestedTags.indexOf(currentTag), false);
		}
		

		if (!newTag.isEmpty() && (newTag != currentTag)) {
			QString oldTag = annotations[currentBox].tag();
			annotations[currentBox].setTag(newTag);

			// Search for annotations in adjacent frames which contains the same annotation id, tag
			std::map<int, BoxAnnotation> sameIdTagAnnotations;

			// Go forward
			for (auto i = currentImage + 1; i < numImages; ++i) {
				QStringList existingAnnotations = getExistingAnnotations(maskFilename(i, Image::RGB), annotations[currentBox].id());

				if (existingAnnotations.size() == 1) {
					BoxAnnotation ann = csvToAnnotation(existingAnnotations[0]);

					if (ann.tag() == oldTag) {
						sameIdTagAnnotations.insert(std::pair<int, BoxAnnotation>(i, ann));
						continue;
					}
				}

				// If we have not found any annotations with the same id, tag in frame i, do not look further
				// as we only need the annotations that are temporally connected
				break;
			}

			// Go backward
			for (auto i = currentImage; i >= 0; --i) {
				QStringList existingAnnotations = getExistingAnnotations(maskFilename(i, Image::RGB), annotations[currentBox].id());

				if (existingAnnotations.size() == 1) {
					BoxAnnotation ann = csvToAnnotation(existingAnnotations[0]);

					if (ann.tag() == oldTag) {
						sameIdTagAnnotations.insert(std::pair<int, BoxAnnotation>(i, ann));
						continue;
					}
				}

				// If we have not found any annotations with the same id, tag in frame i, do not look further
				// as we only need the annotations that are temporally connected
				break;
			}

			if (!sameIdTagAnnotations.empty()) {
				// Create a list of adjacent annotations for the user to choose if to change the tag of these annotations
				QString replacedAnnotations;
				int framesShown = 0;
				auto it = sameIdTagAnnotations.begin();

				// Show the first five frames that contains the annotations
				while (it != sameIdTagAnnotations.end()) {
					replacedAnnotations += QObject::tr("Frame %1: Id: %2, tag: %3\n").arg(it->first + 1).arg(it->second.id()).arg(it->second.tag());

					framesShown++;

					if (framesShown > 4) {
						break;
					}
					it++;
				}

				// And the last annotations if appropropiate
				if (sameIdTagAnnotations.size() > 6) {
					replacedAnnotations += QObject::tr("...\n");

					// Show the last frame that contains the annotations
					auto backIt = sameIdTagAnnotations.rbegin();

					if (backIt != sameIdTagAnnotations.rend()) {
						replacedAnnotations += QObject::tr("Frame %1: Id: %2, tag: %3\n").arg(backIt->first + 1).arg(backIt->second.id()).arg(backIt->second.tag());
					}
				}

				QString message = QObject::tr("There are annotations in adjacent frames which share the same id and tag as the current annotation. \n\n"
					"Do you want to change the tag of these annotations, too?\n\nDetails:\n%1").arg(replacedAnnotations);

				int ret = QMessageBox::question(this, QObject::tr("Replace tag in adjacent frames"), message, QMessageBox::Yes, QMessageBox::No);

				if (ret == QMessageBox::Yes) {
					for (auto ann : sameIdTagAnnotations) {
						ann.second.setTag(annotations[currentBox].tag());

						insertAnnotationInList(ann.first, ann.second); // Replace annotations with new tags and overwrite annotations with old tag
					}
				}
			}
		}

	}
	else {
		AutoCompleteDialog d(this, suggestedTags, QString("Annotation name"));
		if (d.exec() == QDialog::Accepted) {
			annotations[currentBox].setTag(d.getWord());
			actionHistory.append(currentBox);
			if (!suggestedTags.contains(d.getWord())) {
				suggestedTags << d.getWord();
			}

			if (noReplace.indexOf(d.getWord()) == -1 && !currentTag.isEmpty()) {
				int ret = QMessageBox::question(this, "Replace all occurrences?",
					QString("Do you wish to replace all instances of %1 with %2 in this track?").arg(currentTag).arg(d.getWord()),
					QMessageBox::Yes | QMessageBox::No,
					QMessageBox::Yes);
				if (ret == QMessageBox::Yes) {
					replaceInTrack.append(currentTag);
					replaceInTrack.append(d.getWord());
				}
				else {
					noReplace.append(d.getWord());
				}
			}
		}
		else {
		}
	}

	updatePropertiesTable();
	updateCurrentAnnotationsTable();

	return true;
}

void MainWindow::toggleMeta(int metaDataId)
{
	if(currentBox < 0) {
		return;
	}

	if(metaDataId >= 0 && metaDataId < annotations[currentBox].getMetaDataSize()) {
		annotations[currentBox].setMetaData(metaDataId, !annotations[currentBox].getMetaData(metaDataId));
        actionHistory.append(currentBox);
	}
}

void MainWindow::toggleIsFinishedState()
{
	if (currentBox >= 0 && currentBox < annotations.size()) {
		annotations[currentBox].setIsFinished(!annotations[currentBox].isFinished());
	}
}

bool MainWindow::checkForGreyscale(const cv::Mat & image)
{
	bool greyscale = true;
	try {

		for (auto y = 0; y < image.rows; ++y) {
			for (auto x = 0; x < image.cols; ++x) {

				if (uchar(image.at<cv::Vec3b>(y, x)[0]) !=
					uchar(image.at<cv::Vec3b>(y, x)[1]) && 
					uchar(image.at<cv::Vec3b>(y, x)[0]) != 
					uchar(image.at<cv::Vec3b>(y, x)[2])) {

					qDebug() << image.at<cv::Vec3b>(y, x)[0] << " " << image.at<cv::Vec3b>(y, x)[1] << " " << image.at<cv::Vec3b>(y, x)[2];
					greyscale = false;
					return false;
				}
			}
		}
	}
	catch (cv::Exception e) {
		qDebug() << e.what();
	}

	return greyscale;
}

void MainWindow::saveAnnotations() 
{
    if (currentImage >= 0) {
        QHash<int, QString> maskFiles;

        // Make sure to get tags for any untagged annotations:
        for (int i = 0; i < (int)annotations.size(); i++) {
            if (annotations[i].tag().length() < 1) {
                currentBox = i;
                drawAnnotations(currentBox);
                updatePropertiesTable();
                if (!changeTag()) {
                    return;
                }
            }
        }

		// Check if there are instances of an TemporalInterpolator available. If this is the case, we should use
		// the current annotations to interpolate from previous annotations
		if (!annotationInterpolators.empty()) {
			QList<BoxAnnotation> newAnnotations = annotations;

			int currentFrameNumber = currentImage;

			for (auto interpolator : annotationInterpolators) {
				int previousFrameNumber = interpolator->getStartFrameNumber();

				// Fetch the existing annotations between the current frame number and the
				// frame number for which the interpolator was instantiated
				int bigger = currentFrameNumber > previousFrameNumber ? currentFrameNumber : previousFrameNumber;
				int smaller = currentFrameNumber > previousFrameNumber ? previousFrameNumber : currentFrameNumber;

				std::map<int, QList<BoxAnnotation> > existingAnnotations;

				for (auto i = smaller + 1; i < bigger; ++i) {
					QString fileName = maskFilename(i, Image::RGB);

					if (fileName.length() > 0) {

						QStringList existingLines = outFileLines.filter(fileName);
						for (auto j = 0; j < existingLines.size(); j++) {
							existingAnnotations[i].append(csvToAnnotation(existingLines[j]));
						}
					}
				}

				int res = interpolator->interpolate(currentFrameNumber, annotations, existingAnnotations);

				if (res == 0) {
					std::map<int, QList<BoxAnnotation> > interpolatedAnnotations = interpolator->getInterpolatedAnnotations();

					for (auto annotationsPerFrame : interpolatedAnnotations) {
						for (auto singleAnnotation : annotationsPerFrame.second) {
							insertAnnotationInList(annotationsPerFrame.first, singleAnnotation);
						}
					}
				}
			}

			// Dispose the annotation interpolators
			annotationInterpolators.clear();
		}
        
        QString fileName = maskFilename(currentImage, Image::RGB);
        
        // Generate a line in the csv file for each annotation:
        for (auto i = 0; i < annotations.size(); i++) {
			insertAnnotationInList(currentImage, annotations[i]);
        }

        saveCSVFile();
    }
}

void MainWindow::on_retainBoxes_clicked()
{	
	if (functionActions[Function::retainPreviousAnnotation]->isChecked() ||
		functionActions[Function::retainNextAnnotation]->isChecked()) {
		functionActions[Function::interpolateBetweenAnnotations]->setEnabled(true);
	}
	else {
		functionActions[Function::interpolateBetweenAnnotations]->setEnabled(false);
	}
	
}

void MainWindow::on_saveBtn_clicked()
{
	// Save annotation and load next image
    loadNextImage();
}

void MainWindow::on_actionExport_annotations_triggered()
{
	if (!images.empty() && !images[Image::RGB].empty())
	{
		AnnotationExporter annExporter(this, outFileLines, images[Image::RGB].cols, images[Image::RGB].rows,
			baseDir.path());
		annExporter.exec();
	}
}

void MainWindow::on_actionClear_all_triggered()
{
	currentBox = -1;
	annotations.clear();
	drawAnnotations();
}

void MainWindow::on_actionEdit_meta_data_fields_triggered()
{
	MetaDataDialog d(this, metaDataNames, 
        QObject::tr("Edit meta data fields"),
        QObject::tr("Enter the meta data names here, one per line."));
	if(d.exec()) {
		metaDataNames = d.getNames();
	}
	ui->annotationProperties->setRowCount(3+metaDataNames.size());
}

void MainWindow::on_actionEdit_suggested_tags_triggered()
{
	MetaDataDialog d(this, suggestedTags, 
		QObject::tr("Edit suggested tags"),
		QObject::tr("Enter the suggested tag names here, one per line"));
	if (d.exec()) {
		suggestedTags = d.getNames();
	}

	// Write the suggested tags to disk
	QFile tagFile("suggestedTags.txt");
	if (tagFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QTextStream tagStream(&tagFile);

		for (auto line : suggestedTags) {
			tagStream << line << '\n';
		}
		
		tagStream.flush();
	}

	tagFile.close();
}



void MainWindow::on_actionLoad_configuration_triggered()
{
    QSettings settings;

    QString dir = settings.value("baseFolder").toString();
    QString filename = QFileDialog::getOpenFileName(this, tr("Load configuration"), dir, "*.yml");

    if (filename.isEmpty()) {
        return;
    }

    FileStorage fs(filename.toStdString(), FileStorage::READ);

    if (fs.isOpened()) {
        settings.setValue("baseFolder", QFileInfo(filename).absolutePath());

        std::string directory;
        fs["directory"] >> directory;

        std::string calibVars;
        fs["calibVars"] >> calibVars;

        std::string mask;
        fs["mask"] >> mask;

        openConfiguration(QString::fromStdString(directory), QString::fromStdString(calibVars), QString::fromStdString(mask));

        fs.release();
    }
}

void MainWindow::on_actionSave_configuration_triggered()
{
    QSettings settings;

    QString dir = settings.value("baseFolder").toString();

    QString filename = QFileDialog::getSaveFileName(this, "Save configuration", dir, "*.yml");

    if (!filename.isEmpty()) {
        FileStorage fs(filename.toStdString(), FileStorage::WRITE);

        fs << "directory" << baseDir.absolutePath().toStdString();
        fs << "calibVars" << calibPath.toStdString();
        fs << "mask" << maskPath.toStdString();

        fs.release();
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	if(!fileLoaded || annotations.size() == 0) {
		event->accept();
		return;
	}

	int ret = QMessageBox::question(this, "Save current frame?",
										   "Do you wish to save any annotations on the\ncurrent frame before quitting?",
											QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
											QMessageBox::Yes);
	switch(ret) {
	case QMessageBox::Yes:
		on_saveBtn_clicked();
		event->accept();
		break;
	case QMessageBox::No:
		emit quitApp();
		event->accept();
		break;
	default:
		event->ignore();
	}
}

void MainWindow::undo()
{
    if (!actionHistory.empty()) {
        int action = actionHistory.takeLast();

        switch (action)
        {
        case -2:
        {
            // We have added an annotation at this step. Delete it
            if (!annotations.isEmpty()) {
                QList<int> id = { annotations.last().id() };
                deleteAnnotations(id, false);
                drawAnnotations();
            }
            break;
        }
        case -1:
        {
            // We have deleted an annotation at this step. Add it
            if (!deletedAnnotations.isEmpty()) {
                annotations.append(deletedAnnotations.takeLast());
                drawAnnotations(annotations.size() - 1);
            }
            break;
        }
        default:
        {
            // For all other actions, we have recorded the undo history in the annotation itself
            if (annotations.size() > action) {
                annotations[action].undo();
                drawAnnotations(action);
            }
            break;
        }

        }
    }
}



QStringList MainWindow::getExistingAnnotations(QString imageFileName, int id)
{
	return outFileLines.filter(QRegExp(QString("%1;%2").arg(imageFileName).arg(QString("%1").arg(id) + "(;[^;]*){1,9};")));;
}

QString MainWindow::getModalityName(int modality)
{
    QString outputModality;

    switch (modality)
    {
    case Image::RGB:
    {
        outputModality = "RGB";
        break;
    }
    case Image::Thermal:
    {
        outputModality = "Thermal";
        break;
    }
    case Image::Depth:
    {
        outputModality = "Depth";
        break;
    }
    case Image::Mask:
    {
        outputModality = "Mask";
        break;
    }
    default:
        outputModality = "ErrorModality";

    }

    return outputModality;

}

int MainWindow::getModalityIndex(int modality)
{
    int index = 0;

    for (int i = Image::RGB; i <= Image::Mask; ++i)
    {
        if (i == modality)
        {
            return index;
        }

        if (enabledModalities[i])
        {
            ++index;
        }
    }

    // Not supported - return -1
    return -1;
}

void MainWindow::on_actionAbout_triggered()
{
	QString message = tr("Created by Andreas M%1gelmose and Chris Holmberg Bahnsen, Aalborg University.\n\n"
		"Please visit https://bitbucket.org/aauvap/bounding-box-annotator for the newest version of the program."

		"\n\n-----------------------------------------\n"
		"This program is licensed under the MIT License:\n"
		"Copyright(c) 2018 Aalborg University\n\n"
		"Permission is hereby granted, free of charge, to any person obtaining a copy "
		"of this software and associated documentation files(the \"Software\"), to deal "
		"in the Software without restriction, including without limitation the rights "
		"to use, copy, modify, merge, publish, distribute, sublicense, and / or sell "
		"copies of the Software, and to permit persons to whom the Software is "
		"furnished to do so, subject to the following conditions : \n\n"
		"The above copyright notice and this permission notice shall be included in all "
		"copies or substantial portions of the Software.\n\n"
		"THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR "
		"IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, "
		"FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE "
		"AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER "
		"LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, "
		"OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
		"\n\n-----------------------------------------\n"
		"\Some icons by Yusuke Kamiyamane. All rights reserved. Icons licensed under a Creative Commons Attribution 3.0 License.\n"
		"\n\n-----------------------------------------\n"
		"Created using the QT Framework under the LGPL License. For more information about the QT framework regarding this program, please refer to cb@create.aau.dk").arg(QChar(0x00F8));

	QMessageBox::about(this, "AAU VAP Bounding Box Annotator", message);
}

void MainWindow::playPausePlayback()
{
	if (playbackEnabled)
	{
		playbackEnabled = false;
		functionActions[Function::playPausePlayback]->setIcon(QIcon(":/icons/control.png"));
	}
	else {
		functionActions[Function::playPausePlayback]->setIcon(QIcon(":/icons/control-pause.png"));
		playbackEnabled = true;
		continuousPlayback();
	}
}

void MainWindow::continuousPlayback()
{
	while(playbackEnabled)
	{
		QApplication::processEvents();
		nextImage();
	}
}

void MainWindow::deleteCurrentAnnotation()
{
	if (annotations.size() > 0 && currentBox >= 0 && currentBox < annotations.size()) {
		QList<int> deleteIds;
		deleteIds.append(annotations[currentBox].id());
		deleteAnnotations(deleteIds);

		currentBox--;
		if (currentBox < 0) {
			currentBox = 0;
		}
		if (annotations.size() == 0) {
			currentBox = -1;
		}
	}

	drawAnnotations(currentBox);
	updatePropertiesTable();
	updateCurrentAnnotationsTable();
}

void MainWindow::deleteAllFutureAnnotations()
{
	if (annotations.size() > 0 && currentBox >= 0 && currentBox < annotations.size()) {
		// Search for annotations in adjacent frames which contains the same annotation id, tag
		std::map<int, BoxAnnotation> sameIdTagAnnotations;
		std::map<int, QString> sameIdTagAnnotationsString;

		// Go forward
		QProgressDialog progress(QObject::tr("Searching for annotations..."), QString(), 0, sameIdTagAnnotations.size(),
			this);

		findFutureAnnotationsWithSameIdTag(sameIdTagAnnotationsString, sameIdTagAnnotations);

		// Inform the user that we are about to delete (potentially) a lot of annotations
		QString message = QObject::tr("The following %1 annotations are about to be deleted:\n")
			.arg(sameIdTagAnnotations.size()+1);
		message += QObject::tr("Current frame: id: %1, %2\n")
			.arg(annotations[currentBox].id())
			.arg(annotations[currentBox].tag());

		if (!sameIdTagAnnotations.empty()) {
			int framesShown = 0;
			auto it = sameIdTagAnnotations.begin();

			// Show the first five frames that contains the annotations
			while (it != sameIdTagAnnotations.end()) {
				message += QObject::tr("Frame %1: Id: %2, tag: %3\n").arg(it->first + 1).arg(it->second.id()).arg(it->second.tag());

				framesShown++;

				if (framesShown > 4) {
					break;
				}
				it++;
			}

			// And the last annotations if appropropiate
			if (sameIdTagAnnotations.size() > 6) {
				message += QObject::tr("...\n");
			}

			// Show the last frame that contains the annotations
			auto backIt = sameIdTagAnnotations.rbegin();

			if (sameIdTagAnnotations.size() > 5)
			{
				if (backIt != sameIdTagAnnotations.rend()) {
					message += QObject::tr("Frame %1: Id: %2, tag: %3\n").arg(backIt->first + 1).arg(backIt->second.id()).arg(backIt->second.tag());
				}
			}
		}
		else {
			// We only have one annotation. Delete it quietly
			deleteCurrentAnnotation();
			return;
		}

		message += "\nThis action cannot be undone. Are you sure?";

		int answer = QMessageBox::question(this, QObject::tr("Delete %1 annotations").arg(sameIdTagAnnotations.size() + 1),
			message, QMessageBox::Yes, QMessageBox::No);

		if (answer == QMessageBox::Yes) {
			deleteCurrentAnnotation();
			QProgressDialog progress(QObject::tr("Deleting annotations..."), QObject::tr("Abort"), 0, sameIdTagAnnotations.size(),
				this);

			for (auto& annotation : sameIdTagAnnotationsString) {
				progress.setValue(annotation.first - sameIdTagAnnotations.begin()->first);

				if (progress.wasCanceled()) {
					break;
				}
				
				int idx = outFileLines.indexOf(annotation.second);
				outFileLines.removeAt(idx);
			}
			progress.setValue(sameIdTagAnnotations.size());
		}
	}

	drawAnnotations(currentBox);
	updatePropertiesTable();
	updateCurrentAnnotationsTable();
}

void MainWindow::deleteAnnotationsWithinRange()
{
	
}

void MainWindow::mergeAnnotations()
{
	// Step 1: Get list of other annotations than the currently selected
	auto& motherAnnotation = annotations[currentBox];
	QList<BoxAnnotation> otherAnnotations;
	QStringList otherAnnotationIds;

	for (auto& annotation : annotations)
	{
		if (annotation.id() != motherAnnotation.id())
		{
			otherAnnotations.push_back(annotation);
			otherAnnotationIds.push_back(QString("Id: %1, tag: %2").arg(annotation.id()).arg(annotation.tag()));
		}
	}

	// Step 2: Prompt the user to select the annotation to merge
	bool ok;
	QString selectedItem = QInputDialog::getItem(this, QObject::tr("Merge annotations"), QObject::tr("Select other annotation to merge with ID %1. \n"
		"The bounding box of this annotation will be merged into the selected annotation.\n"
		"After the annotations are merged, the chosen annotation ID will be deleted from the current and future frames.").arg(motherAnnotation.id()), 
		otherAnnotationIds, 0, false, &ok);

	if (!ok)
	{
		return;
	}

	int selectedAnnotationId = selectedItem.split(" ").last().toInt();

	// Find the index in the annotation list where the id belongs
	int idInList = 0;
	for (auto i = 0; i < annotations.size(); ++i)
	{
		if (annotations[i].id() == selectedAnnotationId)
		{
			idInList = i;
		}
	}

	drawAnnotations(currentBox);
	updatePropertiesTable();
	updateCurrentAnnotationsTable();
	
	// Step 3: Find adjacent annotations in the next frames
	std::map<int, BoxAnnotation> sameIdTagAnnotationsForMother, sameIdTagAnnotationsForChild;
	findFutureAnnotationsWithSameIdTag(sameIdTagAnnotationsForMother, currentBox);
	findFutureAnnotationsWithSameIdTag(sameIdTagAnnotationsForChild, idInList);
		
	// Step 4: Actually merge the bounding boxes
	for (auto& kv : sameIdTagAnnotationsForMother)
	{
		if (sameIdTagAnnotationsForChild.find(kv.first) != sameIdTagAnnotationsForChild.end())
		{
			// Mother and child exist at same frame. Combine the child into the mother
			QString motherLine = annotationToCsv(kv.first, kv.second);
			int motherIndex = outFileLines.indexOf(motherLine);

			cv::Point motherA = kv.second.a();
			cv::Point childA = sameIdTagAnnotationsForChild[kv.first].a();
			cv::Point combinedA = cv::Point(cv::min(motherA.x, childA.x), cv::min(motherA.y, childA.y));

			cv::Point motherB = kv.second.b();
			cv::Point childB = sameIdTagAnnotationsForChild[kv.first].b();
			cv::Point combinedB = cv::Point(cv::max(motherB.x, childB.x), cv::max(motherB.y, childB.y));
			
			kv.second.setA(combinedA);
			kv.second.setB(combinedB);

			if (motherIndex >= 0 && motherIndex < outFileLines.size())
			{
				// Update the combined item
				outFileLines[motherIndex] = annotationToCsv(kv.first, kv.second); // Replace with new id
			}

			// Delete the child item
			QString childLine = annotationToCsv(kv.first, sameIdTagAnnotationsForChild[kv.first]);
			int childIndex = outFileLines.indexOf(childLine);

			// Remove the key from the child map/dictionary
			sameIdTagAnnotationsForChild.erase(kv.first);

			if (childIndex >= 0 && childIndex < outFileLines.size())
			{
				// Step 5: Delete the leftover annotation
				outFileLines.removeAt(childIndex);
			}
		}
	}

	// Step 6: The remaining entries in sameIdTagAnnotationsForChild represents the future annotations
	// where there was no overlap between the currently selected id (mother) and the other annotation id (child).
	// However, as we want to truly merge the two annotations, we must convert these child annotations
	// to share the mother id
	for (auto& kv : sameIdTagAnnotationsForChild)
	{
		// Find the corresponding line index in the outFileLines
		QString childLine = annotationToCsv(kv.first, sameIdTagAnnotationsForChild[kv.first]);
		int childIndex = outFileLines.indexOf(childLine);

		// Change the ID
		QString newIdChild = changeAnnotationId(childLine, annotations[currentBox].id());

		if (childIndex >= 0 && childIndex < outFileLines.size())
		{
			outFileLines[childIndex] = newIdChild;
		}
	}
	
	// Step 7: Merge the annotations in the current frame
	cv::Point motherA = annotations[currentBox].a();
	cv::Point childA = annotations[idInList].a();
	cv::Point combinedA = cv::Point(cv::min(motherA.x, childA.x), cv::min(motherA.y, childA.y));

	cv::Point motherB = annotations[currentBox].b();
	cv::Point childB = annotations[idInList].b();
	cv::Point combinedB = cv::Point(cv::max(motherB.x, childB.x), cv::max(motherB.y, childB.y));

	annotations[currentBox].setA(combinedA);
	annotations[currentBox].setB(combinedB);

	// Delete the child annotation
	if (annotations.size() > 0 && currentBox >= 0 && currentBox < annotations.size()) {
		QList<int> deleteIds;
		deleteIds.append(annotations[idInList].id());
		deleteAnnotations(deleteIds);

		if (idInList < currentBox) {
			currentBox--;
		}
	}
	
	drawAnnotations(currentBox);
	updatePropertiesTable();
	updateCurrentAnnotationsTable();
}

void MainWindow::findFutureAnnotationsWithSameIdTag(std::map<int, BoxAnnotation>& sameIdTagAnnotations, int boxId)
{
	std::map<int, QString> samedIdTagAnnotationsString;

	findFutureAnnotationsWithSameIdTag(samedIdTagAnnotationsString, sameIdTagAnnotations, boxId);
}

void MainWindow::findFutureAnnotationsWithSameIdTag(std::map<int, QString>& samedIdTagAnnotationsString, int boxId)
{
	std::map<int, BoxAnnotation> sameIdTagAnnotationsBox;
	findFutureAnnotationsWithSameIdTag(samedIdTagAnnotationsString, sameIdTagAnnotationsBox);
}


void MainWindow::findFutureAnnotationsWithSameIdTag(std::map<int, QString>& samedIdTagAnnotationsString, std::map<int, BoxAnnotation>& sameIdTagAnnotations, int boxId)
{
	if (boxId == -1)
	{
		boxId = currentBox;
	}

	int currentId = annotations[boxId].id();
	QString currentTag = annotations[boxId].tag();

	QRegExp rx(QString("\\w+;%1;%2").arg(QString::number(currentId)).arg(currentTag) + "(;[^;]*){1,9};");

	if (!fileLists[Image::RGB].empty())
	{
		// Get the base directory of the image frames
		QString baseDir = QFileInfo(fileLists[Image::RGB].first()).absolutePath();

		for (auto& line : outFileLines)
		{
			if (rx.indexIn(line) != -1)
			{
				// We found an entry with the same ID and tag. Now check if it is in the future frames
				QStringList entries = line.split(";");

				if (entries.size() > 7)
				{
					QString search = QDir(baseDir).absoluteFilePath(entries[0]);
					QRegularExpression extractFilename(search);
					int idx = fileLists[Image::RGB].indexOf(extractFilename);

					if (idx >= 0 && idx > currentImage)
					{
						BoxAnnotation ann = csvToAnnotation(line);
						sameIdTagAnnotations.insert(std::pair<int, BoxAnnotation>(idx, ann));
						samedIdTagAnnotationsString.insert(std::pair<int, QString>(idx, line));
					}
				}
			}
		}
	}

	qDebug() << "Found " << sameIdTagAnnotations.size() << " future annotations with ID " << currentId << " and tag: " << currentTag;

	//// Old, O(n^2) approach
	//for (auto i = currentImage + 1; i < numImages; ++i) {


	//	QStringList existingAnnotations = getExistingAnnotations(maskFilename(i, Image::RGB), annotations[boxId].id());

	//	if (existingAnnotations.size() == 1) {
	//		BoxAnnotation ann = csvToAnnotation(existingAnnotations[0]);

	//		if (ann.tag() == currentTag) {
	//			sameIdTagAnnotations.insert(std::pair<int, BoxAnnotation>(i, ann));
	//			samedIdTagAnnotationsString.insert(std::pair<int, QString>(i, existingAnnotations[0]));
	//		}
	//	}

	//	//// If we have not found any annotations with the same id, tag in frame i, do not look further
	//	//// as we only need the annotations that are temporally connected
	//	//break;
	//}
}
