#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QFile>
#include <QFileDialog>
#include <QInputDialog>
#include <QDir>
#include <QLabel>
#include <QMainWindow>
#include <QMdiSubWindow>
#include <QMessageBox>
#include <QStringList>
#include <QTextStream>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpinBox>
#include <QProgressDialog>

#include <opencv2/core/core.hpp>
#include "common/openCVCapture/opencvcapture.h"
#include "common/openCVViewer/opencvviewer.h"
#include "common/annotatedTrack/annotatedtrack.h"
#include "common/autoCompleteDialog/autocompletedialog.h"
#include "common/registrator/registrator.h"
#include "annotationpreviewer.h"
#include "boxannotation.h"
#include "metadatadialog.h"
#include "settingsdialog.h"
#include "temporalinterpolator.h"
#include "annotationExporter.h"

struct Function {
	/** Placeholder name - in reality the struct (Function) is used directly */
	enum e {
		save, ///< Save all annotations in this image.
		undo, ///< Undo one change.
		deleteAnnotations, ///< Delete the selected annotation(s).
		deleteAllFutureAnnotations, ///< Delete the selected annotion, and all existing future instances
		//deleteAnnotationsWithinRange, ///< Delete the selected annotation within the user-specified frames
		mergeAnnotations, ///< Merge the selected annotations
		moveBBUp, ///< Move bounding box in the negative y-direction
		moveBBDown, ///< Expand bounding box in the positive y-direction
		moveBBLeft, ///< Move bounding box in the negative x-direction
		moveBBRight, ///< Move mask in the positive x-direction
		expandBBVertical, ///< Expand BB in the vertical direction
		shrinkBBVertical, ///< Shrink BB in the vertical direction
		expandBBHorizontal, ///< Expand BB in the horizontal direction
		shrinkBBHorizontal, ///< Shrink BB in the horizontal direction
		zoomIn, ///< Zoom all views in.
		zoomInExtra, ///< Zoom all views in (extra shortcut to enable numpad +).
		zoomOut, ///< Zoom all views out.
		zoomOutExtra, ///< Zoom all views out (extra shortcut to enable numpad -).
		zoomFit, ///< Fit image in all windows.
		zoomActual, ///< Zoom to actual pixel size in all windows.
		previousImage, ///< Go to previous image.
		previousNthImage, ///< Go to the nth previous image
		nextImage, ///< Go to next image.
		nextNthImage, ///< Go to the nth next image
		playPausePlayback, ///< Play and pause playback
		jumpToImage, ///< Go to a specific image, given in an input box in the interface.
		retainPreviousAnnotation, ///< Retain annotations when opening previous frame
		retainNextAnnotation, ///< Retain annotations when opening next frame
		interpolateBetweenAnnotations, ///< Interpolate between frames
		interpolationFrameStep, /// Step size, in frames, used when interpolating annotations
		showDontCareMask, ///< Show don't care mask
		displayTag, ///< Display annotation tag on top of bounding box
	};
};

/**
Identifiers for the four images in the workspace.

The program handles 3 modalities and a mask. These identifiers are used as indexes in the data structures which hold the images, imageviewers, etc.
*/
struct Image { enum e { RGB, Depth, Thermal, Mask }; };

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
	bool eventFilter(QObject *obj, QEvent *event);

private:
	void loadNextImage();

	cv::Mat readImageEvenIfNonAscii(const QString& imagePath, const int imreadFlags);
	
	void drawAnnotations(int highLight = -1, cv::Point boxInProgressPointB = cv::Point(-1,-1));
    void loadImage(int index, bool saveChanges = true, bool showAnnotations = true, bool isNeighbour = false);
    void openConfiguration(QString dir = QString(), QString calibPath = QString(), QString maskPath = QString());

	int inAnyBox(cv::Point position);
    bool idInList(int id, QList<BoxAnnotation> list);
	void keyPressEvent(QKeyEvent *e);
	void keyReleaseEvent(QKeyEvent *e);
	void updatePropertiesTable();
	void updateCurrentAnnotationsTable();
	bool changeTag();
	void toggleMeta(int metaDataId);
	void toggleIsFinishedState();

	bool checkForGreyscale(const cv::Mat& image);

	void closeEvent(QCloseEvent *event);
    QString getModalityName(int modality);
    int getModalityIndex(int modality);
    void initModalityWindows();
    void saveCSVFile();

    BoxAnnotation csvToAnnotation(const QString& csv);
	QString annotationToCsv(int frameNumber, const BoxAnnotation &annotation);
	QString changeAnnotationId(const QString& csv, int newId);

	void insertAnnotationInList(int frameNumber, const BoxAnnotation &annotation);
	
	int getNewAnnotationId();
    QString maskFilename(int index, int modality);
    void deleteAnnotations(QList<int> deleteIds, bool markInHistory = true);
    void deleteById(int id);
    

	void zoom(int direction = 1, float magnitude = 0.25);
	void updateControlButtons();

	void manipulateCurrentBox(int xShift,
		int yShift,
		int verticalChange,
		int horisontalChange);


	QStringList getExistingAnnotations(QString imageFileName, int id);

	bool shiftKeyPressed;

	Ui::MainWindow *ui;
    QList<OpenCVViewer*> viewers;
    QList<QMdiSubWindow*> workWindows;
    QList<QScrollArea*> scrollAreas;
    QList<cv::Mat> images;
    QList<cv::Mat> displayImages;

	QFile trackCollectionFile;
	QDir baseDirectory, saveDirectory;

	QHash<int, QAction*> functionActions;
	
	int frameDistance;
	bool drawingInitiated, allowAnnotations;
    QList<int> actionHistory;
    QList<BoxAnnotation> deletedAnnotations;

	QList<BoxAnnotation> annotations;
	int currentBox;
	cv::Mat cleanFrame;
	QStringList suggestedTags;
	QMap<int, bool> globalIdList;
	QStringList metaDataNames;
	QFile outFile;
	bool fileLoaded;
	QString currentInputLine;
    QStringList outFileLines;
	QStringList replaceInTrack, noReplace;

    QDir baseDir;
    QString calibPath, maskPath;
    QHash<int, bool> enabledModalities;

    Registrator registrator;
    int currentImage;
    int numImages;
    QHash<int, QStringList> fileLists;
    QLabel* fileNumber;
    cv::Mat dontCareMaskRgb, dontCareMaskThermal;
	int boxAdjustAmount;

	QList<std::shared_ptr<TemporalInterpolator> > annotationInterpolators;
	AnnotationPreviewer annotationPreviewer;

	// UI Buttons
	QSpinBox* imageSelectorSpinBox;
	QSpinBox* interpolationStepSizeSpinBox;

    // Settings
    QHash<int, QString> filePatterns;
    QString csvPath;
    QColor overlayColor;
    float overlayOpacity;

	bool playbackEnabled;

private slots:
    void imageMouseHandler(int image, QMouseEvent* event);
    void RGBMouseHandler(QMouseEvent* event);
    void ThermalMouseHandler(QMouseEvent* event);
	
	void scrollHandler(QWheelEvent* event);
	void showDontCareMask();

	void on_actionOpen_folder_triggered();

	void on_actionUse_Dark_Mode_triggered();

	void on_saveBtn_clicked();

	void on_retainBoxes_clicked();

	void undo();

	void on_actionExport_annotations_triggered();
	void on_actionClear_all_triggered();
	void on_actionEdit_meta_data_fields_triggered();
	void on_actionEdit_suggested_tags_triggered();
	void on_annotationProperties_cellDoubleClicked(int row, int column);
    void on_annotationProperties_cellChanged(int row, int column);
	void on_currentAnnotationsTable_itemSelectionChanged();
	void on_nonSelectedAnnotationsTransparencySlider_valueChanged(int value);
	void on_actionReset_layout_triggered();

    void on_action_Settings_triggered();
    void on_actionLoad_configuration_triggered();
    void on_actionSave_configuration_triggered();
	void on_actionAbout_triggered();

	void playPausePlayback();
	void continuousPlayback();

	void saveAnnotations();

	void deleteCurrentAnnotation();
	void deleteAllFutureAnnotations();
	void deleteAnnotationsWithinRange();
	void mergeAnnotations();

	void findFutureAnnotationsWithSameIdTag(std::map<int, BoxAnnotation>& sameIdTagAnnotations, int boxId = -1);
	void findFutureAnnotationsWithSameIdTag(std::map<int, QString>& samedIdTagAnnotationsString, int boxId = -1);
	void findFutureAnnotationsWithSameIdTag(std::map<int, QString>& samedIdTagAnnotationsString, std::map<int, BoxAnnotation>& sameIdTagAnnotations, int boxId = -1);

	void zoomIn();
	void zoomOut();
	void zoomFit();
	void zoomActual();

	void moveBBUp();
	void moveBBDown();
	void moveBBLeft();
	void moveBBRight();
	void expandBBVertical();
	void shrinkBBVertical();
	void expandBBHorizontal();
	void shrinkBBHorizontal();

	void nextImage();
	void previousImage();
	void jumpToImage();

	void readSettings(QString prefix);
	void writeSettings(QString prefix);

signals:
	void quitApp();
};

#endif // MAINWINDOW_H
