Bounding Box Annotator is a tool for bounding-box annotation of objects in up to two different views. Annotations are stored in the coordinates of the first view and mapped to the second view by a homography.

The annotator is built in C++ with QT and OpenCV and may be compiled under Windows, macOS, and Linux. Pre-compiled binaries are available for Windows in the [downloads section](https://bitbucket.org/aauvap/bounding-box-annotator/downloads). The binaries may not always reflect the latest features and bug-fixes, however. 

The annotator is free to use under the MIT License. 

For pixel-level annotations, please visit our [Multimodal Pixel Annotator](https://bitbucket.org/aauvap/multimodal-pixel-annotator/).

## How to use ##

#### Getting started ####

1. [Download the application binaries](https://bitbucket.org/aauvap/bounding-box-annotator/downloads) for your platform or [compile the application](https://bitbucket.org/aauvap/bounding-box-annotator/overview#markdown-header-compilation-and-set-up).
2. Download the [sample annotations](https://bitbucket.org/aauvap/bounding-box-annotator/downloads/sampleAnnotations.zip)
3. Launch Bounding Box Annotator
4. In the menu, go to 'Annotations -> Settings'
     - Make sure that 'Enable Thermal' is checked.
     - In 'File patterns', make sure that these are configured like this:
          * RGB images      | *-cam1.png
          * Thermal images  | *-cam2.png
     - In 'Mask', check 'Use don't care mask'
     - Check 'Limit annotation tags to suggested list'
5. In the menu, go to 'Annotations -> Edit suggested tags', and write the allowed tags. When 'Limit annotation tags to suggested list' is checked, only these tags are allowed as 'categories' for the bounding boxes. In this example, make sure that the following tags are listed:
    *    Pedestrian
    *    Cyclist
    *    Moped
    *    Motorcycle
    *    Car
    *    Van
    *    Lorry
    *    Bus
    *    Other vehicle
5. In 'Directory', click 'Open' and open the sampleAnnotations folder
    - Select 'calibVars.yml' as the calibration file
    - Select 'mask.png' as the don't care mask
6. Read the [Introduction guide](https://bitbucket.org/aauvap/bounding-box-annotator/wiki/Introduction%20guide.md) on the wiki to explore additional aspects of the program.
7. Feel free to explore!

#### Configuring the program ####

1. Make sure that you have enabled the right modalities in 'File -> Settings'. If you have imagery of the same scene from two different views, you may use the thermal modality as the second view. 
2. We search for corresponding image files using [QRegularExpressions](http://doc.qt.io/qt-5/qregularexpression.html). Adjust the search paths to fit your project in 'Annotations -> Settings -> File patterns'
3. A bounding box has a id and a tag. If 'Limit annotation tags to suggested list' is selected in the settings, the possible tags are restricted to a pre-defined list. Otherwise, the tag may be chosen freely.
4. Each bounding box may be accompanied by meta data. Before opening an annotation folder for the first time, set the meta data tags in 'Annotation -> Edit meta data fields...'

### Keyboard shortcuts - see [wiki](https://bitbucket.org/aauvap/bounding-box-annotator/wiki/Introduction%20guide.md#markdown-header-the-taskbar) ###


### Compilation and set-up - see [wiki](https://bitbucket.org/aauvap/bounding-box-annotator/wiki/Compilation%20and%20set-up.md#!compilation-and-set-up) ##


### Contribution guidelines ###

Please test the tool and see if it fits your purpose. If not, open an [issue](https://bitbucket.org/aauvap/bounding-box-annotator/issues?status=new&status=open).



### Who do I talk to? ###
* Chris H. Bahnsen at cb@create.aau.dk