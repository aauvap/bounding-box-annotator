#ifndef ANNOTATIONEXPORTER
#define ANNOTATIONEXPORTER

#include <QApplication>
#include <QComboBox>
#include <QDateEdit>
#include <QDialog>
#include <QDirIterator>
#include <QFileInfo>
#include <QFormLayout>
#include <QGroupBox>
#include <QHash>
#include <QLabel>
#include <QLayout>
#include <QMessageBox>
#include <QPushButton>
#include <QProgressBar>
#include <QSet>
#include <QSpinBox>
#include <QToolButton>
#include <QTextStream>
#include <QTimer>
#include <QTimeEdit>
#include <QThread>

class AnnotationExporter : public QDialog
{
	Q_OBJECT;

public:
	AnnotationExporter(QWidget* parent, const QStringList& annotationList, 
		int imageWidth, int imageHeight, const QString& annotationDir);

public slots:
	void startConversionButtonClicked();

private:
	void forceCoordinatesWithinImageFrame(double& xULC, double& yULC, double& xLRC, double& yLRC);

	QComboBox* fileFormatComboBox;
	QComboBox* objectCategoriesComboBox;
	QPushButton* startConversionButton;
	QProgressBar* progressBar;
	QLabel* progressLabel;

	QList<QString> fileFormats;
	QHash<QString, QString> objectCategories;
	QString annotationDir;
	
	QStringList annotationList;
	int imageWidth;
	int imageHeight;
};



#endif // !ANNOTATIONEXPORTER
