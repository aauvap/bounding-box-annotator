#ifndef METADATADIALOG_H
#define METADATADIALOG_H

#include <QDialog>
#include <QStringList>
#include <QMessageBox>

namespace Ui {
	class MetaDataDialog;
}

class MetaDataDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MetaDataDialog(QWidget *parent, QStringList &wordList, const QString &title, const QString &label);
	~MetaDataDialog();

	void accept();
	QStringList getNames();

private:
	Ui::MetaDataDialog *ui;
	QStringList metaDataNames;
	int maxNames;
};

#endif // METADATADIALOG_H
