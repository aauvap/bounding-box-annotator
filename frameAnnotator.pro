#-------------------------------------------------
#
# Project created by QtCreator 2011-12-16T14:13:33
#
#-------------------------------------------------

QT       += core gui

TARGET = frameAnnotator
TEMPLATE = app
CONFIG   += app_bundle\
            c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    annotationpreviewer.cpp \
    boxannotation.cpp \
    metadatadialog.cpp \
    settingsdialog.cpp \
    temporalinterpolator.cpp \
    common/registrator/registrator.cpp \
    annotationExporter.cpp


HEADERS  += mainwindow.h \
    annotationpreviewer.h \
    boxannotation.h \
    metadatadialog.h \
    settingsdialog.h \
    temporalinterpolator.h \
    common/registrator/registrator.h \
    annotationExporter.h

FORMS    += mainwindow.ui \
    metadatadialog.ui \
    settingsdialog.ui

!win32{
    LIBS += -L/usr/local/lib \
        -lopencv_core \
        -lopencv_imgproc \
        -lopencv_videoio \
        -lopencv_calib3d \
        -lopencv_imgcodecs \
        -lopencv_highgui \
        -lopencv_video

    INCLUDEPATH += /usr/local/include
}

RESOURCES = ressources.qrc

win32{
    LIBS += -LC:\OpenCV\opencv\sources\build\install\x64\mingw\bin -lopencv_core248 -lopencv_highgui248 -lopencv_imgproc248

    INCLUDEPATH += C:\OpenCV\opencv\sources\build\install\include
    DEPENDPATH += C:\OpenCV\opencv\sources\build\install\include
}

VPATH = common/


include($${VPATH}opencvviewer.pri)
include($${VPATH}cvutilities.pri)
include($${VPATH}annotatedtrack.pri)
include($${VPATH}autocompletedialog.pri)
